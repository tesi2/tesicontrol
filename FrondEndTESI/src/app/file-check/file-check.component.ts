import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { CargaService } from '../Services/carga.service';
import { ArchivoService } from '../Services/archivo.service';
import { Carga } from '../Model/carga';
import { Archivo } from '../Model/archivo';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import * as $ from 'jquery'
import { Router } from '@angular/router';
import { Proceso } from '../Model/proceso';
import { ProcesoService } from '../Services/proceso.service';
import { Alumno } from '../Model/alumno';

@Component({
  selector: 'app-file-check',
  templateUrl: './file-check.component.html',
  styleUrls: ['./file-check.component.css']
})
export class FileCheckComponent implements OnInit, OnDestroy {
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }
  dtTrigger = new Subject();
  loading : boolean
  private idcarga = localStorage.getItem('idcarga')
  carga: Carga
  archivoList: Archivo[]
  archivo: Archivo
  alumno : Alumno
  proceso: Proceso
  constructor(private location: Location,
    private cargaService: CargaService,
    private archivoService: ArchivoService,
    private procesoService: ProcesoService,
    private router: Router) { }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe
  }

  ngOnInit(): void {
    this.loading = true
    this.archivoService.findByCarga(this.idcarga).subscribe(result => {
      this.archivoList = result
      this.dtTrigger.next()
      this.loading = false
    })

    this.cargaService.findById(this.idcarga).subscribe(result => {
      this.carga = result;
      this.alumno = result.idalumno
      this.procesoService.findById(result.idproceso).subscribe(result => {
        this.proceso = result
      })
      this.loading = false
    })
    
    

  }
  back() {
    this.location.back();
  }
  endCheck() {
    Swal.fire({
      title: 'Desea finalizar la revision',
      text: 'se notificaran los resultados al alumno una vez acepte la finalizacion de la revision',
      icon: 'warning',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Finalizar revision',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      this.loading = true
      if (result.value) {
        this.carga.revisada = true
        this.cargaService.update(this.carga.idcarga).subscribe((result) => {
          this.loading = false
          Swal.fire({
            title: 'Se finalizo la revision con exito',
            icon: 'success',
            showCloseButton : true,
            showCancelButton : true,
            text: 'Se notifico al alumno la revision',
            confirmButtonText: 'Modificar informacion del alumno?',
            cancelButtonText: 'Regresar a lista de cargas'
          }).then((result) => {
            if(result.value){
                  localStorage.removeItem('idalumno');
                  localStorage.setItem('idalumno',this.carga.idalumno.idalumno.toString());
                  this.router.navigate(['alumnoEditAdmin'])
            }else{
              this.router.navigate(['cargaAdminsitrativoList'])
            }

          })
        })
      }
    })
  }
  view(i) {
    let archivo = this.archivoList[i]
    Swal.fire({
      title: archivo.nombre,
      html: '<img width="750" height="800" src=' + archivo.contenido + '></img>',
      confirmButtonText: 'Aprovar',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      showCloseButton: true,
      cancelButtonText: 'Rechazar'
    }).then(respuesta => {
      if (respuesta.value) {
        archivo.aprobado = true
        archivo.revisado = true
        archivo.comentario = 'archivo correcto'
        
        this.loading = true
        this.archivoService.updateFile(archivo).subscribe(result => {
          this.loading = false
          Swal.fire({
            title: 'Exito',
            showCloseButton: true,
            icon: 'success',
            text: 'Se actualizo el archivo'
          }).then(result => {
            window.location.reload();
          })
        })
      } else {
        let comentario
        Swal.mixin({
          input: 'text',
          confirmButtonText: 'Guardar comentario',
          showCancelButton: true,
          progressSteps: ['1']
        }).queue([
          {
            title: 'Motivo',
            text: 'Especifique el motivo del rechazo '
          }
        ]).then((res) => {
          comentario = res[0]
          console.log(comentario)
          archivo.aprobado = false
          archivo.revisado = true
          archivo.comentario = comentario
          this.loading = true
          this.archivoService.updateFile(archivo).subscribe(result => {
            this.loading = false
            Swal.fire({
              title: 'Exito',
              showCloseButton: true,
              icon: 'success',
              text: 'Se actualizo el archivo'
            }).then(result => {
              window.location.reload();
            })
          })
        })
      }
    })
  }
}

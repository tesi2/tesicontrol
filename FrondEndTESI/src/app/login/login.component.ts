import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Services/authentication.service';
import { UsersService } from '../Services/users.service';
import { Users } from '../Model/users';
import { Properties } from '../Configuration/properties';
import swal from 'sweetalert2';
import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: string;
  password: string;
  loading:boolean
  private users: Users;
  constructor(private router: Router, private authservice: AuthenticationService, private userService: UsersService) { }

  ngOnInit(): void {
  }
  login() {
    this.loading = true
    this.authservice.autentication({ "username": this.user, "password": this.password }).subscribe(
      result => {
        this.loading = false
        console.log('se obtuvo respuesta del back end: ' + Properties.API_ENDPOINT);
        localStorage.setItem('token', result);
        this.obtenerSesion(this.user);
      },
      error =>{
        this.loading = false
        swal.fire({
          title : 'Error',
          text : 'No se logro inciar Sesion',
          confirmButtonText :'OK'
        }).then(
          result=>{
            this.router.navigate(['home']);
          }
        );
      }
    )
  }
  obtenerSesion(username) {
    this.userService.getUser(username).subscribe(
      result => {
        console.log('se obtuvo respuesta del back end: ' + Properties.API_ENDPOINT);
        this.users = result;
        const newLocal = 'username';
        localStorage.setItem(newLocal, result.username);
        if (this.users.sesion == 'ALUMNO') {
          this.router.navigate(['sesionAlumno']);
        } else if (this.users.sesion == 'DOCENTE') {
          this.router.navigate(['sesionDocente']);
        } else if (this.users.sesion == 'ADMINISTRATIVO') {
          this.router.navigate(['sesionAdministrativo']);
        }
      },error =>{
        swal.fire({
          title : 'Error',
          text : 'No se logro inciar Sesion',
          confirmButtonText :'OK'
        }).then(
          result=>{
            this.router.navigate(['home']);
          }
        );
      }
      
    )
  }
}


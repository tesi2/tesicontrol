import { Component, OnInit } from '@angular/core';
import { Alumno } from '../Model/alumno';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AlumnoService } from '../Services/alumno.service';
import { GrupoService } from '../Services/grupo.service';
import { CarreraService } from '../Services/carrera.service';
import { Carrera } from '../Model/carrera';
import { Grupo } from '../Model/grupo';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-alumno-edit-admin',
  templateUrl: './alumno-edit-admin.component.html',
  styleUrls: ['./alumno-edit-admin.component.css']
})
export class AlumnoEditAdminComponent implements OnInit {

  constructor(private router: Router,
    private location: Location,
    private alumnoService: AlumnoService,
    private gruposService: GrupoService,
    private carreraService: CarreraService) { }
  private idalumno
  alumno: Alumno
  loading: boolean
  showUnEditableInfo: boolean
  showEditableInfo: boolean

  carreraList: Carrera[]
  grupoList: Grupo[]
  ngOnInit(): void {
    this.idalumno = localStorage.getItem('idalumno')
    this.loading = true
    this.showUnEditableInfo = true
    this.showEditableInfo = false
    this.alumnoService.findById(this.idalumno).subscribe(result => {
      this.alumno = result
      this.carreraService.findAll().subscribe(result => {
        this.carreraList = result
        this.gruposService.findAll().subscribe(result => {
          this.grupoList = result
          this.loading = false
        })
      })
    })
  }
  back() {
    this.location.back()
  }
  edit() {
    this.showEditableInfo = true
    this.showUnEditableInfo = false
  }
  save() {
    this.loading = true
    console.log(this.alumno)
    this.alumnoService.save(this.alumno).subscribe(result => {
      this.loading = false
      Swal.fire({
        title: 'exito',
        text: 'Se edito la información de manera correcta',
        icon: 'success',
        confirmButtonText: 'Ok'
      }).then(reslut => {
        this.back()
      })
    },error=>{
      this.loading = false
      Swal.fire({
        title : 'Ups....',
        text:'No se logro guardar al informacion',
        icon: 'error'
      }).then(result=>{
        this.back()
      }
       
      )
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { AulaService } from '../Services/aula.service';
import { EdificioService } from '../Services/edificio.service';
import { Edificio } from '../Model/edificio';
import { Aula } from '../Model/aula';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-aulas-add-admin',
  templateUrl: './aulas-add-admin.component.html',
  styleUrls: ['./aulas-add-admin.component.css']
})
export class AulasAddAdminComponent implements OnInit {

  constructor(private location: Location,
    private router: Router,
    private aulaService: AulaService,
    private edificioService: EdificioService) { }
  /**/
  edificio: Edificio
  nombre: string
  loading : boolean
  tipoAula: string
  idedificio = localStorage.getItem('idedificio')
  ngOnInit(): void {
    this.loading = true
    this.edificioService.findById(this.idedificio).subscribe(result => {
      this.edificio = result
      this.loading = false
    })
  }
  back() {
    this.location.back()
  }
  save() {
    this.loading = true
    this.nombre = this.nombre.toUpperCase()
    this.edificio.aulaList=null
    let aula = new Aula()
    aula.idedificio=this.edificio
    aula.nombre = this.nombre
    aula.tipo = this.tipoAula
    console.log(aula)
    this.aulaService.add(aula).subscribe(result=>{
      this.loading = false
      Swal.fire({
        title : 'Exito',
        icon : 'success',
        text : 'se agrego la aula: '+result.nombre,
        confirmButtonText : 'Ok'
      }).then(result=>{
        this.back();
      })
    })
  }
  onchangeName() {
    this.nombre = this.nombre.toUpperCase()
    console.log(this.nombre)
  }
  selectTipo() {

  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { DocenteService } from '../Services/docente.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Docente } from '../Model/docente';

@Component({
  selector: 'app-docente-list-admin',
  templateUrl: './docente-list-admin.component.html',
  styleUrls: ['./docente-list-admin.component.css']
})
export class DocenteListAdminComponent implements OnInit, OnDestroy {

  constructor(private docenteService: DocenteService,
    private router: Router,
    private location: Location) { }

  loading: boolean
  docenteList: Docente[]
  dtTrigger = new Subject()
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }
  ngOnInit(): void {
    this.loading = true
    this.docenteService.findAll().subscribe(result => {
      this.docenteList = result
      this.dtTrigger.next()
      this.loading = false
    }, error=>{
      this.loading = false
    })
  }
  back() {
    this.location.back()
  }
  edit(i) {
    localStorage.removeItem('iddocente')
    let docente = this.docenteList[i]
    console.log('redirigiendo.....')
    localStorage.setItem('iddocente', docente.iddocente.toString())
    this.router.navigate(['docenteEditAdmin'])
  }
  delete(i) {

  }
  golistMaterias(i){
   
    let docente  =  this.docenteList[i]
    console.log(docente)
    localStorage.removeItem('iddocente')
    localStorage.setItem('iddocente' , docente.iddocente.toString())
    console.log('redirigiendo.....')
    this.router.navigate(['docenteMateria'])

  }
  add() {
    this.router.navigate(['docenteAddAdmin'])
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alumno } from 'src/app/Model/alumno';
import { AlumnoService } from 'src/app/Services/alumno.service';
import swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-sesion-alumno',
  templateUrl: './sesion-alumno.component.html',
  styleUrls: ['./sesion-alumno.component.css']
})
export class SesionAlumnoComponent implements OnInit {
  alumno: Alumno;
  private token = localStorage.getItem('token');
  private username;
  constructor(private router: Router, private http: HttpClient, private serviceAlumno: AlumnoService) { }
  goUploadModule() {
    localStorage.setItem('idalumno', this.alumno.idalumno.toString());
    //this.router.navigate(['uploadFileComponent']);
    this.router.navigate(['cargaAlumnoList']);
  }
  goEditModule(){
    localStorage.setItem('idalumno', this.alumno.idalumno.toString());
    //this.router.navigate(['uploadFileComponent']);
    this.router.navigate(['alumnoEditAlumno']);
  }
  ngOnInit(): void {
    //obtener informacion del alumno
    this.username = localStorage.getItem('username');
  
    this.serviceAlumno.getAlumnoByUserName(this.username).subscribe(result => {
  
      if (result != null) {
        this.alumno = result;
      } else {
        swal.fire({
          title: 'Error',
          text: 'no se logro encontrar una alumno con el correo: ' + this.username,
          confirmButtonText: 'ok'
        }).then(result => {
          this.router.navigate(['home']);
        });

      }
    },
    )
  }
}

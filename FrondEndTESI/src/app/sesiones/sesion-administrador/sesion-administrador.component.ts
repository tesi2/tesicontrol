import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Administrativo } from 'src/app/Model/administrativo';
import { AdministrativoService } from 'src/app/Services/administrativo.service';
import { RolService } from 'src/app/Services/rol.service';
import { AreasService } from 'src/app/Services/areas.service';

@Component({
  selector: 'app-sesion-administrador',
  templateUrl: './sesion-administrador.component.html',
  styleUrls: ['./sesion-administrador.component.css']
})
export class SesionAdministradorComponent implements OnInit {

  constructor(private router: Router, private serviceAdmin: AdministrativoService, private rolService: RolService, private areaService: AreasService) { }
  admin: Administrativo
  ngOnInit(): void {
    let username = localStorage.getItem('username')
    this.serviceAdmin.findByUsername(username).subscribe(result => {
      this.admin = result
      this.areaService.findById(this.admin.idarea).subscribe(result => {
        this.admin.idarea = result
      })
      this.rolService.finbById(this.admin.idrol).subscribe(result => {
        this.admin.idrol = result
        console.log(this.admin)
      })
      localStorage.setItem('idadministrativo' , this.admin.idadministrativo.toString())
    });




  }
  goCargaList() {
    this.router.navigate(['cargaAdminsitrativoList'])
  }
  goAulasList() {

  }
  goGruposList() {

  }
  goAlumnosList() {
      this.router.navigate(['alumnoListAdmin'])
  }
  //edificioListAdmin

  goEdificiosList() {
    this.router.navigate(['edificioListAdmin'])
  }
  goMateriasList(){
    this.router.navigate(['materiaListAdmin'])
  }
  goDocentesList(){
    this.router.navigate(['docenteListAdmin'])
  }
}

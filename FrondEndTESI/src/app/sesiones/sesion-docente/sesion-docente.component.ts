import { Component, OnInit } from '@angular/core';
import { DocenteService } from 'src/app/Services/docente.service';
import { Docente } from 'src/app/Model/docente';

@Component({
  selector: 'app-sesion-docente',
  templateUrl: './sesion-docente.component.html',
  styleUrls: ['./sesion-docente.component.css']
})
export class SesionDocenteComponent implements OnInit {
private username = localStorage.getItem('username');
docente : Docente;
  constructor(private docenteService : DocenteService) { }

  ngOnInit(): void {
this.docenteService.findByUsername(this.username).subscribe(result=>{

});
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { CargaService } from '../Services/carga.service';
import { CarreraService } from '../Services/carrera.service';
import { Carga } from '../Model/carga';
import { Carrera } from '../Model/carrera'
import { Proceso } from '../Model/proceso';
import { Subject } from 'rxjs';
import { AlumnoService } from '../Services/alumno.service';
import { Archivo } from '../Model/archivo';
import { ArchivoService } from '../Services/archivo.service';
import { Alumno } from '../Model/alumno';
import { } from 'jszip';
import * as JSZip from 'jszip';
import { saveAs } from 'file-saver'
import { logging } from 'protractor';

@Component({
  selector: 'app-carga-list-administrativo',
  templateUrl: './carga-list-administrativo.component.html',
  styleUrls: ['./carga-list-administrativo.component.css']
})
export class CargaListAdministrativoComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
    private location: Location,
    private cargaService: CargaService,
    private carreraService: CarreraService,
    private alumnoService: AlumnoService,
    private archivoService: ArchivoService
  ) { }

  cargaList: Carga[]
  carrerasList: Carrera[]
  carrera: Carrera
  semestreList: string[]
  proceso: Proceso
  procesoList: Proceso[]
  loading: boolean
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }
  dtTrigger = new Subject()
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  ngOnInit(): void {
    this.loading = true
    this.cargaService.findAll().subscribe(result => {
      this.cargaList = result;
      this.dtTrigger.next();
      this.loading = false
    })
  }
  view(i) {
    let alumno
    let proceso
    let carga = this.cargaList[i]
    this.alumnoService.findById(carga.idalumno.idalumno).subscribe(result => {
      alumno = result
    })
  }
  check(i) {
    localStorage.removeItem('idcarga')
    localStorage.setItem('idcarga', this.cargaList[i].idcarga.toString())
    this.router.navigate(['fileCheck'])
  }
  back() {
    this.location.back()
  }
  download(i) {
    console.log('descargando.......')
    this.loading = true
    let carga = this.cargaList[i]
    console.log('carga seleccionada :' + carga.idcarga)
    let archivoList: Archivo[] = []
    this.archivoService.findByCarga(carga.idcarga.toString()).toPromise().then(result => {
      archivoList = result
      let zip = new JSZip()
      archivoList.forEach(archivo => {
        zip.file(archivo.nombre, archivo.contenido.split(',')[1], { base64: true })
      })
      let content = zip.generateAsync({ type: "blob" })
      this.loading = false
      saveAs(content, 'prueba.zip')
    })


  }

  buildFileName(alumno: Alumno, archivo: Archivo) {
    let filename = ''
    filename.concat(alumno.nombre + alumno.paterno + alumno.materno).toUpperCase().trim()
    filename.concat('_')
    filename.concat(archivo.idtipoarchivo.tipo.toUpperCase()).trim()
    filename.concat('.jpeg')
    return filename
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { DocenteService } from '../Services/docente.service';
import { MateriaService } from '../Services/materia.service';
import { Docente } from '../Model/docente';
import { Materia } from '../Model/materia';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-docente-materia',
  templateUrl: './docente-materia.component.html',
  styleUrls: ['./docente-materia.component.css']
})
export class DocenteMateriaComponent implements OnInit , OnDestroy {

  constructor(private router :  Router,
    private docenteservice : DocenteService,
    private materiaService : MateriaService,
    private location : Location
    ) { }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe
  }
iddocente = localStorage.getItem('iddocente')
docente : Docente
materiaList : Materia[]
loading : boolean
dtTrigger = new Subject()
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }
  ngOnInit(): void {
this.loading = true
this.docenteservice.findById(this.iddocente).subscribe(result=>{
this.docente = result
    this.materiaService.findByIddocente(this.iddocente).subscribe(result=>{
      this.materiaList = result
      this.dtTrigger.unsubscribe()
      this.loading = false
    })
},error=>{
this.loading = false
Swal.fire({
  title : 'Ups.....',
  text : 'algo salio mal',
  icon : 'error'
}).then(r=>{
  this.location.back()
})


})

  }
add(){

}
delete(){
  
}


}

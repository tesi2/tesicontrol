import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AlumnoService } from './Services/alumno.service';
import { UsersService } from './Services/users.service';
import { AuthenticationService } from './Services/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { SesionAlumnoComponent } from './sesiones/sesion-alumno/sesion-alumno.component';
import { HomeComponent } from './home/home.component';
import { SesionDocenteComponent } from './sesiones/sesion-docente/sesion-docente.component';
import { SesionAdministradorComponent } from './sesiones/sesion-administrador/sesion-administrador.component';
import { CFileUploadComponent } from './cfile-upload/cfile-upload.component';
import {CommonModule, UpperCasePipe} from '@angular/common'
import {DataTablesModule} from 'angular-datatables';
import { CargaListAlumnoComponent } from './carga-list-alumno/carga-list-alumno.component';
import { ArchivoListAlumnoComponent } from './archivo-list-alumno/archivo-list-alumno.component';
import { FileListComponent } from './file-list/file-list.component';
import { CargaListAdministrativoComponent } from './carga-list-administrativo/carga-list-administrativo.component';
import { FileCheckComponent } from './file-check/file-check.component';
import { AulasListAdminComponent } from './aulas-list-admin/aulas-list-admin.component';
import { AulasEditAdminComponent } from './aulas-edit-admin/aulas-edit-admin.component';
import { AulasAddAdminComponent } from './aulas-add-admin/aulas-add-admin.component';
import { GrupoListAdminComponent } from './grupo-list-admin/grupo-list-admin.component';
import { GrupoEditAdminComponent } from './grupo-edit-admin/grupo-edit-admin.component';
import { GrupoAddAdminComponent } from './grupo-add-admin/grupo-add-admin.component';
import { AlumnoListAdminComponent } from './alumno-list-admin/alumno-list-admin.component';
import { AlumnoEditAdminComponent } from './alumno-edit-admin/alumno-edit-admin.component';
import { AlumnoAddAdminComponent } from './alumno-add-admin/alumno-add-admin.component';
import { RegistrarCandidatoComponent } from './registrar-candidato/registrar-candidato.component';
import { EdificioListAdminComponent } from './edificio-list-admin/edificio-list-admin.component';
import { EdificioAddAdminComponent } from './edificio-add-admin/edificio-add-admin.component';
import { EdificioEditAdminComponent } from './edificio-edit-admin/edificio-edit-admin.component';
import { LoadingComponent } from './loading/loading.component';
import { MateriaAddAdminComponent } from './materia-add-admin/materia-add-admin.component';
import { MateriaEditAdminComponent } from './materia-edit-admin/materia-edit-admin.component';
import { MateriaListAdminComponent } from './materia-list-admin/materia-list-admin.component';
import { DocenteListAdminComponent } from './docente-list-admin/docente-list-admin.component';
import { DocenteEditAdminComponent } from './docente-edit-admin/docente-edit-admin.component';
import { DocenteAddAdminComponent } from './docente-add-admin/docente-add-admin.component';
import { DocenteMateriaComponent } from './docente-materia/docente-materia.component';
import { AlumnoEditAlumnoComponent } from './alumno-edit-alumno/alumno-edit-alumno.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SesionAlumnoComponent,
    HomeComponent,
    SesionDocenteComponent,
    SesionAdministradorComponent,
    CFileUploadComponent,
    CargaListAlumnoComponent,
    ArchivoListAlumnoComponent,
    FileListComponent,
    CargaListAdministrativoComponent,
    FileCheckComponent,
    AulasListAdminComponent,
    AulasEditAdminComponent,
    AulasAddAdminComponent,
    GrupoListAdminComponent,
    GrupoEditAdminComponent,
    GrupoAddAdminComponent,
    AlumnoListAdminComponent,
    AlumnoEditAdminComponent,
    AlumnoAddAdminComponent,
    RegistrarCandidatoComponent,
    EdificioListAdminComponent,
    EdificioAddAdminComponent,
    EdificioEditAdminComponent,
    LoadingComponent,
    MateriaAddAdminComponent,
    MateriaEditAdminComponent,
    MateriaListAdminComponent,
    DocenteListAdminComponent,
    DocenteEditAdminComponent,
    DocenteAddAdminComponent,
    DocenteMateriaComponent,
    AlumnoEditAlumnoComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    DataTablesModule
  ],
  providers: [
    AlumnoService,
    UsersService,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

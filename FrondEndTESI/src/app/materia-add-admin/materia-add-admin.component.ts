import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Materia } from '../Model/materia';
import { MateriaService } from '../Services/materia.service';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-materia-add-admin',
  templateUrl: './materia-add-admin.component.html',
  styleUrls: ['./materia-add-admin.component.css']
})
export class MateriaAddAdminComponent implements OnInit {

  constructor(private location: Location, private router: Router, private materiaService: MateriaService) { }
  loading: boolean
  materia = new Materia()
  ngOnInit(): void {
  }
  changeNombre() {
    this.materia.nombre = this.materia.nombre.toUpperCase()
    console.log(this.materia.nombre)
  }
  save() {
    this.loading = true
    this.materiaService.check(this.materia.nombre).subscribe(result => {
      if (result == "SI") {
        this.materiaService.save(this.materia).subscribe(result => {
          this.loading = false
          Swal.fire({
            title: 'Exito',
            icon: 'success',
            text: 'Se agrego la materia de forma correcta'
          }).then(result=>{
            this.materia = new Materia()
          })
        }, error => {
          this.loading = false
          Swal.fire({
            title: 'Upss',
            icon: 'error',
            text: 'No se pudo conctar con el servidor'
          })

        })
      } else {
        this.loading = false
        this.materia.nombre = ""
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Esta materia ya ha sido registrada'
        })
      }
    },
      error => {
        this.loading = false
        Swal.fire({
          title: 'Upss',
          icon: 'error',
          text: 'No se pudo conctar con el servidor'
        })
      })
  }
  validar() {

  }
}

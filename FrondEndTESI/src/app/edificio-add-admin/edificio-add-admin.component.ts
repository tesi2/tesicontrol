import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EdificioService } from '../Services/edificio.service';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { Edificio } from '../Model/edificio';

@Component({
  selector: 'app-edificio-add-admin',
  templateUrl: './edificio-add-admin.component.html',
  styleUrls: ['./edificio-add-admin.component.css']
})
export class EdificioAddAdminComponent implements OnInit {

  constructor(private router: Router, private location: Location, private serviceEdificio: EdificioService) { }
  nombre: string
  ubicacion: string
  loading : boolean
  ngOnInit(): void {
  }
  back() {
    this.location.back();
  }
  changeNombre() {
    this.nombre = this.nombre.toUpperCase()
    console.log(this.nombre)
  }
  changeUbicacion() {
    this.ubicacion = this.ubicacion.toUpperCase()
    console.log(this.ubicacion)
  }
  save() {
    //validar nombre
    this.serviceEdificio.check(this.nombre).subscribe(result => {
      if (result == "SI") {
      let edificio = new Edificio();
        edificio.nombre = this.nombre
        edificio.ubicacion = this.ubicacion
        this.loading = true
        this.serviceEdificio.add(edificio).subscribe(result=>{
          this.loading = false
          Swal.fire({
            title : 'Exito', 
            icon : 'success',
            text : 'El edificio' + result.nombre + ' se registro correctamente'

          }).then(result=>{
              this.location.back();
          })
        },
        error =>{
          this.loading = false
          Swal.fire({
            title : 'ups....',
            icon : 'error',
            text : 'no se logro guardar el edificio'
          }).then(result=>{
            this.location.back()
          })
        })
       } else {
        this.loading = false
        Swal.fire({
          title : 'ups',
          text : ' el nombre '+this.nombre+' no esta disponible',
          icon: 'error',
          confirmButtonText : 'Ok'
        }).then(result=>{
          this.nombre = ''
        })
      }
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { AlumnoService } from '../Services/alumno.service';
import { Alumno } from '../Model/alumno';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-alumno-edit-alumno',
  templateUrl: './alumno-edit-alumno.component.html',
  styleUrls: ['./alumno-edit-alumno.component.css']
})
export class AlumnoEditAlumnoComponent implements OnInit {

  constructor(private alumnoService: AlumnoService,
    private router: Router,
    private location: Location) { }
  loading: boolean
  alumno: Alumno
  private username
  idalumno = localStorage.getItem('idalumno')
  ngOnInit(): void {
    this.loading = true
    this.alumnoService.findById(this.idalumno).subscribe(result => {
      this.alumno = result
      this.username = this.alumno.correo
      this.loading = false
    }, error => {
      this.loading = false
      Swal.fire({
        title: 'Ups......',
        text: 'No se encontro inforamcion apra editar',
        icon: 'error'
      })
    })
  }

  back() {
    this.location.back()
  }
  save() {
    if (this.username == this.alumno.correo) {
      this.loading = true
      this.alumnoService.save(this.alumno).subscribe(result => {
        this.loading = false
        Swal.fire({

          icon: 'success',
          title: 'Exito',
          text: 'Se actualizo tu información de forma correcta'
        })
      })
    } else {
      Swal.fire({
        title: 'Cuidado',
        icon: 'warning',
        text: 'Recuerda que el correo se utiliza para tu acceso a la plataforma'
      }).then((result => {
        if (result.value) {
          this.loading = false
          this.alumnoService.save(this.alumno).subscribe(result => {
            Swal.fire({
              icon: 'success',
              title: 'Exito',
              text: 'Se actualizo tu información de forma correcta'
            })
          })
        }
      }))
    }
  }
}


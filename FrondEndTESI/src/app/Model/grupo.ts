import { Aula } from './aula'

export class Grupo {
    idgrupo: number
    turno: string
    codigo: string
    idaula: Aula
}

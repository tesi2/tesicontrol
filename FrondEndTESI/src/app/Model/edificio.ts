import { Aula } from './aula'

export class Edificio {
    idedificio : number
    nombre : string
    ubicacion :  string
    aulaList  : Aula[]
}

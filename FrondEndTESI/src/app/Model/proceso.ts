export class Proceso {
    idproceso : number;
    nombre: string;
    valido : boolean;
    fechaIncio : Date;
    fechaFinal: Date;
}

import { Tipoarchivo } from './tipo-archivo';
import { Carga } from './carga';


export class Archivo {
    idarchivo: number;
    fechaCarga: Date;
    nombre: string;
    contenido: string;
    comentario: string;
    aprobado: boolean;
    revisado: boolean;
    idalumno: number;
    idcarga: number;
    idtipoarchivo: Tipoarchivo;

}

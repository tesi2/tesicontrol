import { Materia } from './materia'

export class Docente {
    iddocente : number
    nombre : string
    paterno :  string
    materno : string
    cedula : string
    correo :  string
    telefono : string
    matricula : string
    nss : string
    materiaList : Materia[]
}

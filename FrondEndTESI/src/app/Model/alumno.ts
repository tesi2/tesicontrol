import { Carrera } from './carrera';
import { Grupo } from './grupo';

export class Alumno {
    idalumno: number;
    matricula: string;
    nombre: string;
    paterno: string;
    materno: string;
    semestre: string;
    nss: string;
    telefono: string;
    correo: string;
    tipo: string;
    inscrito: boolean;
    idcarrera: Carrera;
    idgrupo: Grupo;

    getfullName():string{
        return this.nombre + ' '+this.paterno + ' ' +this.materno 
    }
}

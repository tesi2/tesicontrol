import { Area } from './area'
import { Rol } from './rol'

export class Administrativo {
    idadministrativo: number
    nombre: string
    paterno: string
    materno: string
    telefono: string
    correo: string
    idarea: Area
    idrol: Rol

}

import { Alumno } from './alumno'

export class Carrera {
    idcarrera : number
    nombre : string
    codigo : string
    alumnoList :Alumno[]
}

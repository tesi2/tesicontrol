import { Edificio } from './edificio'

export class Aula {
    idaula :  number
    nombre  :  string
    tipo : string
    idedificio : Edificio
}

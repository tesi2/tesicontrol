import { Archivo } from './archivo';
import { Alumno } from './alumno';
import { Proceso } from './proceso';

export class Carga {
    idcarga: number;
    fechacarga: Date;
    revisada: boolean;
    idalumno: Alumno;
    idproceso : number;
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EdificioService } from '../Services/edificio.service';
import { Edificio } from '../Model/edificio';
import Swal from 'sweetalert2';
import { Administrativo } from '../Model/administrativo';
import { RolService } from '../Services/rol.service';
import { AdministrativoService } from '../Services/administrativo.service';
import { Location } from '@angular/common';
import { AulaService } from '../Services/aula.service';

@Component({
  selector: 'app-edificio-list-admin',
  templateUrl: './edificio-list-admin.component.html',
  styleUrls: ['./edificio-list-admin.component.css']
})
export class EdificioListAdminComponent implements OnInit {

  constructor(private router: Router,
    private serviceEdificio: EdificioService,
    private serviceAdministrativo: AdministrativoService,
    private serviceAula: AulaService,
    private location: Location) {
  }
  loading : boolean
  edificioList: Edificio[]
  administrativo: Administrativo
  idadministrativo = localStorage.getItem('idadministrativo')
  ngOnInit(): void {
    this.loading = true
    this.serviceAdministrativo.findById(this.idadministrativo).subscribe(result => {
      this.administrativo = result
      this.serviceEdificio.findAll().subscribe(result => {
        this.edificioList = result;
        this.loading = false
      })
    })
    

  }
  deleteEdificio(i) {
    let edificio = this.edificioList[i]
    Swal.fire({
      title: 'Confirmar',
      text: 'Desea eliminar el edificio: ' + edificio.nombre + ' junto con sus aulas',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar',
      cancelButtonText: 'no'
    }).then(result => {

      if (result.value) {
        this.loading = true
        this.serviceEdificio.delete(edificio.idedificio, this.administrativo.idadministrativo).subscribe(result => {
         this.loading = false
          if (result == "OK") {
            Swal.fire({
              title: 'Exito',
              icon: 'success',
              text: 'El edificio fue eliminado de forma exitosa'
            }).then(result => {
              window.location.reload()
            })
          }
        })
      } else {
      }
    })
  }
  deleteAula(i, j) {
    let edificio = this.edificioList[i]
    let aula = edificio.aulaList[j]
    edificio.aulaList = null
    aula.idedificio = edificio
    Swal.fire({
      title: 'Confirmar',
      icon: 'warning',
      text: 'Desea eliminar el aula: ' + aula.nombre,
      confirmButtonText: 'Si, eliminar',
      showCancelButton: true,
      cancelButtonText: 'No'
    }).then(result => {
      this.loading = true
      if (result.value) {
        this.serviceAula.delete(aula).subscribe(res => {
          this.loading = false
          if (res == "OK") {
            Swal.fire({
              title: 'Exito',
              text: 'Se Elimino el aula de forma correcta',
              confirmButtonText: 'Ok',
              icon: 'success'
            }).then(result => {
              window.location.reload()
            }
            )
          }
        })
      }
    })
  }
  addAula(i) {
    let edificio = this.edificioList[i]
    localStorage.removeItem('idedificio')
    localStorage.setItem('idedificio', edificio.idedificio.toString())
    this.router.navigate(['aulaAddAdmin'])
  }
  addEdificio() {
    this.router.navigate(['edificioAddAdmin']);
  }

}

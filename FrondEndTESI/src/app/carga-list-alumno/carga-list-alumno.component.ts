import { Component, OnInit, OnDestroy } from '@angular/core';
import { CargaService } from '../Services/carga.service';
import { AlumnoService } from '../Services/alumno.service';
import { Alumno } from '../Model/alumno';
import { Carga } from '../Model/carga';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-carga-list-alumno',
  templateUrl: './carga-list-alumno.component.html',
  styleUrls: ['./carga-list-alumno.component.css']
})
export class CargaListAlumnoComponent implements OnInit, OnDestroy {

  constructor(private location :Location,private router : Router,private cargaService: CargaService, private alumnoService: AlumnoService) { }

  private idalumno
  alumno: Alumno
  cargaList: Carga[]
  loading : boolean
  dtOptions: DataTables.Settings = { 
    language : {
      url : '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }
  dtTrigger  = new Subject()
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
  ngOnInit(): void {
    this.loading = true
    this.idalumno = localStorage.getItem('idalumno')
    this.alumnoService.findById(this.idalumno).subscribe(result => {
      this.alumno = result
    })
    this.cargaService.findByIdAlumno(this.idalumno).subscribe(result => {
      this.cargaList = result;
      this.dtTrigger.next();
      this.loading = false
    })
  }
  goFileUpload(){
  
    this.router.navigate(['uploadFileComponent'])
  }
  viewFile(i){
    let carga = this.cargaList[i];
    localStorage.setItem('idcarga', carga.idcarga.toString());
    this.router.navigate(['fileList']);
  }
  back(){
this.location.back();
  }
}

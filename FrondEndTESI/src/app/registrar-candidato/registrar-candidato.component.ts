import { Component, OnInit } from '@angular/core';
import { AlumnoService } from '../Services/alumno.service';
import { Alumno } from '../Model/alumno';
import { CarreraService } from '../Services/carrera.service';
import { Carrera } from '../Model/carrera';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-candidato',
  templateUrl: './registrar-candidato.component.html',
  styleUrls: ['./registrar-candidato.component.css']
})
export class RegistrarCandidatoComponent implements OnInit {

  constructor(private alumnoService : AlumnoService, private carreraService : CarreraService) { }
alumno : Alumno

loading : boolean
carreras : Carrera[]
carrera : Carrera
  ngOnInit(): void {
    this.loading = true
    this.alumno = new Alumno()
    this.carreraService.findAll().subscribe(result=>{
      this.carreras = result
      this.carrera = result[0]
      this.loading = false
    })
  }
  singUp(){
    Swal.fire({
      title : 'Confirmar Informacion',
      icon : 'warning',
      text : 'Seras registrado como aspirante a la carrera: '+this.carrera.nombre,
      confirmButtonText : 'Si, registrar',
      showCancelButton : true,
      cancelButtonText : 'Cancelar'
    }).then(result=>{
      if(result.value){
        
        this.alumno.idcarrera = this.carrera
        this.loading = true
        this.alumnoService.singUp(this.alumno).subscribe(result=>{
          this.loading = false
          Swal.fire({
            title : 'Exito',
            icon : 'success',
            text : result.matricula

          })
        },error=>{
          this.loading = false
          Swal.fire({
            title : 'Ups.....',
            text : 'no se puedo registrar al aspirante',
            icon : 'error'
          })
        })
      }
    })
  }

}

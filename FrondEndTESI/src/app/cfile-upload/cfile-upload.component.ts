import { Component, OnInit } from '@angular/core';

import swal from 'sweetalert2';
import { Alumno } from '../Model/alumno';
import { Archivo } from '../Model/archivo';
import { CargaService } from '../Services/carga.service';
import { TipoArchivoService } from '../Services/tipo-archivo.service';
import { Tipoarchivo } from '../Model/tipo-archivo';
import { Proceso } from '../Model/proceso';
import { ProcesoService } from '../Services/proceso.service';
import { ArchivoService } from '../Services/archivo.service';
import { Carga } from '../Model/carga';
import { AlumnoService } from '../Services/alumno.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cfile-upload',
  templateUrl: './cfile-upload.component.html',
  styleUrls: ['./cfile-upload.component.css']
})
export class CFileUploadComponent implements OnInit {

  constructor(private serviceCarga: CargaService,
    private serviceTipoArchivo: TipoArchivoService,
    private serviceProceso: ProcesoService,
    private serviceArchivo: ArchivoService,
    private serviceAlumno: AlumnoService,
    private router: Router,
    private location: Location
  ) {
  }
  file = File;
  tipoArchivo: Tipoarchivo;
  tipoArchivoList: Tipoarchivo[];
  procesoList: Proceso[];
  proceso: Proceso;
  idalumno = localStorage.getItem('idalumno');
  alumno: Alumno;
  archivoList: Archivo[] = [];
  carga: Carga;
  loading: boolean

  ngOnInit(): void {
    this.loading = true
    this.serviceAlumno.findById(this.idalumno).subscribe(result => {
      this.alumno = result;
      this.serviceTipoArchivo.getAll().subscribe(result => {
        this.tipoArchivoList = result;
        this.tipoArchivo = this.tipoArchivoList[0];
        this.serviceProceso.findAll().subscribe(result => {
          this.procesoList = result;
          this.proceso = this.procesoList[0];
          this.loading = false
        });
      });
    });


  }
  uploadFiles() {
    //crear carga 
    let carga = new Carga();
this.loading = true
    carga.idalumno = this.alumno;
    carga.idproceso = this.proceso.idproceso
    // carga.archivosList = this.archivoList;
    console.log(carga);
    this.serviceCarga.saveCarga(carga).subscribe(result => {
      this.carga = result;
      console.log('carga registrada con exito con el id carga: ' + this.carga.idcarga);
      this.saveFiles(result.idcarga);
    },
      error => {
        this.loading = false
        swal.fire({
          title: 'Ups..',
          text: 'Ocurrio un error intente mas tarde',
          icon: 'error'
        })
      })
  }
  saveFiles(idcarga: number) {
    this.archivoList.forEach(a => {
      this.serviceArchivo.saveFile(a, idcarga).subscribe(result => {
        console.log('Se persistio el archivo de forma correcta con el id : ' + result.idarchivo);
      })
    })
    this.loading = false
    swal.fire({
      title: 'Exito',
      icon: 'success',
      text: 'se han cargado los harchivos de forma correcta con el folio de carga: ' + this.carga.idcarga
    })
  }
  getFile(event) {
    let archivo = new Archivo();
    let f: File = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = function () {
      archivo.contenido = reader.result.toString();

    };
    reader.readAsDataURL(f);
    archivo.nombre = f.name.toString();
    archivo.idtipoarchivo = this.tipoArchivo;
    this.archivoList.push(archivo);

  }
  cleanTable() {
    var tabla = document.getElementById('tbl');
    var tbody = document.getElementById('tbl-bd');
    tbody.childNodes.forEach(function (node) {
      tbody.removeChild(node);
    })

  }
  addFile() {
    this.cleanTable();
    var tblBody = document.getElementById('tbl-bd');
    this.archivoList.forEach(function (archivo, index) {
      var row = document.createElement('tr');
      var tr0 = document.createElement('td');
      tr0.appendChild(document.createTextNode((index + 1).toString()));
      var tr1 = document.createElement('td');
      tr1.appendChild(document.createTextNode(archivo.idtipoarchivo.tipo));
      var tr2 = document.createElement('td');
      tr2.appendChild(document.createTextNode(archivo.nombre));
      var tr3 = document.createElement('td');
      var btn = document.createElement('button');
      var i = document.createElement('i');
      i.classList.add('fas', 'fa-times');
      btn.appendChild(i);
      btn.classList.add('btn');
      btn.classList.add('btn-danger');
       
      tr3.appendChild(btn);
      row.appendChild(tr0);
      row.appendChild(tr1);
      row.appendChild(tr2);
      row.appendChild(tr3);
      tblBody.appendChild(row);
    })
  }
  back() {
    this.location.back();
  }
}

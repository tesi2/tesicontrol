import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MateriaService } from '../Services/materia.service';
import { Materia } from '../Model/materia';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-materia-list-admin',
  templateUrl: './materia-list-admin.component.html',
  styleUrls: ['./materia-list-admin.component.css']
})
export class MateriaListAdminComponent implements OnInit, OnDestroy {

  constructor(private router: Router, private location: Location, private materiaService: MateriaService) { }
  materiaList: Materia[]
  dtTrigger = new Subject()
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }
  loading: boolean
  ngOnInit(): void {
    this.loading = true
    this.materiaService.findAll().subscribe(result => {
      console.log(result)
      this.loading = false
      this.materiaList = result
      
      //this.dtTrigger.next()
    })
  }
  back(){
    this.location.back()
  }
add(){
  this.router.navigate(['materiaAddAdmin'])
}
}

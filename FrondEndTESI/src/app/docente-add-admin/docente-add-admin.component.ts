import { Component, OnInit } from '@angular/core';
import { DocenteService } from '../Services/docente.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Docente } from '../Model/docente';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-docente-add-admin',
  templateUrl: './docente-add-admin.component.html',
  styleUrls: ['./docente-add-admin.component.css']
})
export class DocenteAddAdminComponent implements OnInit {

  constructor(private docenteService : DocenteService ,private location : Location
    ,private router : Router) { }
loading : boolean
docente = new Docente()
  ngOnInit(): void {
   
  }

  add(){
    this.loading = true
    console.log(this.docente)
    this.docenteService.add(this.docente).subscribe(result=>{
      this.loading = false
      Swal.fire({
        title : 'Exito',
        icon : 'success',
        text : 'El docente fue registrado con exito'
      })
    })
    
  }
back(){
  this.location.back();
}
}

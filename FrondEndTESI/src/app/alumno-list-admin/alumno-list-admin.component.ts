import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AlumnoService } from '../Services/alumno.service';
import { Subject } from 'rxjs';
import { Alumno } from '../Model/alumno';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-alumno-list-admin',
  templateUrl: './alumno-list-admin.component.html',
  styleUrls: ['./alumno-list-admin.component.css']
})
export class AlumnoListAdminComponent implements OnInit , OnDestroy {

  constructor(private router :Router,private location:Location, private alumnoService : AlumnoService) { }
  dtTrigger = new Subject()
  alumnoList : Alumno[]
  loading : boolean
  dtOptions: DataTables.Settings = {
    language: {
      url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
    }
    
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe()
  }

  ngOnInit(): void {
    this.loading = true
    this.alumnoService.findAll().subscribe(result=>{
      this.alumnoList = result 
      console.log(result)
      this.dtTrigger.next()
      this.loading = false
    })
    
    
  }
  view(i){
    let alumno = this.alumnoList[i]
    let swal_html ="<table class='table'>"
    + " <tr><td>carrera </td><td>"+  alumno.idcarrera.nombre +"</td></tr>"
    + " <tr><td>Telefono </td><td>"+  alumno.telefono +"</td></tr>"
    + " <tr><td>correo </td><td>"+  alumno.correo +"</td></tr>"
    + " <tr><td>N.S.S </td><td>"+  alumno.nss +"</td></tr>"
    + " <tr><td>Semestre </td><td>"+  alumno.semestre+'°' +"</td></tr>"
    +" </table>"
      Swal.fire({
        title : alumno.nombre +' '+ alumno.paterno +' '+ alumno.materno,
        icon : 'info',
        html : swal_html,
        confirmButtonText : 'Ok'
      })
  }
  edit(i){
    localStorage.removeItem('idalumno')
    let alumno = this.alumnoList[i]
    localStorage.setItem('idalumno', alumno.idalumno.toString())
    this.router.navigate(['alumnoEditAdmin'])
  }

}

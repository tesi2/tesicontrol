import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SesionAlumnoComponent } from './sesiones/sesion-alumno/sesion-alumno.component';
import { HomeComponent } from './home/home.component';
import { SesionDocenteComponent } from './sesiones/sesion-docente/sesion-docente.component';
import { SesionAdministradorComponent } from './sesiones/sesion-administrador/sesion-administrador.component';
import { CFileUploadComponent } from './cfile-upload/cfile-upload.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CargaListAlumnoComponent } from './carga-list-alumno/carga-list-alumno.component';
import { FileListComponent } from './file-list/file-list.component';
import { CargaListAdministrativoComponent } from './carga-list-administrativo/carga-list-administrativo.component';
import { FileCheckComponent } from './file-check/file-check.component';
import { AlumnoAddAdminComponent } from './alumno-add-admin/alumno-add-admin.component';
import { AlumnoListAdminComponent } from './alumno-list-admin/alumno-list-admin.component';
import { AulasAddAdminComponent } from './aulas-add-admin/aulas-add-admin.component';
import { AulasEditAdminComponent } from './aulas-edit-admin/aulas-edit-admin.component';
import { AulasListAdminComponent } from './aulas-list-admin/aulas-list-admin.component';
import { GrupoListAdminComponent } from './grupo-list-admin/grupo-list-admin.component';
import { GrupoEditAdminComponent } from './grupo-edit-admin/grupo-edit-admin.component';
import { GrupoAddAdminComponent } from './grupo-add-admin/grupo-add-admin.component';
import { RegistrarCandidatoComponent } from './registrar-candidato/registrar-candidato.component';
import { EdificioListAdminComponent } from './edificio-list-admin/edificio-list-admin.component';
import { EdificioAddAdminComponent } from './edificio-add-admin/edificio-add-admin.component';
import { AlumnoEditAdminComponent } from './alumno-edit-admin/alumno-edit-admin.component';
import { DocenteAddAdminComponent } from './docente-add-admin/docente-add-admin.component';
import { DocenteEditAdminComponent } from './docente-edit-admin/docente-edit-admin.component';
import { DocenteListAdminComponent } from './docente-list-admin/docente-list-admin.component';
import { MateriaAddAdminComponent } from './materia-add-admin/materia-add-admin.component';
import { MateriaListAdminComponent } from './materia-list-admin/materia-list-admin.component';
import { DocenteMateriaComponent } from './docente-materia/docente-materia.component';
import { AlumnoEditAlumnoComponent } from './alumno-edit-alumno/alumno-edit-alumno.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sesionAlumno', component: SesionAlumnoComponent },
  { path: 'home', component: HomeComponent },
  { path: 'sesionDocente', component: SesionDocenteComponent },
  { path: 'uploadFileComponent', component: CFileUploadComponent },
  { path: 'sesionAdministrativo', component: SesionAdministradorComponent },
  { path: 'cargaAlumnoList', component: CargaListAlumnoComponent },
  { path: 'fileList', component: FileListComponent },
  { path: 'cargaAdminsitrativoList', component: CargaListAdministrativoComponent },
  { path: 'fileCheck', component: FileCheckComponent },
  { path: 'alumnoEditAdmin', component: AlumnoEditAdminComponent },
  { path: 'alumnoAddAdmin', component: AlumnoAddAdminComponent },
  { path: 'alumnoListAdmin', component: AlumnoListAdminComponent },
  { path: 'aulaAddAdmin', component: AulasAddAdminComponent },
  { path: 'aulaEditAdmin', component: AulasEditAdminComponent },
  { path: 'aulaListAdmin', component: AulasListAdminComponent },
  { path: 'grupoListAdmin', component: GrupoListAdminComponent },
  { path: 'grupoEditAdmin', component: GrupoEditAdminComponent },
  { path: 'grupoAddAdmin', component: GrupoAddAdminComponent },
  { path: 'registrarCandidato', component: RegistrarCandidatoComponent },
  { path: 'edificioListAdmin', component: EdificioListAdminComponent },
  { path: 'edificioAddAdmin', component: EdificioAddAdminComponent },
  { path: 'docenteAddAdmin', component: DocenteAddAdminComponent },
  { path: 'docenteEditAdmin', component: DocenteEditAdminComponent },
  { path: 'docenteListAdmin', component: DocenteListAdminComponent },
  { path: 'materiaAddAdmin', component: MateriaAddAdminComponent },
  { path: 'materiaListAdmin', component: MateriaListAdminComponent },
  { path: 'materiaEditAdmin', component: MateriaListAdminComponent },
  {path : 'docenteMateria' ,  component :DocenteMateriaComponent},
  {path : 'alumnoEditAlumno', component:AlumnoEditAlumnoComponent}
];

@NgModule({
  imports: [CommonModule, BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { DocenteService } from '../Services/docente.service';
import { Router } from '@angular/router';
import { Docente } from '../Model/docente';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-docente-edit-admin',
  templateUrl: './docente-edit-admin.component.html',
  styleUrls: ['./docente-edit-admin.component.css']
})
export class DocenteEditAdminComponent implements OnInit {

  constructor(private docenteService: DocenteService,
    private location: Location,
    private router: Router) { }
  loading: boolean
  docente: Docente
  iddocente = sessionStorage.getItem('iddocente')
  ngOnInit(): void {
    this.loading = true
    this.docenteService.findById(this.iddocente).subscribe(result => {
      this.docente = result
      this.loading = false
    },
      error => {
        this.loading = false
        Swal.fire({
          title: 'Ups...',
          text: 'no se pudo encontrar',
          icon: 'error'
        }).then(result => {
          this.back()
        })
      })
  }
  back() {
    this.location.back()
  }
  edit() {
    this.loading = true
    this.docenteService.save(this.docente).subscribe(reslut => {
      this.loading = false
      Swal.fire({
        title: 'Exito',
        text: 'La Información fue guardada con exito',
        icon: 'success'
      }).then(reslut => {
        this.back()
      })
    }, error => {
      this.loading = false
      Swal.fire({
        title: 'Ups...',
        icon: 'error',
        text: 'no se logro guardar la informacion'
      }).then(result => {
        this.back()
      })
    })
  }
}

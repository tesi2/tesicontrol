import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { Aula } from '../Model/aula';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AulaService {
  private URL = Properties.API_ENDPOINT + 'api/aula/'
  private TOKEN = localStorage.getItem('token')
  constructor(private http: HttpClient) { }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN }
    }
    return this.http.get<Aula[]>(this.URL + 'findAll', opt)
  }
  findById(idaula) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idaula': idaula }
    }
    return this.http.get<Aula>(this.URL + 'findById', opt)
  }
  add(aula :Aula):Observable<Aula> {
    let opt={
      headers : {'Authorization' : this.TOKEN,
      'Content-Type': 'application/json;charset=UTF-8'}
    }
    return this.http.post<Aula>(this.URL+'save',aula ,opt)
   }
   delete(aula){
    let opt={
      headers : {'Authorization' : this.TOKEN,
      'Content-Type': 'application/json;charset=UTF-8'}
    }
    return this.http.post<string>(this.URL+'delet',aula ,opt)
   }
  findByEdificio(idedificio) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idedificio': idedificio }
    }
    return this.http.get<Aula[]>(this.URL + 'findByEdificio' , opt)
  }
}

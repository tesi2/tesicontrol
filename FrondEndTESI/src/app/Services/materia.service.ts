import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { Materia } from '../Model/materia';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  constructor(private http: HttpClient) { }
  private TOKEN = localStorage.getItem('token')
  private URL = Properties.API_ENDPOINT + 'api/materia/'

  findById(idmateria) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idmateria': idmateria }
    }
    return this.http.get<Materia>(this.URL + 'findById', opt)
  }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN }
    }
    return this.http.get<Materia[]>(this.URL + 'findAll', opt)
  }
  findByCarrera(idcarrera) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idcarrera': idcarrera }
    }
    return this.http.get<Materia[]>(this.URL + 'findByCarrera', opt)
  }

  save(materia: Materia): Observable<Materia> {
    let opt = {
      headers: {
        'Authorization': this.TOKEN,
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }
    return this.http.post<Materia>(this.URL + 'save', materia, opt)
  }
  check(nombre) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'nombre': nombre }
    }
    return this.http.get<string>(this.URL + 'check', opt)
  }

  findByIddocente(iddocente) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'iddocente': iddocente }
    }
    return this.http.get<Materia[]>(this.URL + 'findByDocente', opt)
  }
}

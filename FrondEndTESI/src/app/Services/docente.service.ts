import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Docente } from '../Model/docente';
import { Properties } from '../Configuration/properties';

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

  constructor(private http: HttpClient) { }
  private TOKEN = localStorage.getItem('token');
  private URL = Properties.API_ENDPOINT + 'api/docente/'
  findByUsername(username): Observable<Docente> {
    let opt = {
      headers: { 'Authorization': this.TOKEN }, params: { 'username': username }
    }
    return this.http.get<Docente>(this.URL + 'findByUsername', opt);
  }
  findById(iddocente) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'iddocente': iddocente }
    }
    return this.http.get<Docente>(this.URL + 'findById', opt)
  }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN }
    }
    return this.http.get<Docente[]>(this.URL + 'findAll', opt)
  }
  save(docente: Docente): Observable<Docente> {
    let opt = {
      headers: { 'Authorization': this.TOKEN,
      'Content-Type': 'application/json;charset=UTF-8'
     }
    }
    return this.http.post<Docente>(this.URL + 'save', docente, opt)
  }
  add(docente: Docente): Observable<Docente> {
    console.log(this.TOKEN)
    let opt = {
      headers: { 'Authorization': this.TOKEN,
      'Content-Type': 'application/json;charset=UTF-8'
     }
    }
    return this.http.post<Docente>(this.URL + 'add', docente, opt)
  }
  check(correo){
    let opt={
      headers : {'Authorization' : this.TOKEN},
      params : {'correo' : correo}
    }
    console.log( 'revisando el correo : '+correo)
    let response =  this.http.get<string>(this.URL + 'check' , opt)
    console.log(response)
    return response
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Users } from '../Model/users';
import { Observable } from 'rxjs';
import { Properties } from '../Configuration/properties';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }
  private usuario = new Users
  //private token = localStorage.getItem('token');
  getUser(username:string):Observable<Users>{
   var  token  = localStorage.getItem('token');
    console.log("recuperando sesion de user "+username +"con el token: "+ token);
    
    return this.http.get<Users>(Properties.API_ENDPOINT+"api/users/getUser",{headers:{'Authorization':token},params:{'username':username}});
  }
}

import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { AulaService } from './aula.service';
import { Edificio } from '../Model/edificio';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EdificioService {
  private URL = Properties.API_ENDPOINT + 'api/edificio/'
  private TOKEN = localStorage.getItem('token')
  constructor(private http: HttpClient, private aulaService: AulaService) { }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN }
    }
    return this.http.get<Edificio[]>(this.URL + 'findAll', opt).pipe(
      map(result => {
        let edificios = result as Edificio[]
        return edificios.map(edificio => {
          this.aulaService.findByEdificio(edificio.idedificio).subscribe(result => {
            edificio.aulaList = result

          })
          return edificio
        })
      })
    )
  }
  findById(idedificio) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idedificio': idedificio }
    }
    return this.http.get<Edificio>(this.URL + 'findById', opt).pipe(
      map(result => {
        let edificio = result as Edificio
        this.aulaService.findByEdificio(edificio.idedificio).subscribe(result => {
          edificio.aulaList = result
        })
        return edificio;
      })
    )
  }
  add(edificio: Edificio): Observable<Edificio> {
    let opt = {
      headers: {
        'Authorization': this.TOKEN,
        'Content-Type': 'application/json;charset=UTF-8'
      }
     
    }
    console.log(edificio)
    return this.http.post<Edificio>(this.URL+'add' ,edificio,opt);
  }
  check(nombre){
    let opt = {
      headers: {
        'Authorization': this.TOKEN
      },
      params : {'nombre' : nombre}
     
    }
    return this.http.get<string>(this.URL + 'check' , opt)
  }
  delete(idedificio ,idusuario){
    let opt = {
      headers : {'Authorization' : this.TOKEN},
      params:{'idedificio' : idedificio ,'idadministratio' : idusuario}
    }
  
    return this.http.get<string>(this.URL + 'delete' , opt);
  }
}

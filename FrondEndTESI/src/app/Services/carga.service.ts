import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Carga } from '../Model/carga';
import { HttpClient } from '@angular/common/http';
import { Properties } from '../Configuration/properties';
import { map } from 'rxjs/operators';
import { formatDate, DatePipe } from '@angular/common';
import { AlumnoService } from './alumno.service';
import { ProcesoService } from './proceso.service';

@Injectable({
  providedIn: 'root'
})
export class CargaService {
  private token = localStorage.getItem('token');
  private url = Properties.API_ENDPOINT + 'api/carga/'
  constructor(private http: HttpClient, 
    private serviceAlumno: AlumnoService ,
     private ServiceProceso :ProcesoService) { }
  saveCarga(carga: Carga): Observable<Carga> {
    let opt = {
      headers: {
        'Authorization': this.token,
        'Content-Type': 'application/json;charset=UTF-8'
      }

    }
    return this.http.post<Carga>(this.url + 'saveCarga', carga, opt);
  }
  findByIdAlumno(idalumno) {
    let opt = {
      headers: {
        'Authorization': this.token
      },
      params: {
        'idalumno': idalumno.toString()
      }
    }
    return this.http.get<Carga[]>(this.url + 'findByAlumno', opt)
  }
  findAll() {
    let opt = {
      headers: {
        'Authorization': this.token
      }
    }
    return this.http.get<Carga[]>(this.url + 'findAll', opt).pipe(
      map(result => {
        let cargas = result as Carga[]
        return cargas.map(carga => {
          this.serviceAlumno.findById(carga.idalumno).subscribe(a => {
            carga.idalumno = a;
          })
          return carga;
        })
      })
    )
  }
  findById(idcarga) {
    let opt = {
      headers: { 'Authorization': this.token },
      params: { 'idcarga': idcarga.toString() }
    }
    return this.http.get<Carga>(this.url + 'findByIdcarga', opt).
    pipe(
      map (result =>{
        let carga = result  as Carga
        this.serviceAlumno.findById(carga.idalumno).subscribe(a=>{
          carga.idalumno = a
        })
        return carga;
       
      }))
  }
  update(idcarga) {
    let opt = {
      headers: {
        'Authorization': this.token
        
      },
      params : {'idcarga': idcarga.toString()}
    }
    return this.http.get<Carga>(this.url + 'checkCarga', opt)
  }
}

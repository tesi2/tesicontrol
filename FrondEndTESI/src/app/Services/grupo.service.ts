import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { Grupo } from '../Model/grupo';
import { AulaService } from './aula.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GrupoService {
private TOKEN =  localStorage.getItem('token')
private URL = Properties.API_ENDPOINT + 'api/grupos/'
  constructor(private http : HttpClient, private aulaService : AulaService ) { }
  findAll(){
    let opt = {
      headers : {'Authorization' : this.TOKEN}
    }
    return this.http.get<Grupo[]>(this.URL + 'findAll' ,opt)
  }
  findById(idgrupo){
    let opt = {
      headers : {'Authorization' : this.TOKEN},
      params : {'idgrupo' : idgrupo}
    }
    return this.http.get<Grupo>(this.URL + 'findById' , opt).pipe(
      map(result=>{
        let grupo  = result
        this.aulaService.findById(grupo.idaula).subscribe(result=>{
          grupo.idaula = result
        })
        return grupo
      })
    )
  }
  add(){}
}

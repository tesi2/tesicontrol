import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { Alumno } from '../Model/alumno';
import { Properties } from '../Configuration/properties';
import { CarreraService } from './carrera.service';
import { GrupoService } from './grupo.service';

@Injectable({
  providedIn: 'root'
})
export class AlumnoService {
  private token = localStorage.getItem('token');
  private url = Properties.API_ENDPOINT + 'api/alumno/'
  constructor(private http: HttpClient, private carreraService : CarreraService, private grupoService : GrupoService) { }
  getAlumnoByUserName(username): Observable<Alumno> {
    let opt = {
      headers: { 'Authorization': this.token },
      params: { 'username': username }
    }
    return this.http.get<Alumno>(this.url + 'findByUsername', opt);
  }
  findAll(){
    let opt = {
      headers: { 'Authorization': this.token }
    }
    return this.http.get<Alumno[]>(this.url + 'getAll' , opt).pipe(
      map(result=>{
        let alumnos = result as Alumno[]
        return alumnos.map(alumno=>{
          this.carreraService.findById(alumno.idcarrera).subscribe(result=>{
            alumno.idcarrera = result
          })
          this.grupoService.findById(alumno.idgrupo).subscribe(result=>{
            alumno.idgrupo  = result
          })
          return alumno
        })
      })
    )
  }
  findById(idalumno): Observable<Alumno> {
    let opt = {
      headers: { 'Authorization': this.token },
      params: { 'idalumno': idalumno }
    }
    return this.http.get<Alumno>(this.url + 'findById', opt).pipe(
      map(result=>{
        let alumno = result as Alumno
        this.carreraService.findById(alumno.idcarrera).subscribe(result=>{
          alumno.idcarrera = result
        })
        this.grupoService.findById(alumno.idgrupo).subscribe(result=>{
          alumno.idgrupo = result
        })
        return alumno
      })
    )
  }
  singUp(alumno: Alumno): Observable<Alumno> {
    return this.http.post<Alumno>(this.url + 'singUp', alumno);
  }

  save(alumno:Alumno):Observable<Alumno>{
    let opt = {
      headers: { 'Authorization': this.token,
      'Content-Type': 'application/json;charset=UTF-8' }
    }
    return this.http.post<Alumno>(this.url + 'save' ,alumno,opt)
  }
}

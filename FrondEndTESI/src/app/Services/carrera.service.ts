import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Properties } from '../Configuration/properties';
import { Carrera } from '../Model/carrera';

@Injectable({
  providedIn: 'root'
})
export class CarreraService {

  constructor(private http : HttpClient) { }
private URL = Properties.API_ENDPOINT + 'api/carrera/'
private TOKEN = localStorage.getItem('token')
  findById(idcarrera){
    let opt = {
      headers : {'Authorization' : this.TOKEN},
      params : {'idcarrera' : idcarrera}

    }
    return this.http.get<Carrera>(this.URL + 'findById' ,opt
    )
  }
  findAll(){
    
  return this.http.get<Carrera[]>(this.URL + 'findAll' )
  }
}

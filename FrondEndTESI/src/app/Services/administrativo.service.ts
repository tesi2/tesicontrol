import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Administrativo } from '../Model/administrativo';
import { Properties } from '../Configuration/properties';
import { map } from 'rxjs/operators';
import { AreasService } from './areas.service';
import { RolService } from './rol.service';

@Injectable({
  providedIn: 'root'
})
export class AdministrativoService {
  private TOKEN = localStorage.getItem('token');
  private URL = Properties.API_ENDPOINT + "api/administrativo/"
  constructor(private http: HttpClient, private areaService : AreasService, private rolService : RolService) { }
  findByUsername(username): Observable<Administrativo> {
    let options =
    {
      headers: { 'Authorization': this.TOKEN },
      params: { 'username': username }
    };

    return this.http.get<Administrativo>(this.URL + 'findByUsername', options)
  }
  findById(idadministrativo){
    let opt =
    {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idadministrativo': idadministrativo }
    }
    return this.http.get<Administrativo>(this.URL + 'findById' ,opt)
  }
}

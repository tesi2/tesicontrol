import { Injectable } from '@angular/core';
import {Properties} from '../Configuration/properties'
import { HttpClient } from '@angular/common/http';
import { Proceso } from '../Model/proceso';
@Injectable({
  providedIn: 'root'
})
export class ProcesoService {
private token = localStorage.getItem('token');
private url = Properties.API_ENDPOINT+'api/procesos/'
  constructor(private http:HttpClient) { }

  findAll(){
    let opt = {
      headers : {'Authorization' : this.token}
    }
    return this.http.get<Proceso[]>(this.url+'findAll',opt);
  }
  findById(idproceso){
    let opt = {
      headers : {'Authorization' : this.token},
      params : {'idproceso' : idproceso}
    }
    return this.http.get<Proceso>(this.url + 'findById' , opt)
  }
}

import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { Rol } from '../Model/rol';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  private TOKEN = localStorage.getItem('token')
  private URL = Properties.API_ENDPOINT + 'api/rol/'
  constructor(private http: HttpClient) { }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN },

    }
    return this.http.get<Rol[]>(this.URL + 'findAll', opt)
  }
  finbById(idrol) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idrol': idrol }
    }
    return this.http.get<Rol>(this.URL + 'findById', opt)
  }
}

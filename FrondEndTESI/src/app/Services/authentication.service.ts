import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Properties } from '../Configuration/properties';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http:HttpClient) {}
   autentication(data):Observable<any>{
    return this.http.post(Properties.API_ENDPOINT+'login',data)
  }
}

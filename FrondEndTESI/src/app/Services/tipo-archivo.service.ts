import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Properties } from '../Configuration/properties';
import { Observable } from 'rxjs';
import { Tipoarchivo } from '../Model/tipo-archivo';
import { map, shareReplay, mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoArchivoService {
private url = Properties.API_ENDPOINT+'api/tipoArchivo/';
private token = localStorage.getItem('token');
  constructor(private http : HttpClient) { }
  getAll(){
    let opt = {
      headers :{'Authorization':this.token}
    }
     return this.http.get<Tipoarchivo[]>(this.url+'findAll',opt); 
    }
    findById(idtipoArchivo):Observable<Tipoarchivo>{
      let opt = {
        headers :{'Authorization':this.token},
        params : {'idtipoarchivo' : idtipoArchivo}
      }
      return this.http.get<Tipoarchivo>(this.url+'findById',opt)
    }
}

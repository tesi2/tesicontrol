import { Injectable } from '@angular/core';
import { Properties } from '../Configuration/properties';
import { HttpClient } from '@angular/common/http';
import { Area } from '../Model/area';

@Injectable({
  providedIn: 'root'
})
export class AreasService {
  private TOKEN = localStorage.getItem('token')
  private URL = Properties.API_ENDPOINT + 'api/areas/'
  constructor(private http: HttpClient) { }
  findAll() {
    let opt = {
      headers: { 'Authorization': this.TOKEN }
    }
    return this.http.get<Area[]>(this.URL + 'findAll', opt)
  }
  findById(idarea) {
    let opt = {
      headers: { 'Authorization': this.TOKEN },
      params: { 'idarea': idarea }
    }
    return this.http.get<Area>(this.URL + 'findById', opt)
  }
}

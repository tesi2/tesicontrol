import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Properties } from '../Configuration/properties';
import { Archivo } from '../Model/archivo';
import { Observable } from 'rxjs';
import { Carga } from '../Model/carga';
import { map } from 'rxjs/operators';
import { TipoArchivoService } from './tipo-archivo.service';
import { Tipoarchivo } from '../Model/tipo-archivo';

@Injectable({
  providedIn: 'root'
})
export class ArchivoService {
  private TOKEN = localStorage.getItem('token');
  private URL = Properties.API_ENDPOINT + 'api/archivo/';
  constructor(private http: HttpClient,private TipoArchivoService : TipoArchivoService) { }


  saveFile(archivo: Archivo,idcarga:number):Observable<Archivo> {
    let opt = {
      headers: {
        'Authorization': this.TOKEN,
        'Content-Type': 'application/json;charset=UTF-8'
      },
      param :{'idcarga' : idcarga}
    }
    archivo.idcarga = idcarga;
  return this.http.post<Archivo>(this.URL + 'saveFile',archivo,opt);
  }
  updateFile(archivo: Archivo):Observable<Archivo>{
    let opt = {
      headers: {
        'Authorization': this.TOKEN,
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }
    return this.http.post<Archivo>(this.URL + 'update',archivo , opt);
  }
  findByCarga(idcarga:string){
    let opt = {
      headers: {
        'Authorization': this.TOKEN
      },
      params :{
        'idcarga' : idcarga}
    }
    return this.http.get<Archivo[]>(this.URL + 'findByCarga' ,opt).pipe(
      map(result =>{
          let archivos = result as Archivo[]
         return archivos.map(archivo=>{
            let tp 
             this.TipoArchivoService.findById(archivo.idtipoarchivo).subscribe(res=>{
              archivo.idtipoarchivo = res;
               console.log (res)
             })
            
            return archivo
          }) 
      })
    )
  }
}

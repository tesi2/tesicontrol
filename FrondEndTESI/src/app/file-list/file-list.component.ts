import { Component, OnInit, OnDestroy } from '@angular/core';
import { ArchivoService } from '../Services/archivo.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Location } from '@angular/common';
import { Archivo } from '../Model/archivo';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit,OnDestroy {

  constructor(private location: Location,private archivoService : ArchivoService ,private router : Router) { }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
private idcarga;
loading : boolean
 archivoList:Archivo[]
 dtOption :DataTables.Settings = {
  language : {
    url : '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
  }
 }
 dtTrigger = new Subject()
  ngOnInit(): void {
    this.loading=true
    this.idcarga = localStorage.getItem('idcarga');
    if(this.idcarga==null||this.idcarga== undefined){
      this.router.navigate[('cargaAlumnoList')];
    }
    this.archivoService.findByCarga(this.idcarga).subscribe(result=>{
      this.archivoList = result
      this.dtTrigger.next();
      this.loading = false
    })
    
  }
deletArchivo(i){
Swal.fire({
  title : 'confirmar',
  icon: 'warning',
  text : 'Desea eliminar este archivo? control escolar no se hara responsable si el edocumento no ha sido revisado',
  showCancelButton : true,
  confirmButtonText : 'Eliminar de la carga',
  cancelButtonText : 'No'
})
}
sync(i){

}
view(i){
  let archivo = this.archivoList[i]
  Swal.fire({
    title : 'Archivo: '+archivo.nombre,
    html : '<img src='+archivo.contenido+'></img>'
  })
}
back(){
  this.location.back();
}
}

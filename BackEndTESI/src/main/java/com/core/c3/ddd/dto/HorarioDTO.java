/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.ddd.dto;

import com.core.c3.dao.entity.Aulas;
import com.core.c3.dao.entity.Docentes;
import com.core.c3.dao.entity.Docentesmaterias;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.dao.entity.Horarios;
import com.core.c3.dao.entity.Materias;

/**
 *
 * @author balam
 */
public class HorarioDTO {
    
    private Horarios horario;
    private Docentesmaterias docentesmaterias;
    private Docentes docente;
    private Materias materia;
    private Aulas aula;
    private Grupos grupo;

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }

    public Docentesmaterias getDocentesmaterias() {
        return docentesmaterias;
    }

    public void setDocentesmaterias(Docentesmaterias docentesmaterias) {
        this.docentesmaterias = docentesmaterias;
    }

    public Docentes getDocente() {
        return docente;
    }

    public void setDocente(Docentes docente) {
        this.docente = docente;
    }

    public Materias getMateria() {
        return materia;
    }

    public void setMateria(Materias materia) {
        this.materia = materia;
    }

    public Aulas getAula() {
        return aula;
    }

    public void setAula(Aulas aula) {
        this.aula = aula;
    }

    public Grupos getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupos grupo) {
        this.grupo = grupo;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "docentesmaterias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Docentesmaterias.findAll", query = "SELECT d FROM Docentesmaterias d"),
    @NamedQuery(name = "Docentesmaterias.findByIddocentesmateriascol", query = "SELECT d FROM Docentesmaterias d WHERE d.iddocentesmateriascol = :iddocentesmateriascol")})
public class Docentesmaterias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddocentesmateriascol")
    private Integer iddocentesmateriascol;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddocentesmateriascol", fetch = FetchType.LAZY)
    private List<Horarios> horariosList;
    @JoinColumn(name = "iddocente", referencedColumnName = "iddocente")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Docentes iddocente;
    @JoinColumn(name = "idmateria", referencedColumnName = "idmateria")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Materias idmateria;

    public Docentesmaterias() {
    }

    public Docentesmaterias(Integer iddocentesmateriascol) {
        this.iddocentesmateriascol = iddocentesmateriascol;
    }

    public Integer getIddocentesmateriascol() {
        return iddocentesmateriascol;
    }

    public void setIddocentesmateriascol(Integer iddocentesmateriascol) {
        this.iddocentesmateriascol = iddocentesmateriascol;
    }

    @XmlTransient
    @JsonIgnore
    public List<Horarios> getHorariosList() {
        return horariosList;
    }

    public void setHorariosList(List<Horarios> horariosList) {
        this.horariosList = horariosList;
    }

    public Docentes getIddocente() {
        return iddocente;
    }

    public void setIddocente(Docentes iddocente) {
        this.iddocente = iddocente;
    }

    public Materias getIdmateria() {
        return idmateria;
    }

    public void setIdmateria(Materias idmateria) {
        this.idmateria = idmateria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocentesmateriascol != null ? iddocentesmateriascol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Docentesmaterias)) {
            return false;
        }
        Docentesmaterias other = (Docentesmaterias) object;
        if ((this.iddocentesmateriascol == null && other.iddocentesmateriascol != null) || (this.iddocentesmateriascol != null && !this.iddocentesmateriascol.equals(other.iddocentesmateriascol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Docentesmaterias[ iddocentesmateriascol=" + iddocentesmateriascol + " ]";
    }
    
}

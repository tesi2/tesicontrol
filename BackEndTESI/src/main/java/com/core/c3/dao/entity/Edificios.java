/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "edificios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Edificios.findAll", query = "SELECT e FROM Edificios e"),
    @NamedQuery(name = "Edificios.findByIdedificio", query = "SELECT e FROM Edificios e WHERE e.idedificio = :idedificio"),
    @NamedQuery(name = "Edificios.findByNombre", query = "SELECT e FROM Edificios e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Edificios.findByUbicacion", query = "SELECT e FROM Edificios e WHERE e.ubicacion = :ubicacion")})
public class Edificios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idedificio")
    private Integer idedificio;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "ubicacion")
    private String ubicacion;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idedificio", fetch = FetchType.LAZY)
    private List<Aulas> aulasList;

    public Edificios() {
    }

    public Edificios(Integer idedificio) {
        this.idedificio = idedificio;
    }

    public Integer getIdedificio() {
        return idedificio;
    }

    public void setIdedificio(Integer idedificio) {
        this.idedificio = idedificio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @XmlTransient
    @JsonIgnore
    public List<Aulas> getAulasList() {
        return aulasList;
    }

    public void setAulasList(List<Aulas> aulasList) {
        this.aulasList = aulasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idedificio != null ? idedificio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Edificios)) {
            return false;
        }
        Edificios other = (Edificios) object;
        if ((this.idedificio == null && other.idedificio != null) || (this.idedificio != null && !this.idedificio.equals(other.idedificio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Edificios[ idedificio=" + idedificio + " ]";
    }

    public static Edificios fromId(Integer idedificio) {
        Edificios edificio = new Edificios();
        edificio.setIdedificio(idedificio);
        return edificio;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author balam
 */
@Entity
@Table(name = "servidorcorreo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servidorcorreo.findAll", query = "SELECT s FROM Servidorcorreo s"),
    @NamedQuery(name = "Servidorcorreo.findByIdservidorcorreo", query = "SELECT s FROM Servidorcorreo s WHERE s.idservidorcorreo = :idservidorcorreo"),
    @NamedQuery(name = "Servidorcorreo.findByHost", query = "SELECT s FROM Servidorcorreo s WHERE s.host = :host"),
    @NamedQuery(name = "Servidorcorreo.findByPuerto", query = "SELECT s FROM Servidorcorreo s WHERE s.puerto = :puerto"),
    @NamedQuery(name = "Servidorcorreo.findBySeguridad", query = "SELECT s FROM Servidorcorreo s WHERE s.seguridad = :seguridad"),
    @NamedQuery(name = "Servidorcorreo.findByCuenta", query = "SELECT s FROM Servidorcorreo s WHERE s.cuenta = :cuenta"),
    @NamedQuery(name = "Servidorcorreo.findByClave", query = "SELECT s FROM Servidorcorreo s WHERE s.clave = :clave"),
    @NamedQuery(name = "Servidorcorreo.findByTipo", query = "SELECT s FROM Servidorcorreo s WHERE s.tipo = :tipo")})

public class Servidorcorreo implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idservidorcorreo")
    private Integer idservidorcorreo;
    @Size(max = 45)
    @Column(name = "host")
    private String host;
    @Column(name = "puerto")
    private Integer puerto;
    @Column(name = "seguridad")
    private String seguridad;
    @Size(max = 100)
    @Column(name = "cuenta")
    private String cuenta;
    @Size(max = 100)
    @Column(name = "clave")
    private String clave;
    @Column(name = "conteo")
    private Integer conteo;
    @Column(name = "tipo")
    private String tipo;  
    
    public Servidorcorreo() {
    }
    
    public Servidorcorreo(Integer idservidorcorreo) {
        this.idservidorcorreo = idservidorcorreo;
    }

    public Integer getIdservidorcorreo() {
        return idservidorcorreo;
    }

    public void setIdservidorcorreo(Integer idservidorcorreo) {
        this.idservidorcorreo = idservidorcorreo;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPuerto() {
        return puerto;
    }

    public void setPuerto(Integer puerto) {
        this.puerto = puerto;
    }

    public String getSeguridad() {
        return seguridad;
    }

    public void setSeguridad(String seguridad) {
        this.seguridad = seguridad;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Integer getConteo() {
        return conteo;
    }

    public void setConteo(Integer conteo) {
        this.conteo = conteo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idservidorcorreo != null ? idservidorcorreo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Servidorcorreo)) {
            return false;
        }
        Servidorcorreo other = (Servidorcorreo) object;
        if ((this.idservidorcorreo == null && other.idservidorcorreo != null) || (this.idservidorcorreo != null && !this.idservidorcorreo.equals(other.idservidorcorreo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Servidorcorreo[ idservidorcorreo=" + idservidorcorreo + " ]";
    }
}

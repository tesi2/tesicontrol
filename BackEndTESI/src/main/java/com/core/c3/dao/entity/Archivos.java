/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "archivos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Archivos.findAll", query = "SELECT a FROM Archivos a"),
    @NamedQuery(name = "Archivos.findByIdarchivo", query = "SELECT a FROM Archivos a WHERE a.idarchivo = :idarchivo"),
    @NamedQuery(name = "Archivos.findByFechacarga", query = "SELECT a FROM Archivos a WHERE a.fechacarga = :fechacarga"),
    @NamedQuery(name = "Archivos.findByAprobado", query = "SELECT a FROM Archivos a WHERE a.aprobado = :aprobado"),
    @NamedQuery(name = "Archivos.findByNombre", query = "SELECT a FROM Archivos a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Archivos.findByRevisado", query = "SELECT a FROM Archivos a WHERE a.revisado = :revisado")})
public class Archivos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idarchivo")
    private Integer idarchivo;
    @Column(name = "fechacarga")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacarga;
    @Lob
    @Size(max = 16777215)
    @Column(name = "contenido")
    private String contenido;
    @Column(name = "aprobado")
    private Boolean aprobado;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "revisado")
    private Boolean revisado;
    @Column(name = "comentario")
    private String comentario;
    @JsonProperty("idcarga")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idcarga")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idcarga", referencedColumnName = "idcarga")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Carga idcarga;
    @JsonProperty("idtipoarchivo")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idtipoarchivo")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idtipoarchivo", referencedColumnName = "idtipoarchivo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tipoarchivo idtipoarchivo;

    public Archivos() {
    }

    public Archivos(Integer idarchivo) {
        this.idarchivo = idarchivo;
    }

    public Integer getIdarchivo() {
        return idarchivo;
    }

    public void setIdarchivo(Integer idarchivo) {
        this.idarchivo = idarchivo;
    }

    public Date getFechacarga() {
        return fechacarga;
    }

    public void setFechacarga(Date fechacarga) {
        this.fechacarga = fechacarga;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Boolean getAprobado() {
        return aprobado;
    }

    public void setAprobado(Boolean aprobado) {
        this.aprobado = aprobado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getRevisado() {
        return revisado;
    }

    public void setRevisado(Boolean revisado) {
        this.revisado = revisado;
    }

    public Carga getIdcarga() {
        return idcarga;
    }

    public void setIdcarga(Carga idcarga) {
        this.idcarga = idcarga;
    }

    public Tipoarchivo getIdtipoarchivo() {
        return idtipoarchivo;
    }

    public void setIdtipoarchivo(Tipoarchivo idtipoarchivo) {
        this.idtipoarchivo = idtipoarchivo;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idarchivo != null ? idarchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Archivos)) {
            return false;
        }
        Archivos other = (Archivos) object;
        if ((this.idarchivo == null && other.idarchivo != null) || (this.idarchivo != null && !this.idarchivo.equals(other.idarchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Archivos[ idarchivo=" + idarchivo + " ]";
    }

    @JsonProperty("idcarga")
    public void setCarga(Integer pIdCarga) {
        idcarga = Carga.fromId(pIdCarga);
    }
}

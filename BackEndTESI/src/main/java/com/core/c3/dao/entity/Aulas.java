/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "aulas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aulas.findAll", query = "SELECT a FROM Aulas a"),
    @NamedQuery(name = "Aulas.findByIdaula", query = "SELECT a FROM Aulas a WHERE a.idaula = :idaula"),
    @NamedQuery(name = "Aulas.findByNombre", query = "SELECT a FROM Aulas a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Aulas.findByTipo", query = "SELECT a FROM Aulas a WHERE a.tipo = :tipo")})
public class Aulas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idaula")
    private Integer idaula;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "tipo")
    private String tipo;
    @JsonProperty("idedificio")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idedificio")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idedificio", referencedColumnName = "idedificio")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Edificios idedificio;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idaula", fetch = FetchType.LAZY)
    private List<Grupos> gruposList;

    public Aulas() {
    }

    public Aulas(Integer idaula) {
        this.idaula = idaula;
    }

    public Integer getIdaula() {
        return idaula;
    }

    public void setIdaula(Integer idaula) {
        this.idaula = idaula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Edificios getIdedificio() {
        return idedificio;
    }

    public void setIdedificio(Edificios idedificio) {
        this.idedificio = idedificio;
    }

    @XmlTransient
    @JsonIgnore
    public List<Grupos> getGruposList() {
        return gruposList;
    }

    public void setGruposList(List<Grupos> gruposList) {
        this.gruposList = gruposList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idaula != null ? idaula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aulas)) {
            return false;
        }
        Aulas other = (Aulas) object;
        if ((this.idaula == null && other.idaula != null) || (this.idaula != null && !this.idaula.equals(other.idaula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Aulas[ idaula=" + idaula + " ]";
    }

    @JsonProperty("idedificio")
    public void setEdificio(Integer pidedificio) {
        idedificio = Edificios.fromId(pidedificio);
    }

}

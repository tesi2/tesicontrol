/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "procesos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procesos.findAll", query = "SELECT p FROM Procesos p"),
    @NamedQuery(name = "Procesos.findByIdproceso", query = "SELECT p FROM Procesos p WHERE p.idproceso = :idproceso"),
    @NamedQuery(name = "Procesos.findByNombre", query = "SELECT p FROM Procesos p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Procesos.findByValido", query = "SELECT p FROM Procesos p WHERE p.valido = :valido"),
    @NamedQuery(name = "Procesos.findByFechainicio", query = "SELECT p FROM Procesos p WHERE p.fechainicio = :fechainicio"),
    @NamedQuery(name = "Procesos.findByFechafinal", query = "SELECT p FROM Procesos p WHERE p.fechafinal = :fechafinal")})
public class Procesos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idproceso")
    private Integer idproceso;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "valido")
    private Boolean valido;
    @Column(name = "fechainicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechainicio;
    @Column(name = "fechafinal")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechafinal;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idproceso", fetch = FetchType.LAZY)
    private List<Carga> cargaList;

    public Procesos() {
    }

    public Procesos(Integer idproceso) {
        this.idproceso = idproceso;
    }

    public Integer getIdproceso() {
        return idproceso;
    }

    public void setIdproceso(Integer idproceso) {
        this.idproceso = idproceso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getValido() {
        return valido;
    }

    public void setValido(Boolean valido) {
        this.valido = valido;
    }

    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    public Date getFechafinal() {
        return fechafinal;
    }

    public void setFechafinal(Date fechafinal) {
        this.fechafinal = fechafinal;
    }

    @XmlTransient
    @JsonIgnore
    public List<Carga> getCargaList() {
        return cargaList;
    }

    public void setCargaList(List<Carga> cargaList) {
        this.cargaList = cargaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproceso != null ? idproceso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procesos)) {
            return false;
        }
        Procesos other = (Procesos) object;
        if ((this.idproceso == null && other.idproceso != null) || (this.idproceso != null && !this.idproceso.equals(other.idproceso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Procesos[ idproceso=" + idproceso + " ]";
    }
 public static Procesos fromId(Integer pIdProcesos){
    Procesos proceso = new Procesos();
    proceso.setIdproceso(pIdProcesos);
    return proceso;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author balam
 */
@Entity
@Table(name = "colacorreos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Colacorreos.findAll", query = "SELECT c FROM Colacorreos c"),
    @NamedQuery(name = "Colacorreos.findByIdcolacorreos", query = "SELECT c FROM Colacorreos c WHERE c.idcolacorreos = :idcolacorreos"),
    @NamedQuery(name = "Colacorreos.findByReceptores", query = "SELECT c FROM Colacorreos c WHERE c.receptores = :receptores"),
    @NamedQuery(name = "Colacorreos.findByCopias", query = "SELECT c FROM Colacorreos c WHERE c.copias = :copias"),
    @NamedQuery(name = "Colacorreos.findByCcpo", query = "SELECT c FROM Colacorreos c WHERE c.ccpo = :ccpo"),
    @NamedQuery(name = "Colacorreos.findByAsunto", query = "SELECT c FROM Colacorreos c WHERE c.asunto = :asunto"),
    @NamedQuery(name = "Colacorreos.findByTipocuerpo", query = "SELECT c FROM Colacorreos c WHERE c.tipocuerpo = :tipocuerpo"),
    @NamedQuery(name = "Colacorreos.findByCuerpo", query = "SELECT c FROM Colacorreos c WHERE c.cuerpo = :cuerpo"),
    @NamedQuery(name = "Colacorreos.findByEnviado", query = "SELECT c FROM Colacorreos c WHERE c.enviado = :enviado"),
    @NamedQuery(name = "Colacorreos.findByFechageneracion", query = "SELECT c FROM Colacorreos c WHERE c.fechageneracion = :fechageneracion"),
    @NamedQuery(name = "Colacorreos.findByFechaenvio", query = "SELECT c FROM Colacorreos c WHERE c.fechaenvio = :fechaenvio")})
public class Colacorreos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcolacorreos")
    private Integer idcolacorreos;
    @NotNull
    @Size(max = 2000)
    @Column(name = "receptores")
    private String receptores;
    @Size(max = 2000)
    @Column(name = "copias")
    private String copias;
    @Size(max = 2000)
    @Column(name = "ccpo")
    private String ccpo;
    @NotNull
    @Size(max = 500)
    @Column(name = "asunto")
    private String asunto;
    @NotNull
    @Column(name = "tipocuerpo")
    private String tipocuerpo; 
    @NotNull
    @Column(name = "cuerpo")
    private String cuerpo;
    @Column(name = "enviado")
    private boolean enviado;
    @Column(name = "fechageneracion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechageneracion;
    @NotNull
    @Column(name = "fechaenvio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaenvio;
    
    public Colacorreos() {
    }
    
    public Colacorreos(Integer idcolacorreos) {
        this.idcolacorreos = idcolacorreos;
    }

    public Integer getIdcolacorreos() {
        return idcolacorreos;
    }

    public void setIdcolacorreos(Integer idcolacorreos) {
        this.idcolacorreos = idcolacorreos;
    }

    public String getReceptores() {
        return receptores;
    }

    public void setReceptores(String receptores) {
        this.receptores = receptores;
    }

    public String getCopias() {
        return copias;
    }

    public void setCopias(String copias) {
        this.copias = copias;
    }

    public String getCcpo() {
        return ccpo;
    }

    public void setCcpo(String ccpo) {
        this.ccpo = ccpo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getTipocuerpo() {
        return tipocuerpo;
    }

    public void setTipocuerpo(String tipocuerpo) {
        this.tipocuerpo = tipocuerpo;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    public Date getFechageneracion() {
        return fechageneracion;
    }

    public void setFechageneracion(Date fechageneracion) {
        this.fechageneracion = fechageneracion;
    }

    public Date getFechaenvio() {
        return fechaenvio;
    }

    public void setFechaenvio(Date fechaenvio) {
        this.fechaenvio = fechaenvio;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcolacorreos != null ? idcolacorreos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Colacorreos)) {
            return false;
        }
        Colacorreos other = (Colacorreos) object;
        if ((this.idcolacorreos == null && other.idcolacorreos != null) || (this.idcolacorreos != null && !this.idcolacorreos.equals(other.idcolacorreos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Colacorreos[ idcolacorreos=" + idcolacorreos + " ]";
    }
}

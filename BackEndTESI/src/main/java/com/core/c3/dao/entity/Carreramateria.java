/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "carreramateria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carreramateria.findAll", query = "SELECT c FROM Carreramateria c"),
    @NamedQuery(name = "Carreramateria.findByIdcarreramateria", query = "SELECT c FROM Carreramateria c WHERE c.idcarreramateria = :idcarreramateria")})
public class Carreramateria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcarreramateria")
    private Integer idcarreramateria;
    @JoinColumn(name = "idcarrera", referencedColumnName = "idcarrera")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Carreras idcarrera;
    @JoinColumn(name = "idmateria", referencedColumnName = "idmateria")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Materias idmateria;

    public Carreramateria() {
    }

    public Carreramateria(Integer idcarreramateria) {
        this.idcarreramateria = idcarreramateria;
    }

    public Integer getIdcarreramateria() {
        return idcarreramateria;
    }

    public void setIdcarreramateria(Integer idcarreramateria) {
        this.idcarreramateria = idcarreramateria;
    }

    public Carreras getIdcarrera() {
        return idcarrera;
    }

    public void setIdcarrera(Carreras idcarrera) {
        this.idcarrera = idcarrera;
    }

    public Materias getIdmateria() {
        return idmateria;
    }

    public void setIdmateria(Materias idmateria) {
        this.idmateria = idmateria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcarreramateria != null ? idcarreramateria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carreramateria)) {
            return false;
        }
        Carreramateria other = (Carreramateria) object;
        if ((this.idcarreramateria == null && other.idcarreramateria != null) || (this.idcarreramateria != null && !this.idcarreramateria.equals(other.idcarreramateria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Carreramateria[ idcarreramateria=" + idcarreramateria + " ]";
    }
    
}

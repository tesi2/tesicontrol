/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "grupos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupos.findAll", query = "SELECT g FROM Grupos g"),
    @NamedQuery(name = "Grupos.findByIdgrupo", query = "SELECT g FROM Grupos g WHERE g.idgrupo = :idgrupo"),
    @NamedQuery(name = "Grupos.findByTurno", query = "SELECT g FROM Grupos g WHERE g.turno = :turno"),
    @NamedQuery(name = "Grupos.findByCodigo", query = "SELECT g FROM Grupos g WHERE g.codigo = :codigo")})
public class Grupos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgrupo")
    private Integer idgrupo;
    @Size(max = 10)
    @Column(name = "turno")
    private String turno;
    @Column(name="activo")
    private boolean activo;
    @Size(max = 45)
    @Column(name = "codigo")
    private String codigo;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupo", fetch = FetchType.LAZY)
    private List<Alumnos> alumnosList;
     @JsonProperty("idaula")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idaula")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idaula", referencedColumnName = "idaula")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Aulas idaula;
    @JsonProperty("idcarrera")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idcarrera")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idcarrera", referencedColumnName = "idcarrera")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Carreras idcarrera;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupo", fetch = FetchType.LAZY)
    private List<Horarios> horariosList;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idgrupo" , fetch = FetchType.LAZY)
    private List<Materiasgrupos> materiasgruposList;

    public Grupos() {
    }

    public Grupos(Integer idgrupo) {
        this.idgrupo = idgrupo;
    }

    public Integer getIdgrupo() {
        return idgrupo;
    }

    public void setIdgrupo(Integer idgrupo) {
        this.idgrupo = idgrupo;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @XmlTransient
    @JsonIgnore
    public List<Alumnos> getAlumnosList() {
        return alumnosList;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    

    public void setAlumnosList(List<Alumnos> alumnosList) {
        this.alumnosList = alumnosList;
    }

    public Aulas getIdaula() {
        return idaula;
    }

    public void setIdaula(Aulas idaula) {
        this.idaula = idaula;
    }

    public Carreras getIdcarrera() {
        return idcarrera;
    }

    public void setIdcarrera(Carreras idcarrera) {
        this.idcarrera = idcarrera;
    }

    @XmlTransient
    @JsonIgnore
    public List<Horarios> getHorariosList() {
        return horariosList;
    }

    public void setHorariosList(List<Horarios> horariosList) {
        this.horariosList = horariosList;
    }
    
    @XmlTransient
    @JsonIgnore
    public List<Materiasgrupos> getMateriasgruposList() {
        return materiasgruposList;
    }

    public void setMateriasgruposList(List<Materiasgrupos> materiasgruposList) {
        this.materiasgruposList = materiasgruposList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgrupo != null ? idgrupo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupos)) {
            return false;
        }
        Grupos other = (Grupos) object;
        if ((this.idgrupo == null && other.idgrupo != null) || (this.idgrupo != null && !this.idgrupo.equals(other.idgrupo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Grupos[ idgrupo=" + idgrupo + " ]";
    }

    public static Grupos fromId(Integer pIdGrupo) {
        Grupos grupo = new Grupos();
        grupo.setIdgrupo(pIdGrupo);
        return grupo;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "carga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carga.findAll", query = "SELECT c FROM Carga c"),
    @NamedQuery(name = "Carga.findByIdcarga", query = "SELECT c FROM Carga c WHERE c.idcarga = :idcarga"),
    @NamedQuery(name = "Carga.findByFechacarga", query = "SELECT c FROM Carga c WHERE c.fechacarga = :fechacarga"),
    @NamedQuery(name = "Carga.findByRevisada", query = "SELECT c FROM Carga c WHERE c.revisada = :revisada")})
public class Carga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcarga")
    private Integer idcarga;
    @Column(name = "fechacarga")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacarga;
    @Column(name = "revisada")
    private Boolean revisada;
    @JsonProperty("idalumno")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idalumno")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idalumno", referencedColumnName = "idalumno")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Alumnos idalumno;
    @JsonProperty("idproceso")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idproceso")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idproceso", referencedColumnName = "idproceso")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Procesos idproceso;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcarga", fetch = FetchType.LAZY)
    private List<Archivos> archivosList;

    public Carga() {
    }

    public Carga(Integer idcarga) {
        this.idcarga = idcarga;
    }

    public Integer getIdcarga() {
        return idcarga;
    }

    public void setIdcarga(Integer idcarga) {
        this.idcarga = idcarga;
    }

    public Date getFechacarga() {
        return fechacarga;
    }

    public void setFechacarga(Date fechacarga) {
        this.fechacarga = fechacarga;
    }

    public Boolean getRevisada() {
        return revisada;
    }

    public void setRevisada(Boolean revisada) {
        this.revisada = revisada;
    }

    public Alumnos getIdalumno() {
        return idalumno;
    }

    public void setIdalumno(Alumnos idalumno) {
        this.idalumno = idalumno;
    }

    public Procesos getIdproceso() {
        return idproceso;
    }

    public void setIdproceso(Procesos idproceso) {
        this.idproceso = idproceso;
    }

    @XmlTransient
    @JsonIgnore
    public List<Archivos> getArchivosList() {
        return archivosList;
    }

    public void setArchivosList(List<Archivos> archivosList) {
        this.archivosList = archivosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcarga != null ? idcarga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carga)) {
            return false;
        }
        Carga other = (Carga) object;
        if ((this.idcarga == null && other.idcarga != null) || (this.idcarga != null && !this.idcarga.equals(other.idcarga))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Carga[ idcarga=" + idcarga + " ]";
    }
@JsonProperty("idproceso")
    public void setproceso(Integer pIdProceso) {
        this.idproceso = Procesos.fromId(pIdProceso);
    }
    public static Carga fromId(Integer pIdCarga) {
        Carga carga = new Carga();
        carga.setIdcarga(pIdCarga);
        return carga;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "docentes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Docentes.findAll", query = "SELECT d FROM Docentes d"),
    @NamedQuery(name = "Docentes.findByIddocente", query = "SELECT d FROM Docentes d WHERE d.iddocente = :iddocente"),
    @NamedQuery(name = "Docentes.findByCedula", query = "SELECT d FROM Docentes d WHERE d.cedula = :cedula"),
    @NamedQuery(name = "Docentes.findByMatricula", query = "SELECT d FROM Docentes d WHERE d.matricula = :matricula"),
    @NamedQuery(name = "Docentes.findByNombre", query = "SELECT d FROM Docentes d WHERE d.nombre = :nombre"),
    @NamedQuery(name = "Docentes.findByPaterno", query = "SELECT d FROM Docentes d WHERE d.paterno = :paterno"),
    @NamedQuery(name = "Docentes.findByMaterno", query = "SELECT d FROM Docentes d WHERE d.materno = :materno"),
    @NamedQuery(name = "Docentes.findByTelefono", query = "SELECT d FROM Docentes d WHERE d.telefono = :telefono"),
    @NamedQuery(name = "Docentes.findByCorreo", query = "SELECT d FROM Docentes d WHERE d.correo = :correo"),
    @NamedQuery(name = "Docentes.findByNss", query = "SELECT d FROM Docentes d WHERE d.nss = :nss")})
public class Docentes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddocente")
    private Integer iddocente;
    @Size(max = 45)
    @Column(name = "cedula")
    private String cedula;
    @Size(max = 45)
    @Column(name = "matricula")
    private String matricula;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "paterno")
    private String paterno;
    @Size(max = 45)
    @Column(name = "materno")
    private String materno;
    @Size(max = 45)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 45)
    @Column(name = "correo")
    private String correo;
    @Size(max = 45)
    @Column(name = "nss")
    private String nss;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddocente", fetch = FetchType.LAZY)
    private List<Docentesmaterias> docentesmateriasList;

    public Docentes() {
    }

    public Docentes(Integer iddocente) {
        this.iddocente = iddocente;
    }

    public Integer getIddocente() {
        return iddocente;
    }

    public void setIddocente(Integer iddocente) {
        this.iddocente = iddocente;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    @XmlTransient
    @JsonIgnore
    public List<Docentesmaterias> getDocentesmateriasList() {
        return docentesmateriasList;
    }

    public void setDocentesmateriasList(List<Docentesmaterias> docentesmateriasList) {
        this.docentesmateriasList = docentesmateriasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocente != null ? iddocente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Docentes)) {
            return false;
        }
        Docentes other = (Docentes) object;
        if ((this.iddocente == null && other.iddocente != null) || (this.iddocente != null && !this.iddocente.equals(other.iddocente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Docentes[ iddocente=" + iddocente + " ]";
    }
    
}

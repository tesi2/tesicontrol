/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "administrativos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Administrativos.findAll", query = "SELECT a FROM Administrativos a"),
    @NamedQuery(name = "Administrativos.findByIdadministrativo", query = "SELECT a FROM Administrativos a WHERE a.idadministrativo = :idadministrativo"),
    @NamedQuery(name = "Administrativos.findByNombre", query = "SELECT a FROM Administrativos a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Administrativos.findByPaterno", query = "SELECT a FROM Administrativos a WHERE a.paterno = :paterno"),
    @NamedQuery(name = "Administrativos.findByMaterno", query = "SELECT a FROM Administrativos a WHERE a.materno = :materno"),
    @NamedQuery(name = "Administrativos.findByTelefono", query = "SELECT a FROM Administrativos a WHERE a.telefono = :telefono"),
    @NamedQuery(name = "Administrativos.findByCorreo", query = "SELECT a FROM Administrativos a WHERE a.correo = :correo")})
public class Administrativos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idadministrativo")
    private Integer idadministrativo;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "paterno")
    private String paterno;
    @Size(max = 45)
    @Column(name = "materno")
    private String materno;
    @Size(max = 45)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 45)
    @Column(name = "correo")
    private String correo;
    @JsonProperty("idarea")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idarea")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idarea", referencedColumnName = "idarea")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Areas idarea;
    @JsonProperty("idrol")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idrol")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "idrol", referencedColumnName = "idrol")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Roles idrol;

    public Administrativos() {
    }

    public Administrativos(Integer idadministrativo) {
        this.idadministrativo = idadministrativo;
    }

    public Integer getIdadministrativo() {
        return idadministrativo;
    }

    public void setIdadministrativo(Integer idadministrativo) {
        this.idadministrativo = idadministrativo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Areas getIdarea() {
        return idarea;
    }

    public void setIdarea(Areas idarea) {
        this.idarea = idarea;
    }

    public Roles getIdrol() {
        return idrol;
    }

    public void setIdrol(Roles idrol) {
        this.idrol = idrol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idadministrativo != null ? idadministrativo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Administrativos)) {
            return false;
        }
        Administrativos other = (Administrativos) object;
        if ((this.idadministrativo == null && other.idadministrativo != null) || (this.idadministrativo != null && !this.idadministrativo.equals(other.idadministrativo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Administrativos[ idadministrativo=" + idadministrativo + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "tipoarchivo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoarchivo.findAll", query = "SELECT t FROM Tipoarchivo t"),
    @NamedQuery(name = "Tipoarchivo.findByIdtipoarchivo", query = "SELECT t FROM Tipoarchivo t WHERE t.idtipoarchivo = :idtipoarchivo"),
    @NamedQuery(name = "Tipoarchivo.findByTipo", query = "SELECT t FROM Tipoarchivo t WHERE t.tipo = :tipo")})
public class Tipoarchivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipoarchivo")
    private Integer idtipoarchivo;
    @Size(max = 45)
    @Column(name = "tipo")
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipoarchivo", fetch = FetchType.LAZY)
    private List<Archivos> archivosList;

    public Tipoarchivo() {
    }

    public Tipoarchivo(Integer idtipoarchivo) {
        this.idtipoarchivo = idtipoarchivo;
    }

    public Integer getIdtipoarchivo() {
        return idtipoarchivo;
    }

    public void setIdtipoarchivo(Integer idtipoarchivo) {
        this.idtipoarchivo = idtipoarchivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    @JsonIgnore
    public List<Archivos> getArchivosList() {
        return archivosList;
    }

    public void setArchivosList(List<Archivos> archivosList) {
        this.archivosList = archivosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoarchivo != null ? idtipoarchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipoarchivo)) {
            return false;
        }
        Tipoarchivo other = (Tipoarchivo) object;
        if ((this.idtipoarchivo == null && other.idtipoarchivo != null) || (this.idtipoarchivo != null && !this.idtipoarchivo.equals(other.idtipoarchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Tipoarchivo[ idtipoarchivo=" + idtipoarchivo + " ]";
    }
    
}

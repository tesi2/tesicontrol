/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author balam
 */
@Entity
@Table(name = "materiasgrupos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materiasgrupos.findAll", query = "SELECT m FROM Materiasgrupos m"),
    @NamedQuery(name = "Materiasgrupos.findByIdmateriasgrupos", query = "SELECT m FROM Materiasgrupos m WHERE m.idmateriasgrupos = :idmateriasgrupos")})
public class Materiasgrupos implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmateriasgrupos")
    private Integer idmateriasgrupos;
    @JoinColumn(name = "idmateria", referencedColumnName = "idmateria")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Materias idmateria;
    @JoinColumn(name = "idgrupo", referencedColumnName = "idgrupo")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Grupos idgrupo;
    
    public Materiasgrupos() {
    }
    
    public Materiasgrupos(Integer idmateriasgrupos) {
        this.idmateriasgrupos = idmateriasgrupos;
    }

    public Integer getIdmateriasgrupos() {
        return idmateriasgrupos;
    }

    public void setIdmateriasgrupos(Integer idmateriasgrupos) {
        this.idmateriasgrupos = idmateriasgrupos;
    }
    
    public Materias getIdmateria() {
        return idmateria;
    }

    public void setIdmateria(Materias idmateria) {
        this.idmateria = idmateria;
    }

    public Grupos getIdgrupo() {
        return idgrupo;
    }

    public void setIdgrupo(Grupos idgrupo) {
        this.idgrupo = idgrupo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmateriasgrupos != null ? idmateriasgrupos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Materiasgrupos)) {
            return false;
        }
        Materiasgrupos other = (Materiasgrupos) object;
        if ((this.idmateriasgrupos == null && other.idmateriasgrupos != null) || (this.idmateriasgrupos != null && !this.idmateriasgrupos.equals(other.idmateriasgrupos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Materiasgrupos[ idmateriasgrupos=" + idmateriasgrupos + " ]";
    }    
}

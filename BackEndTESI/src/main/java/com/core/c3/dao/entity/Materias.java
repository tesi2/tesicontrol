/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c3.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alan Isaac Villafan
 */
@Entity
@Table(name = "materias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materias.findAll", query = "SELECT m FROM Materias m"),
    @NamedQuery(name = "Materias.findByIdmateria", query = "SELECT m FROM Materias m WHERE m.idmateria = :idmateria"),
    @NamedQuery(name = "Materias.findByCreditos", query = "SELECT m FROM Materias m WHERE m.creditos = :creditos"),
    @NamedQuery(name = "Materias.findByNombre", query = "SELECT m FROM Materias m WHERE m.nombre = :nombre")})
public class Materias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmateria")
    private Integer idmateria;
    @Column(name = "creditos")
    private Integer creditos;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idmateria", fetch = FetchType.LAZY)
    private List<Carreramateria> carreramateriaList;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idmateria", fetch = FetchType.LAZY)
    private List<Docentesmaterias> docentesmateriasList;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idmateria", fetch = FetchType.LAZY)
    private List<Materiasgrupos> materiasgruposList;

    public Materias() {
    }

    public Materias(Integer idmateria) {
        this.idmateria = idmateria;
    }

    public Integer getIdmateria() {
        return idmateria;
    }

    public void setIdmateria(Integer idmateria) {
        this.idmateria = idmateria;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    @JsonIgnore
    public List<Carreramateria> getCarreramateriaList() {
        return carreramateriaList;
    }

    public void setCarreramateriaList(List<Carreramateria> carreramateriaList) {
        this.carreramateriaList = carreramateriaList;
    }

    @XmlTransient
    @JsonIgnore
    public List<Docentesmaterias> getDocentesmateriasList() {
        return docentesmateriasList;
    }

    public void setDocentesmateriasList(List<Docentesmaterias> docentesmateriasList) {
        this.docentesmateriasList = docentesmateriasList;
    }
    
    @XmlTransient
    @JsonIgnore
    public List<Materiasgrupos> getMateriasgruposList() {
        return materiasgruposList;
    }

    public void setMateriasgruposList(List<Materiasgrupos> materiasgruposList) {
        this.materiasgruposList = materiasgruposList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmateria != null ? idmateria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materias)) {
            return false;
        }
        Materias other = (Materias) object;
        if ((this.idmateria == null && other.idmateria != null) || (this.idmateria != null && !this.idmateria.equals(other.idmateria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.core.c3.dao.entity.Materias[ idmateria=" + idmateria + " ]";
    }

}

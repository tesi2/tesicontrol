/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Materias;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IMateriaRepository extends JpaRepository<Materias, Integer> {

    @Query("SELECT m FROM Materias m WHERE m.idmateria =:idMateria")
    public Materias buscarPorId(@Param("idMateria") Integer idMateria);

    @Query("SELECT m FROM Materias m JOIN FETCH  m.carreramateriaList cl WHERE cl.idcarrera.idcarrera = :idcarrera")
    public List<Materias> findByIdCarrera(@Param("idcarrera") Integer idcarrera);

    @Query("SELECT m FROM Materias m WHERE m.nombre = :materia")
    public Materias findByNombre(@Param("materia") String materia);

    @Query("SELECT m FROM Materias m JOIN FETCH m.docentesmateriasList dl WHERE dl.iddocente.iddocente = :iddocente")
    public List<Materias> findByDocente(@Param("iddocente") Integer iddocente);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Colacorreos;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author balam
 */
@Repository
public interface IColacorreosRepository extends JpaRepository<Colacorreos, Integer>{
    
    @Query("SELECT cc FROM Colacorreos cc WHERE cc.enviado = 0")
    public List<Colacorreos> buscarSinEnviar();
    
}

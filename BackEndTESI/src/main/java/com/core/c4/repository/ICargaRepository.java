/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Carga;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface ICargaRepository extends JpaRepository<Carga, Integer> {

    @Query("SELECT c FROM Carga c WHERE c.idalumno.idalumno = :idalumno")
    public List<Carga> findByAlumno(@Param("idalumno") Integer idalumno);
}

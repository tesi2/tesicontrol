/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Docentes;
import com.core.c3.dao.entity.Docentesmaterias;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author balam
 */
@Repository
public interface IDocentesmateriasRepository extends JpaRepository<Docentesmaterias, Integer>{
    
    @Query("SELECT dm FROM Docentesmaterias dm WHERE dm.iddocentesmateriascol =:idDocentesmateriascol")
    public Docentesmaterias buscarPorId(@Param("idDocentesmateriascol")Integer idDocentesmateriascol);
    
    @Query("SELECT dm FROM Docentesmaterias dm WHERE dm.iddocente =:docente")
    public List<Docentesmaterias> buscarPorDocente(@Param("docente")Docentes docente);
    
}

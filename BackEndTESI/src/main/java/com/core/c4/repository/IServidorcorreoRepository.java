/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Servidorcorreo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author balam
 */
@Repository
public interface IServidorcorreoRepository extends JpaRepository<Servidorcorreo, Integer> {
    
    @Query(value = "SELECT sc FROM Servidorcorreo sc WHERE sc.tipo = :tipo AND sc.conteo < 495 ORDER BY sc.conteo DESC")
    public List<Servidorcorreo> buscarPorTipo(@Param("tipo") String tipo);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Alumnos;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author user
 */
public interface IAlumnoRepository extends JpaRepository<Alumnos, Integer> {

    @Query(value = "SELECT a FROM Alumnos a WHERE a.idcarrera = :idcarrera")
    public List<Alumnos> listarAlumnosPorCarrera(@Param("idcarrera") Integer idempresa);

    @Query(value = "SELECT a FROM Alumnos a WHERE a.correo = :correo")
    public Alumnos buscarPorCorreo(@Param("correo") String correo);

    @Query(value = "SELECT a FROM Alumnos a WHERE a.matricula = :matricula")
    public Alumnos findByMatricula(@Param("matricula") String matricula);
}

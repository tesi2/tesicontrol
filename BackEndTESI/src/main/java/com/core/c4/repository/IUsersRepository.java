/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

//import com.core.c3.dao.entity.Users;
import com.core.c3.dao.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author user
 */
@Repository
public interface IUsersRepository extends JpaRepository<Users, Integer>{
//    extends JpaRepository<Users, Integer>
    public abstract Users findByUsername(String username);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Archivos;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IArchivosRepository extends JpaRepository<Archivos, Integer> {

   

    @Query(value = "SELECT a FROM Archivos a JOIN FETCH a.idtipoarchivo as t WHERE a.idcarga.idcarga =:idcarga")
    public List<Archivos> listarPorCarga(@Param("idcarga") Integer idcarga);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Grupos;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IGruposRepository extends JpaRepository<Grupos, Integer> {

    @Query("SELECT g FROM Grupos g WHERE g.idcarrera.idcarrera =:idcarrera  ")
    public List<Grupos> findByCarrera(@Param("idcarrera") Integer idcarrera);

    @Query("SELECT g FROM Grupos g WHERE g.codigo = 'SIN GRUPO'")
    public Grupos getGrupoDefault();
    
    @Query("SELECT g FROM Grupos g WHERE g.idgrupo =:idgrupo")
    public Grupos buscarPorId(@Param("idgrupo") Integer idgrupo);
}

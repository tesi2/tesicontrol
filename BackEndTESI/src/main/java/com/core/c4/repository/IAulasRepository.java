/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Aulas;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IAulasRepository extends JpaRepository<Aulas, Integer>{
    
    @Query("SELECT a FROM Aulas a WHERE a.idaula =:idAula")
    public Aulas buscarPorId(@Param("idAula")Integer idAula);
    
    @Query("SELECT a FROM Aulas a WHERE a.nombre =:nombre")
    public Aulas buscarPorNombre(@Param("nombre")String nombre);
    
    @Query("SELECT a FROM Aulas a WHERE a.tipo =:tipo")
    public List<Aulas> buscarPorTipo(@Param("tipo")String tipo);
    
    @Query("SELECT a FROM Aulas a WHERE a.idedificio.idedificio =:idedificio")
    public List<Aulas> findByEdificio(@Param("idedificio")Integer idedificio);
}

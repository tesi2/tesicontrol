/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Docentesmaterias;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.dao.entity.Horarios;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IHorariosRepository extends JpaRepository<Horarios, Integer> {
    
    @Query(value="SELECT h FROM Horarios h WHERE h.idgrupo = :grupo")
    public List<Horarios> buscarPorGrupo(@Param("grupo") Grupos grupo);
    
    @Query(value="SELECT h FROM Horarios h WHERE h.iddocentesmateriascol = :docmat")
    public Horarios buscarPorDocentemateria(@Param("docmat") Docentesmaterias docmat);
    
}

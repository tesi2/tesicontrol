/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Administrativos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IAdministrativoRepository extends JpaRepository<Administrativos, Integer>{
    @Query(value="SELECT a FROM Administrativos a WHERE a.correo = :correo")
    public Administrativos findByCorreo(@Param("correo") String correo);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c4.repository;

import com.core.c3.dao.entity.Docentes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Repository
public interface IDocentesRepository extends JpaRepository<Docentes, Integer>{
    
    @Query("SELECT d FROM Docentes d WHERE d.iddocente =:idDocente")
    public Docentes buscarPorId(@Param("idDocente")Integer idDocente);
    
    @Query("SELECT d FROM Docentes d WHERE d.correo =:correo")
    public Docentes findByCorreo(@Param("correo")String correo);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.grupos;

import com.core.c1.web.controllers.docente.DocenteController;
import com.core.c2.services.grupos.IServiceGrupos;
import com.core.c3.dao.entity.Grupos;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/grupos")
public class GruposController {

     private static final Logger log = LogManager.getLogger(GruposController.class);
    @Autowired
    private IServiceGrupos iServiceGrupos;

    @GetMapping("/findAll")
    public List<Grupos> findAll() {
        log.info("@TESI---->Listando todos los grupos");
        return iServiceGrupos.findAll();
    }

    @GetMapping("/findById")
    public Grupos findById(@RequestParam("idgrupo") Integer idgrupo) {
        log.info("@TESI---->Buscando grupo con id: " + idgrupo);
        return this.iServiceGrupos.findById(idgrupo);
    }
}

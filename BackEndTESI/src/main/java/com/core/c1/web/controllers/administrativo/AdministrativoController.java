/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.administrativo;

import com.core.c1.web.controllers.edificio.EdificioController;
import com.core.c2.services.administrativo.IServiceAdministrativo;
import com.core.c3.dao.entity.Administrativos;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/administrativo")
public class AdministrativoController {

    private static final Logger log = LogManager.getLogger(AdministrativoController.class);
    @Autowired
    private IServiceAdministrativo iServiceAdministrativo;

    @GetMapping("/findByUsername")
    public Administrativos findByUsername(@RequestParam("username") String username) {
        log.info("@TESI---->Buscando Administrativos con el username: " + username);
        return this.iServiceAdministrativo.findByCorreo(username);
    }

    @GetMapping("/findById")
    public Administrativos findById(@RequestParam("idadministrativo") Integer idadministrativo) {
        log.info("@TESI---->Buscando Administrativos con el idadministrativo: " + idadministrativo);
        return this.iServiceAdministrativo.findById(idadministrativo);
    }

    public Administrativos add() {
        return null;
    }

    public Administrativos save() {
        return null;
    }
}

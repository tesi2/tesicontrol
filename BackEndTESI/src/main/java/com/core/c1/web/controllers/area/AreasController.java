/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.area;

import com.core.c2.services.areas.IServiceAreas;
import com.core.c3.dao.entity.Areas;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/areas")
public class AreasController {

    private final static Logger log = LogManager.getLogger(AreasController.class);
    @Autowired
    private IServiceAreas iServiceAreas;

    @GetMapping("/findAll")
    public List<Areas> findAll() {
        log.info("@TESI---->Buscando todas las areas ");
        return this.iServiceAreas.findAll();
    }

    @GetMapping("/findById")
    public Areas findById(@RequestParam("idarea") Integer idarea) {
        log.info("@TESI---->Buscando area con el id: "+idarea);
        return this.iServiceAreas.findById(idarea);
    }

}

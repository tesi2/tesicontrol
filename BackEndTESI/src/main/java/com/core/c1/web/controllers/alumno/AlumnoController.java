 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.alumno;

import com.core.c1.web.controllers.administrativo.AdministrativoController;
import com.core.c2.services.alumno.IServiceAlumno;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.core.c3.dao.entity.Alumnos;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author user
 */
@RestController
@CrossOrigin(origins = "*")//localhost 
@RequestMapping("/api/alumno")
public class AlumnoController {

    private static final Logger log = LogManager.getLogger(AlumnoController.class);
    @Autowired
    private IServiceAlumno iServiceAlumno;

    @GetMapping(value = "/getAll")
    public List<Alumnos> getall() {
         log.info("@TESI----> Listando todos los alumnos");
        return this.iServiceAlumno.findAll();
    }

    @GetMapping(value = "/findByUsername")
    public Alumnos getAlumno(@RequestParam("username") String username) {
        return this.iServiceAlumno.findByCorreo(username);
    }

    @GetMapping(value = "/findById")
    public Alumnos findById(@RequestParam("idalumno") Integer pIdAlumno) {
         log.info("@TESI----> Buscando alumno con el id: "+pIdAlumno);
        return this.iServiceAlumno.findById(pIdAlumno);
    }

    @GetMapping(value = "/findByCarrera")
    public List<Alumnos> getAlumnoByCarrera(@RequestParam("idcarrera") Integer idcarrera) {
        log.info("@TESI----> Listando alumnos por carrera");
        return this.iServiceAlumno.findByCarrera(idcarrera);
    }

    @PostMapping("/save")
    public Alumnos updateAlumno(@RequestBody Alumnos alumno) {
        log.info("@TESI---->Actualizando la informacion del alumno: "
                + alumno.getMatricula());
        return this.iServiceAlumno.save(alumno);
    }

    @PostMapping("/singUp")
    public Alumnos singUp(@RequestBody Alumnos alumno) {
        log.info("@TESI----> Registrando al alumno : " + new Gson().toJson(alumno));
        return this.iServiceAlumno.singUp(alumno);
    }
}

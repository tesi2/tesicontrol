/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.grupos.horario;

import com.core.c2.services.grupos.IServiceGrupos;
import com.core.c2.services.horarios.IServiceHorarios;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.ddd.dto.HorarioDTO;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author balam
 */
@RestController
@CrossOrigin("*")
@RequestMapping("")
public class HorarioGrupoController {
    
    private final static Logger log = LogManager.getLogger(HorarioGrupoController.class);
    
    @Autowired
    private IServiceGrupos iServiceGrupos;
    @Autowired
    private IServiceHorarios iServiceHorarios;
    
     @GetMapping("/loadHorarios")
     public void cargarHorario(@RequestParam("idgrupo") Integer idgrupo) {
        Grupos grupo = this.iServiceGrupos.findById(idgrupo);
         List<HorarioDTO> horariosList = this.iServiceHorarios.obtenerPorGrupo(grupo);
     }
}

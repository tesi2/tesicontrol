/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.tipoArchivo;

import com.core.c1.web.controllers.procesos.ProcesosController;
import com.core.c2.services.tipoArchivo.IServiceTipoArchivo;
import com.core.c3.dao.entity.Tipoarchivo;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/tipoArchivo")
public class TipoArchivoController {

     private static final Logger log = LogManager.getLogger(TipoArchivoController.class);
    @Autowired
    private IServiceTipoArchivo iServiceTipoArchivo;

    @GetMapping("/findAll")
    public List<Tipoarchivo> findAll() {
        log.info("@TESI---->Listando todos los tipos de archivos");
        return this.iServiceTipoArchivo.findAll();
    }
    @GetMapping("/findById")
    public Tipoarchivo findById(@RequestParam("idtipoarchivo") Integer idtipoarchivo){
        log.info("@TESI---->Buscando tipo de archivo con id: "+idtipoarchivo);
    return this.iServiceTipoArchivo.findById(idtipoarchivo);
    }
}

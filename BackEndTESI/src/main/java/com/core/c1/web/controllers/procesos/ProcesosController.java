/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.procesos;

import com.core.c1.web.controllers.grupos.GruposController;
import com.core.c2.services.procesos.IServiceProcesos;
import com.core.c3.dao.entity.Procesos;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/procesos")
public class ProcesosController {

     private static final Logger log = LogManager.getLogger(ProcesosController.class);
    @Autowired
    private IServiceProcesos iServiceProcesos;

    @GetMapping("/findAll")
    public List<Procesos> findAll() {
        return this.iServiceProcesos.findAll();
    }

    @GetMapping("/findById")
    public Procesos findById(@RequestParam("idproceso") Integer idproceso) {
        return this.iServiceProcesos.findById(idproceso);
    }
}

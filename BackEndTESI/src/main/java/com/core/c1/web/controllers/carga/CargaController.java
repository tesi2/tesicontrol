/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.carga;

import com.core.c2.app.services.notificar.IServiceNotificarWhatsApp;
import com.core.c2.services.carga.IServiceCarga;
import com.core.c3.dao.entity.Alumnos;
import com.core.c3.dao.entity.Carga;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/carga")
public class CargaController {

    private static final String UPLOAD_LOCATION = "C://TESI//TEMP//";
    private final static Logger log = LogManager.getLogger(CargaController.class);
    @Autowired
    private IServiceCarga iServiceCarga;
    @Autowired
    private IServiceNotificarWhatsApp iServiceNotificar;

    @GetMapping("/findByAlumno")
    public List<Carga> findByAlumno(@RequestParam("idalumno") Integer idalumno) {
        log.info("@TESI--->Listando cargas del alumno con id : "
                + idalumno);
        return this.iServiceCarga.findByAlumno(idalumno);
    }

    @GetMapping("/findByIdcarga")
    public Carga findByProceso(@RequestParam("idcarga") Integer idcarga) {
        return this.iServiceCarga.findById(idcarga);
    }

    @GetMapping("/findAll")
    public List<Carga> findAll() {
        return this.iServiceCarga.findAll();
    }

    @PostMapping("/saveCarga")
    public Carga saveCarga(@RequestBody Carga pCarga) {
        Carga carga = new Carga();
      log.info("@TESI---->Se Inicia la carga de archivos del alumno: " + pCarga.getIdalumno().getMatricula());
        Alumnos alumno = pCarga.getIdalumno();
        carga.setIdalumno(alumno);
        carga.setIdproceso(pCarga.getIdproceso());
        carga.setFechacarga(new Date());
        carga.setRevisada(Boolean.FALSE);
        carga = this.iServiceCarga.save(carga);
        //enviar correo a control escolar 
        return carga;
    }

    @GetMapping("/checkCarga")
    public Carga updateCarga(@RequestParam("idcarga") Integer idcarga) {
        Carga carga = this.iServiceCarga.findById(idcarga);
        carga.setRevisada(Boolean.TRUE);
//        this.iServiceNotificar.notificarAlumnoRevisionDeCarga(carga.getIdalumno().getIdalumno(), idcarga);
        return this.iServiceCarga.save(carga);
    }

}

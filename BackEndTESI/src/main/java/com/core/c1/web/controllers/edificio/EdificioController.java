/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.edificio;

import com.core.c2.services.administrativo.IServiceAdministrativo;
import com.core.c2.services.aula.IServiceAula;
import com.core.c2.services.edificios.IServiceEdificios;
import com.core.c3.dao.entity.Administrativos;
import com.core.c3.dao.entity.Aulas;
import com.core.c3.dao.entity.Edificios;
import com.google.gson.Gson;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/edificio")
public class EdificioController {

    private static final Logger log = LogManager.getLogger(EdificioController.class);
    @Autowired
    private IServiceEdificios iServiceEdificio;
    @Autowired
    private IServiceAdministrativo iServiceUsuario;
    @Autowired
    private IServiceAula iServiceAula;

    @GetMapping("/findAll")
    public List<Edificios> findAll() {
        log.info("@TESI---->Listando todos los edificios");
        return this.iServiceEdificio.findAll();
    }

    @GetMapping("/findById")
    public Edificios findById(@RequestParam("idedificio") Integer idedificio) {
        log.info("@TESI---->Buscando edificio con id: " + idedificio);
        return this.iServiceEdificio.findById(idedificio);
    }

    @PostMapping("/add")
    public Edificios save(@RequestBody Edificios edificio) {
        log.info("@TESI---->Agregando nuevo edificio: " + edificio.getNombre());
        return this.iServiceEdificio.save(edificio);
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("idedificio") Integer idedificio, @RequestParam("idadministratio") Integer idadministratio) {
        String result = "";
        Administrativos admin = this.iServiceUsuario.findById(idadministratio);
        String adminStr = admin.getPaterno() + " " + admin.getMaterno() + " " + admin.getNombre();
        Edificios edificio = this.iServiceEdificio.findById(idedificio);
        List<Aulas> aulasList = this.iServiceAula.findByEdificio(idedificio);
        if (aulasList != null && !aulasList.isEmpty()) {
            log.info("@TESI---> Se inicia la eliminacion del edificio: " + edificio.getNombre() + " Solicitada por: " + adminStr);
            for (Aulas aula : aulasList) {
                log.info("@TESI---->Eliminando aula: " + aula.getNombre());
                this.iServiceAula.delete(aula);
            }
            log.info("@TESI----> Se eliminaron las aulas");
        }
        this.iServiceEdificio.delete(edificio);
        log.info("@TESI----> Se eliminaron el edificio");
        result = "OK";
        return new Gson().toJson(result);
    }

    @GetMapping("/check")
    public String checkEdificio(@RequestParam("nombre") String nombre) {
        String nom = nombre.toUpperCase();
        nom = nom.trim();
        Edificios e = this.iServiceEdificio.findByNombre(nom);
        String response;
        if (e != null) {
            response = "NO";
            log.info("@TESI---->Nombre de edificio no disponible");
        } else {
            response = "SI";
            log.info("@TESI---->Nombre de edificio disponible");
        }
        return new Gson().toJson(response);
    }

}

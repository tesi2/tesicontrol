/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.users;

import com.core.c2.services.users.IServiceUsers;
import com.core.c3.dao.entity.Users;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*")
public class UsersController {

    private final static Logger log = LogManager.getLogger(UsersController.class);
    @Autowired
    private IServiceUsers iServiceUser;

    @GetMapping("/getUser")
    public Users getUser(@RequestParam("username") String userName) {
        log.info("@TESI---->Buscando usuario: " + userName);
        return this.iServiceUser.findByUserName(userName);
    }

    public String changePassword() {
        //cuando se solita un cambio de contraseña 
        return null;
    }

    @GetMapping("/ifExist")
    public boolean ifExistsUser(@RequestParam("username") String username) {
        Users u = this.iServiceUser.findByUserName(username);
        if (u != null) {
            return false;
        } else {
            return true;
        }
    }

}

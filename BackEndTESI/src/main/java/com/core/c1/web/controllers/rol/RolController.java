/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.rol;

import com.core.c2.services.roles.IServiceRoles;
import com.core.c3.dao.entity.Roles;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@RequestMapping("/api/rol")
@CrossOrigin("*")
public class RolController {

    @Autowired
    private IServiceRoles iServiceRol;

    @GetMapping("/findAll")
    public List<Roles> findAll() {
        return this.iServiceRol.findAll();
    }

    @GetMapping("findById")
    public Roles findbyId(@RequestParam("idrol") Integer idrol) {
        return this.iServiceRol.findById(idrol);
    }
}

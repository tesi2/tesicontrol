/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.docente;

import com.core.c1.web.controllers.carrera.CarreraControllers;
import com.core.c2.services.docente.IServiceDocente;
import com.core.c3.dao.entity.Docentes;
import com.core.c3.dao.entity.Users;
import com.core.c4.repository.IUsersRepository;
import com.core.c5.poa.clave.GeneradorDeClaves;
import com.google.gson.Gson;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/docente")
public class DocenteController {

    private static final Logger log = LogManager.getLogger(DocenteController.class);
    @Autowired
    private IServiceDocente iServiceDocente;
    @Autowired
    private IUsersRepository iUsersRepository;

    @GetMapping("/findByUsername")
    public Docentes findByUsername(@RequestParam("username") String username) {
        log.info("@TESI---->Listando al docente con correo: " + username);
        return this.iServiceDocente.findByCorreo(username);
    }

    @GetMapping("/findAll")
    public List<Docentes> findALl() {
        log.info("@TESI---->Listando todos los docentes");
        return this.iServiceDocente.findAll();
    }
//modificar funciones 
    @PostMapping("/save")
    public Docentes save(@RequestBody Docentes docente) {
        log.info("@TESI---->Grabando datos del docente: " + new Gson().toJson(docente));
        return this.iServiceDocente.save(docente);
    }

    @PostMapping("/add")
    public Docentes add(@RequestBody Docentes docente) {
        Users user = new Users();
        log.info("@TESI---->Creando nuevo usuario: ");
        docente.setNombre(docente.getNombre().toUpperCase()) ;
        docente.setPaterno(docente.getPaterno().toUpperCase()) ;
        docente.setMaterno(docente.getMaterno().toUpperCase());
        user.setUsername(docente.getCorreo());
        user.setPassword("{noop}" + GeneradorDeClaves.generar(10, 1, 1, 2));
        log.info("@TESI----->Asignando clave a nuevo usuario");
        user.setSesion("DOCENTE");
        user.setEnable(true);
        user.setEnabled(true);
        log.info("@TESI----->Activando nuevo usuario");
        Users userResponse = this.iUsersRepository.save(user);
        if (userResponse != null) {
            log.info("@TESI---->Usuario creado con el id: " + userResponse.getIdusers());
            log.info("@TESI---->Creando sesion de docente");
            Docentes docenteResponse = this.iServiceDocente.save(docente);
            if (docenteResponse != null) {
                log.info("@TESI---->Docente registrado con exito");
                return docenteResponse;
            } else {
                return null;
            }

        } else {
            log.info("@TESI--->Existe un usuario registrado con el mismo correo electronico");
            return null;
        }
    }

    @GetMapping("/findById")
    public Docentes findById(@RequestParam("iddocente") Integer iddocente) {
        log.info("@TESI---->Buscnado al docente con el id: " + iddocente);
        return this.iServiceDocente.findById(iddocente);
    }

    @GetMapping("/check")
    public String check(@RequestParam("correo") String correo) {
        log.info("@TESI---->Buscando usuario con el correo: " + correo);
        String response = "";
        Users user = this.iUsersRepository.findByUsername(correo);
        if (user == null) {
            response = "SI";
        } else {
            response = "NO";
        }
        return new Gson().toJson(response);
    }
}

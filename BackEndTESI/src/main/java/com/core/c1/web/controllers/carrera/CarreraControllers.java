/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.carrera;

import com.core.c2.services.carrera.IServiceCarrera;
import com.core.c3.dao.entity.Carreras;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/carrera")
public class CarreraControllers {

    private static final Logger log = LogManager.getLogger(CarreraControllers.class);
    @Autowired
    private IServiceCarrera iServiceCarrera;

    @GetMapping("/findAll")
    public List<Carreras> findAll() {
        log.info("@TESI---->Listando todas las carrera");
        return this.iServiceCarrera.findAll();
    }

    @GetMapping("/findById")
    public Carreras findById(@RequestParam("idcarrera") Integer idcarrera) {
        log.info("@TESI---->Buscando carrera con el id: " + idcarrera);
        return this.iServiceCarrera.findById(idcarrera);
    }
}

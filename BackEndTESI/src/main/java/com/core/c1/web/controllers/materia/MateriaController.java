/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.materia;

import com.core.c2.services.materias.IServiceMateria;
import com.core.c3.dao.entity.Materias;
import com.google.gson.Gson;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@RequestMapping("/api/materia")
@CrossOrigin("*")
public class MateriaController {

    private static final Logger log = LogManager.getLogger(MateriaController.class);
    @Autowired
    private IServiceMateria iMateriaService;

    @GetMapping("/findById")
    public Materias findById(@RequestParam("idmateria") Integer idmateria) {
        log.info("@TESI---->Buscando la materia con id: " + idmateria);
        return this.iMateriaService.findById(idmateria);
    }

    @GetMapping("/findByDocente")
    public List<Materias> findByDocente(@RequestParam("iddocente") Integer iddocente) {
        log.info("@TESI----->bucsando por id docente: " + iddocente);
        return this.iMateriaService.findByIdDocente(iddocente);
    }

    @GetMapping("/findAll")
    public List<Materias> findAll() {
        log.info("@TESI---->Listando todas las materias");
        return this.iMateriaService.findAll();
    }

    @GetMapping("/findByCarrera")
    public List<Materias> findByIdCarrera(@RequestParam("idcarrera") Integer idcarrera) {
        log.info("@TESI---->Listando las materias de la carrera: " + idcarrera);
        return this.iMateriaService.findByIdCarrera(idcarrera);
    }

    @PostMapping("/save")
    public Materias save(@RequestBody Materias materia) {
        log.info("@TESI-----> grabando materia: " + new Gson().toJson(materia));
        return this.iMateriaService.save(materia);
    }

    @GetMapping("/check")
    public String check(@RequestParam("nombre") String nombre) {
        String response;
        log.info("@TESI---->Validando disponivilidad del nombre de la materia: " + nombre);
        Materias materia = this.iMateriaService.findByNombre(nombre);
        if (materia != null) {
            log.info("@TESI---->Nombre no disponible");
            response = "NO";
        } else {
            log.info("@TESI---->Nombre disponible");
            response = "SI";
        }
        return new Gson().toJson(response);
    }

}

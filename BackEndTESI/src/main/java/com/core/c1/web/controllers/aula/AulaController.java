/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.aula;

import com.core.c2.services.aula.IServiceAula;
import com.core.c3.dao.entity.Aulas;
import com.google.gson.Gson;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alan Isaac Villafan
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/aula")
public class AulaController {

    private final static Logger log = LogManager.getLogger(AulaController.class);
    @Autowired
    private IServiceAula iServiceAula;

    @GetMapping("/findAll")
    public List<Aulas> findAll() {
        return this.iServiceAula.findAll();
    }

    @GetMapping("/findById")
    public Aulas findById(@RequestParam("idaula") Integer idaula) {
        return this.iServiceAula.findById(idaula);
    }

    @GetMapping("/findByEdificio")
    public List<Aulas> findByEdificio(@RequestParam("idedificio") Integer idedificio) {
        log.info("@TESI---->Listando aulas del edicio: " +idedificio);
        return this.iServiceAula.findByEdificio(idedificio);
    }

    @PostMapping("/save")
    public Aulas addNewAula(@RequestBody Aulas aula) {
        log.info("@TESI---->Agregando el aula: " + new Gson().toJson(aula));
        return this.iServiceAula.save(aula);
    }

    @PostMapping("/delet")
    public String deleteAula(@RequestBody Aulas aula) {
        log.info("@TESI---->Eliminando el aula: " + new Gson().toJson(aula));
        this.iServiceAula.delete(aula);
        return new Gson().toJson("OK");
    }

}

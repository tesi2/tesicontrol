/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c1.web.controllers.archivo;

import com.core.c2.services.archivos.IServiceArchivos;
import com.core.c2.services.carga.IServiceCarga;
import com.core.c3.dao.entity.Archivos;
import com.core.c3.dao.entity.Carga;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@CrossOrigin("*")
@RequestMapping("api/archivo")
public class ArchivoController {

    private final static Logger log = LogManager.getLogger(ArchivoController.class);
    @Autowired
    private IServiceCarga iServiceCarga;
    @Autowired
    private IServiceArchivos iServiceArchivo;

    @PostMapping("/saveFile")
    public Archivos saveCarga(@RequestBody Archivos pArchivo, @PathVariable(name = "idcarga", required = false) Integer idcarga) {
        Carga carga;
        log.info("@TESI---->Se inicia la carga de archivos ");
        if (idcarga != null) {
            carga = this.iServiceCarga.findById(idcarga);
            pArchivo.setIdcarga(carga);
            pArchivo.setFechacarga(new Date());
            System.out.println("Se cargo con exito el archivo");
            return this.iServiceArchivo.save(pArchivo);
        } else if (pArchivo.getIdcarga() != null) {
            carga = iServiceCarga.findById(pArchivo.getIdcarga().getIdcarga());
            pArchivo.setIdcarga(carga);
            pArchivo.setAprobado(Boolean.FALSE);
            pArchivo.setFechacarga(new Date());
            System.out.println("Se cargo con exito el archivo");
            return this.iServiceArchivo.save(pArchivo);
        } else {
            System.out.println("no se logro cargar el archivo");
        }
        return null;
    }

    @GetMapping("/findByCarga")
    public List<Archivos> findByCarga(@RequestParam("idcarga") Integer idcarga) {
        log.info("@TESI---->Lsitando Archivos de la carga" + idcarga);
        return this.iServiceArchivo.listarPorCarga(idcarga);
    }

    @PostMapping("/update")
    public Archivos update(@RequestBody Archivos archivo) {
        log.info("@TESI----> Actualizando archivo: " + archivo.getIdarchivo());
        return this.iServiceArchivo.save(archivo);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.app.services.notificar;

import com.core.c2.services.alumno.IServiceAlumno;
import com.core.c2.services.archivos.IServiceArchivos;
import com.core.c2.services.carga.IServiceCarga;
import com.core.c2.services.procesos.IServiceProcesos;
import com.core.c3.dao.entity.Alumnos;
import com.core.c3.dao.entity.Archivos;
import com.core.c3.dao.entity.Carga;
import com.core.c3.dao.entity.Procesos;
import com.core.c5.poa.Date.DateUtil;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceNotificarWhatsAppImpl implements IServiceNotificarWhatsApp {

    private static final String ACCOUNT_SID = "ACa1375c0b681968aab425d357af077b39";
    private static final String AUTHTOKEN = "36663dcd2b62fcf14f9fba76902d9dcb";
    private static final String DATE_FORMAT = "dd-MM-yyyy";
    private static final Logger log = LogManager.getLogger(ServiceNotificarWhatsAppImpl.class);
    @Autowired
    private IServiceAlumno iServiceAlumno;
    @Autowired
    private IServiceCarga iServiceCarga;
    @Autowired
    private IServiceArchivos iServiceArchivo;
    @Autowired
    private IServiceProcesos iServiceProceso;

    @Override
    public void notificarAlumnoRevisionDeCarga(Integer idalumno, Integer idacarga) {
        Alumnos alumno = this.iServiceAlumno.findById(idalumno);
        Carga carga = this.iServiceCarga.findById(idacarga);
        log.info("@TESI---->Generando mensaje Whats App  para el alumno :" + alumno.getMatricula());
        String mensageStr = this.generarMensajeRevisionCarga(carga);

        try {
            Twilio.init(ACCOUNT_SID, AUTHTOKEN);
            Message message = Message.creator(
                     new com.twilio.type.PhoneNumber("+52" + alumno.getTelefono()),new com.twilio.type.PhoneNumber("+12058272579"), mensageStr).create();
            log.info(message.getSid());
        } catch (Exception e) {
            log.info("@ERROR----> Se genero un error al notificar por whatsapp :" + e.getMessage());
        }

    }

    private String generarMensajeRevisionCarga(Carga pCarga) {
        String mensaje = "";
        String fechaCargaStr = DateUtil.obtenerFechaConFormato(pCarga.getFechacarga(), DATE_FORMAT);
        Procesos proceso = this.iServiceProceso.findById(pCarga.getIdproceso().getIdproceso());
        mensaje += "Se ha revisado tu carga de documentos con folio: " + pCarga.getIdcarga();
        mensaje += " con fecha: " + fechaCargaStr;
        mensaje += " para el proceso: " + proceso.getNombre();
        mensaje += this.obtenerInformacionDeArchivos(pCarga.getIdcarga());
        return mensaje;
    }

    private String obtenerInformacionDeArchivos(Integer idcarga) {
        String msg = "";
        List<Archivos> archivosList = this.iServiceArchivo.listarPorCarga(idcarga);
        if (archivosList != null && !archivosList.isEmpty()) {
            msg += " donde se cargaron " + archivosList.size() + " archivo(S)";
            msg += " con los siguientes comentarios ";
            for (Archivos archivo : archivosList) {
                msg += " Tipo de archivo: " + archivo.getIdtipoarchivo().getTipo() + " ";
                msg += archivo.getNombre() + " : " + archivo.getComentario() + " ";
            }
        }
        return msg;
    }

}

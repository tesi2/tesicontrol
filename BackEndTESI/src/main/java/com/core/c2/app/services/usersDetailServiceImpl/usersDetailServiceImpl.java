/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.app.services.usersDetailServiceImpl;

import com.core.c2.services.users.IServiceUsers;
import com.core.c3.dao.entity.Users;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author user
 */
@Service
public class usersDetailServiceImpl  implements UserDetailsService{
    @Autowired
    private IServiceUsers iServiceUsers;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users=iServiceUsers.findByUserName(username);
        return new User(users.getUsername(), users.getPassword(), users.getEnabled(),users.getEnabled(),users.getEnabled(),users.getEnabled(),builgrante(users.getSesion()));
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public List<GrantedAuthority> builgrante(String rol){
       List<GrantedAuthority> auth= new ArrayList<>();
       auth.add(new SimpleGrantedAuthority(rol));
    return auth ;    }
}

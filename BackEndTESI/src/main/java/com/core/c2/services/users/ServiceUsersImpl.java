/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.users;

import com.core.c3.dao.entity.Users;
import com.core.c4.repository.IUsersRepository;
import com.core.c5.poa.clave.GeneradorDeClaves;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author user
 */
@Service
public class ServiceUsersImpl implements IServiceUsers {

    @Autowired
    private IUsersRepository iUsersRepository;

    @Override
    public List<Users> getAll() {
        return this.iUsersRepository.findAll();
    }

    @Override
    public Users findByUserName(String userName) {
        return this.iUsersRepository.findByUsername(userName);
    }

    @Override
    public Users addAlumno(String username) {
        Users user = new Users();
        user.setSesion("ALUMNO");
        String clave = GeneradorDeClaves.generar(15, 5, 5, 5);
        String claveFinal = "{noop}".concat(clave);
        user.setPassword(claveFinal);
        user.setUsername(username);
        user.setEnable(true);
        user.setEnabled(true);
        return this.iUsersRepository.save(user);
    }

}

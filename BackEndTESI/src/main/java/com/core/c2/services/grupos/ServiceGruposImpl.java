/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.grupos;

import com.core.c3.dao.entity.Grupos;
import com.core.c4.repository.IGruposRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceGruposImpl implements IServiceGrupos {

    @Autowired
    private IGruposRepository iGruposRepository;

    @Override
    public Grupos findById(Integer idgrupo) {
        return this.iGruposRepository.findById(idgrupo).get();
    }

    @Override
    public List<Grupos> findByCarrera(Integer idcarrera) {
        return this.iGruposRepository.findByCarrera(idcarrera);
    }

    @Override
    public List<Grupos> findAll() {
        return this.iGruposRepository.findAll();
    }

    @Override
    public Grupos save(Grupos grupos) {
        return this.iGruposRepository.save(grupos);
    }

    @Override
    public void delete(Grupos grupo) {
        this.iGruposRepository.delete(grupo);
    }

    @Override
    public Grupos getGrupoDefault() {
        return this.iGruposRepository.getGrupoDefault();
    }

}

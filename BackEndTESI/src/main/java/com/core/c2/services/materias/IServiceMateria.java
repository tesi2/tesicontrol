/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.materias;

import com.core.c3.dao.entity.Materias;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceMateria {

    public void delete(Materias materia);

    public Materias save(Materias materia);

    public Materias findById(Integer idmateria);

    public List<Materias> findAll();

    public List<Materias> findByIdCarrera(Integer idmateria);
    
    public List<Materias> findByIdDocente(Integer iddocente);

    public Materias findByNombre(String nombre);

}

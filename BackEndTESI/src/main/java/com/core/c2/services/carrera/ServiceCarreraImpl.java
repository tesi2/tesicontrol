/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carrera;

import com.core.c3.dao.entity.Carreras;
import com.core.c4.repository.ICarreraRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceCarreraImpl implements IServiceCarrera {
    
    @Autowired
    private ICarreraRepository iCarreraRepository;
    
    @Override
    public Carreras save(Carreras carrera) {
        return this.iCarreraRepository.save(carrera);
    }
    
    @Override
    public Carreras findById(Integer idcarrera) {
        return this.iCarreraRepository.findById(idcarrera).get();
    }
    
    @Override
    public void delete(Carreras carrera) {
         this.iCarreraRepository.delete(carrera);
    }
    
    @Override
    public List<Carreras> findAll() {
        return this.iCarreraRepository.findAll();
    }
    
}

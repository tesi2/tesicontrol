/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.grupos;

import com.core.c3.dao.entity.Grupos;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceGrupos {

    public Grupos findById(Integer idgrupo);

    public List<Grupos> findByCarrera(Integer idcarrera);

    public List<Grupos> findAll();

    public Grupos save(Grupos grupos);

    public void delete(Grupos grupo);
    
    public Grupos getGrupoDefault();

}

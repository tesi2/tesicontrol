/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carreramateria;

import com.core.c3.dao.entity.Carreramateria;
import com.core.c4.repository.ICarreraRepository;
import com.core.c4.repository.ICarreramateriaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceCarreraMateriaImpl implements IServiceCarreraMateria {

    @Autowired
    private ICarreramateriaRepository iCarreraRepoitory;

    @Override
    public Carreramateria save(Carreramateria carreramateria) {
        return this.iCarreraRepoitory.save(carreramateria);
    }

    @Override
    public void delete(Carreramateria carreramateria) {
        this.iCarreraRepoitory.delete(carreramateria);
    }

    @Override
    public List<Carreramateria> findAll() {
        return this.iCarreraRepoitory.findAll();
    }

}

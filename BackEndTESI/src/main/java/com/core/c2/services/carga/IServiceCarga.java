/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carga;

import com.core.c3.dao.entity.Carga;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceCarga {

    public Carga save(Carga carga);

    public void delete(Carga carga);
    
    public Carga findById(Integer idcarga);

    public List<Carga> findAll();

    public List<Carga> findByAlumno(Integer idalumno);
}

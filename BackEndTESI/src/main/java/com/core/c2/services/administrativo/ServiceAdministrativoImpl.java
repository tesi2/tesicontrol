/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.administrativo;

import com.core.c3.dao.entity.Administrativos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.core.c4.repository.IAdministrativoRepository;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceAdministrativoImpl implements IServiceAdministrativo {

    @Autowired
    private IAdministrativoRepository iAdministrativoRepo;

    @Override
    public Administrativos save(Administrativos admin) {
        return this.iAdministrativoRepo.save(admin);
    }

    @Override
    public void delete(Administrativos admin) {
        this.iAdministrativoRepo.delete(admin);
    }

    @Override
    public Administrativos findById(Integer IdAdministrativo) {
        return this.iAdministrativoRepo.findById(IdAdministrativo).get();
    }

    @Override
    public Administrativos findByCorreo(String correo) {
        return this.iAdministrativoRepo.findByCorreo(correo);
    }

    @Override
    public List<Administrativos> findAll() {
        return this.iAdministrativoRepo.findAll();
    }

}

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.enviarCorreo;

import java.io.File;
import java.util.List;

/**
 * 
 * @author Balam
 */
public interface IServiceEnviarCorreo {
    
    public void notificarRegistro(String correo, String nombre);
    
    public void eniviarCorreo(String asunto, List<String> receptores, String mensaje);
    
    public void enviarCorreo(String asunto, List<String> receptores, List<String> copiados, List<String> ocultos, String formato, String mensaje, List<File> adjuntoList);
    
}

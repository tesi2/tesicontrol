/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.roles;

import com.core.c3.dao.entity.Roles;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceRoles {

    public void delete(Roles rol);

    public Roles findById(Integer idrol);

    public Roles save(Roles rol);

    public List<Roles> findAll();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carreramateria;

import com.core.c3.dao.entity.Carreramateria;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceCarreraMateria {

    public Carreramateria save(Carreramateria carreramateria);

    public void delete(Carreramateria carreramateria);

    public List<Carreramateria> findAll();
}

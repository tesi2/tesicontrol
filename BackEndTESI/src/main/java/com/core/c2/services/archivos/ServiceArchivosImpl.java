/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.archivos;

import com.core.c3.dao.entity.Archivos;
import com.core.c4.repository.IArchivosRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceArchivosImpl implements IServiceArchivos {
    
    @Autowired
    private IArchivosRepository iArchivoRepo;
    
    @Override
    public Archivos save(Archivos archivo) {
        return this.iArchivoRepo.save(archivo);
    }
    
    @Override
    public void delete(Archivos archivos) {
        this.iArchivoRepo.delete(archivos);
    }
    
    @Override
    public Archivos findById(Integer idarchivo) {
        return this.iArchivoRepo.findById(idarchivo).get();
    }
    
    @Override
    public List<Archivos> listarPorAlumno(Integer idalumno) {
        return null;
    }
    
    @Override
    public List<Archivos> listarPorCarga(Integer idalumno) {
        return this.iArchivoRepo.listarPorCarga(idalumno);
    }
    
}

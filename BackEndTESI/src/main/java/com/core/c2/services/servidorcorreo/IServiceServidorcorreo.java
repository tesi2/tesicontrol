/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.servidorcorreo;

import com.core.c3.dao.entity.Servidorcorreo;
import java.util.List;

/**
 *
 * @author balam
 */
public interface IServiceServidorcorreo {
    
    public void guardarServidor(Servidorcorreo servidor);
    
    public List<Servidorcorreo> buscarServidorPorTipo(String tipo);
}

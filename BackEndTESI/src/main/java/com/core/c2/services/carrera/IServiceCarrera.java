/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carrera;

import com.core.c3.dao.entity.Carreras;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceCarrera {

    public Carreras save(Carreras carrera);

    public Carreras findById(Integer idcarrera);

    public void delete(Carreras carrera);

    public List<Carreras> findAll();
}

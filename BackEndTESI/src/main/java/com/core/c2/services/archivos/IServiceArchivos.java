/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.archivos;

import com.core.c3.dao.entity.Archivos;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceArchivos {

    public Archivos save(Archivos archivo);

    public void delete(Archivos archivos);

    public Archivos findById(Integer idarchivo);

    public List<Archivos> listarPorAlumno(Integer idalumno);

    public List<Archivos> listarPorCarga(Integer idalumno);

}

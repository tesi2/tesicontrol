/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.tipoArchivo;

import com.core.c3.dao.entity.Tipoarchivo;
import com.core.c4.repository.ITipoArchivoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceTipoArchivoImpl implements IServiceTipoArchivo {

    @Autowired
    private ITipoArchivoRepository iTipoRepo;

    @Override
    public void delete(Tipoarchivo tipoArchivo) {
        this.iTipoRepo.delete(tipoArchivo);
    }

    @Override
    public Tipoarchivo save(Tipoarchivo tipoArchivo) {
        return this.iTipoRepo.save(tipoArchivo);
    }

    @Override
    public Tipoarchivo findById(Integer idTipoArchivo) {
        return this.iTipoRepo.findById(idTipoArchivo).get();
    }

    @Override
    public List<Tipoarchivo> findAll() {
        return this.iTipoRepo.findAll();
    }

}

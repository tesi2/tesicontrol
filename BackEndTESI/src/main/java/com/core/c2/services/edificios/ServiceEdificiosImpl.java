/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.edificios;

import com.core.c3.dao.entity.Edificios;
import com.core.c4.repository.IEdificiosRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceEdificiosImpl implements IServiceEdificios {

    @Autowired
    private IEdificiosRepository iEdificioRepository;

    @Override
    public Edificios save(Edificios edificio) {
        return this.iEdificioRepository.save(edificio);
    }

    @Override
    public Edificios findById(Integer idedificio) {
        return this.iEdificioRepository.findById(idedificio).get();
    }

    @Override
    public void delete(Edificios edificio) {
        this.iEdificioRepository.delete(edificio);
    }

    @Override
    public List<Edificios> findAll() {
        return this.iEdificioRepository.findAll();
    }

    @Override
    public Edificios findByNombre(String nombre) {
        return this.iEdificioRepository.findByNombre(nombre);
    }

}

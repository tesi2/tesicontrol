/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.carga;

import com.core.c3.dao.entity.Carga;
import com.core.c4.repository.ICargaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceCargaImpl implements IServiceCarga {

    @Autowired
    private ICargaRepository iCargaRepository;

    @Override
    public Carga save(Carga carga) {
        return this.iCargaRepository.save(carga);
    }

    @Override
    public void delete(Carga carga) {
        this.iCargaRepository.delete(carga);
    }

    @Override
    public Carga findById(Integer idcarga) {
        return this.iCargaRepository.findById(idcarga).get();
    }

    @Override
    public List<Carga> findAll() {
        return this.iCargaRepository.findAll();
    }

    @Override
    public List<Carga> findByAlumno(Integer idalumno) {
        return this.iCargaRepository.findByAlumno(idalumno);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.aula;

import com.core.c3.dao.entity.Aulas;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceAula {

    public List<Aulas> findAll();

    public Aulas findById(Integer idaula);

    public Aulas save(Aulas aula);

    public void delete(Aulas aula);
    
    public List<Aulas> findByEdificio(Integer idedificio);
}

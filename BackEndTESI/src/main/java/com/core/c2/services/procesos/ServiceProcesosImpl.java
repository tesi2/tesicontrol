/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.procesos;

import com.core.c3.dao.entity.Procesos;
import com.core.c4.repository.IProcesosRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceProcesosImpl implements IServiceProcesos {

    @Autowired
    private IProcesosRepository iProcesoRepo;

    @Override
    public Procesos save(Procesos proceso) {
        return this.iProcesoRepo.save(proceso);
    }

    @Override
    public Procesos findById(Integer idprocesos) {
        return this.iProcesoRepo.findById(idprocesos).get();
    }

    @Override
    public void delete(Procesos proceso) {
        this.iProcesoRepo.delete(proceso);
    }

    @Override
    public List<Procesos> findAll() {
        return this.iProcesoRepo.findAll();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.horarios;

import com.core.c3.dao.entity.Docentes;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.dao.entity.Horarios;
import com.core.c3.ddd.dto.HorarioDTO;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceHorarios {

    public List<HorarioDTO> obtenerPorGrupo(Grupos grupo);
    
    public List<HorarioDTO> obtenerPorDocente(Docentes docente);
    
    public void guardar(Horarios horario);
    
    public void eliminar(Horarios horario);
    
    
   
}

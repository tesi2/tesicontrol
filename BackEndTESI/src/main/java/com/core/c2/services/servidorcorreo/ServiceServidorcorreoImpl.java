/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.servidorcorreo;

import com.core.c3.dao.entity.Servidorcorreo;
import com.core.c4.repository.IServidorcorreoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author balam
 */
@Service
public class ServiceServidorcorreoImpl implements IServiceServidorcorreo{
    
    @Autowired
    private IServidorcorreoRepository iServidorcorreoRepository;
    
    @Override
    public void guardarServidor(Servidorcorreo servidor){
        this.iServidorcorreoRepository.save(servidor);
    }
    
    @Override
    public List<Servidorcorreo> buscarServidorPorTipo(String tipo){
        return this.iServidorcorreoRepository.buscarPorTipo(tipo);
    }
}

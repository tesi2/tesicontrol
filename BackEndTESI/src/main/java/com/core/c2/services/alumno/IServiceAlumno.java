/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.alumno;

import com.core.c3.dao.entity.Alumnos;
import java.util.List;

/**
 *
 * @author user
 */
public interface IServiceAlumno {

    public List<Alumnos> findAll();
    
    public Alumnos findById(Integer idAlumno);

    public List<Alumnos> findByCarrera(Integer idalumno);

    public Alumnos findByCorreo(String correo);

    public void delete(Alumnos alumno);

    public Alumnos save(Alumnos alumno);
    
    public Alumnos findByMatricula(String pMatricula);
    
    public Alumnos singUp(Alumnos alumno);

}

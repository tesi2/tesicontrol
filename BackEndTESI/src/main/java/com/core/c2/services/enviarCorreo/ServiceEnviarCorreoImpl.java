/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.enviarCorreo;

import com.core.c5.poa.Exeption.TesExcepSeveridad;
import com.core.c5.poa.Exeption.TesException;
import com.core.c5.poa.smtp.SmtpImpl;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Clase encargada de enviar los correos que requiere el TESI para la
 * plataforma en linea
 * 
 * @author Balam
 */
@Service
public class ServiceEnviarCorreoImpl implements IServiceEnviarCorreo{
    
    private static final Logger log = LogManager.getLogger(ServiceEnviarCorreoImpl.class);
    
    /**
     * Este metodo se encarga de enviar un correo al alumno que realizó su registro de
     * inscripción o reincripción, lo unico que notifica es la correcta recpción de
     * documentos.<p>
     * 
     * @param correo    String con la cuenta a la que el correo será enviado
     * @param nombre    String con el nombre del alumno que realizó su registro
     */
    @Override
    public void notificarRegistro(String correo, String nombre) {
        String asunto = "Recepcion de solicitud para incripción en el Tecnologico de Estudios Superiores de Ixtapaluca";
        List<String> correoList = new ArrayList<>();
        correoList.add(correo);
        String mensaje = "<h3>Estimada(o) " + nombre + "</h3>";
        mensaje += "<p>Hemos recibido correctamente tu documentación para tu inscripción/reinscripción. En caso de ser "
                + "correcta tanto la información como los documentos, o si alguno de ellos pudiera estar incorrecto o "
                + "necesitara alguna aclaración, por este medio, te notificaremos las correcciones a realizar o en su "
                + "defecto las fechas para que te presentes en control escolar y los documentos necesarios para finalizar "
                + "tu trámite.</p><br/>";
        mensaje += "<h3 align=\"center\">Atentamente el Tecnológico de Estudios Superiores de Ixtapaluca.</h3>";
        mensaje += "<h4 align=\"center\">Este mensaje es generado en automático, favor de no responder.</h4>";
        
        try {
            this.enviarCorreo(asunto, correoList, null, null, "HTML", mensaje, null);
        }catch (Exception ex) {
            throw new TesException(0602, "No se pudo enviar el correo de notificacion a la cuenta " + correo, this.getClass().getName(), TesExcepSeveridad.FATAL, ex);
        }
        
    }
    
    /**
    * Este metodo se encarga de enviar un correo simple 
    * 
    * @param  asunto String con el asunto del correo a enviar
    * @param  receptores Lista de String con las cuentas a enviar el correo
    * @param  mensaje String con el mensaje a enviar, por default es de tipo TXT
    */
    @Override
    public void eniviarCorreo(String asunto, List<String> receptores, String mensaje) {
        
        SmtpImpl smtp = new SmtpImpl("smtp.gmail.com","balam.m.hernandez@gmail.com","pajaritopanzon");
        
        try {
            if (asunto!= null && !asunto.equals("")) {
                smtp.setAsunto(asunto);
            } else {
                throw new TesException(605, "No se permite enviar un correo sin asunto", this.getClass().getName(), TesExcepSeveridad.INFO);
            }            
            if (receptores != null && !receptores.isEmpty()) {
                smtp.setReceptoresList(receptores);
            } else {
                throw new TesException(604, "El correo necesita tener un receptor", this.getClass().getName(), TesExcepSeveridad.INFO);
            }
            smtp.setFormatoConteido("TXT");
            if (mensaje != null && !mensaje.equals("")) {
                smtp.setMensaje(mensaje);
            } else {
                throw new TesException(606, "No se permite enviar un correo con el mensaje en blanco", this.getClass().getName(), TesExcepSeveridad.INFO);
            }
            
            smtp.enviarCorreo();
        } catch (TesException ex) {
            this.log.info("Error al enviar el correo. Causa: " + ex.getError());
            throw ex;
        }
    }
    
    /**
     * Este metodo se encarga de enviar un correo pero requiere de todas las
     * variables <p>
     * 
     * @param asunto String con el asunto del correo a enviar
     * @param receptores Lista de String con las cuentas a enviar el correo
     * @param copiados Lista de String con las cuentas a enviar el correo que se colocaran como "CC" (con copia)
     * @param ocultos Lista de String con las cuentas a enviar el correo que se colocaran como "CCO" (con copia oculta)
     * @param formato String tipo enum, <ul><li>TXT</li><li>HTML</li></ul>
     * @param mensaje String con el mensaje a enviar
     * @param adjuntoList Lista de File con los archivos que se colocaran adjuntos
     */
    @Override
    public void enviarCorreo(String asunto, List<String> receptores, List<String> copiados, List<String> ocultos, String formato, String mensaje, List<File> adjuntoList) {
        
        SmtpImpl smtp = new SmtpImpl("smtp.gmail.com","balam.m.hernandez@gmail.com","pajaritopanzon");
        
        try {
            if (asunto!= null && !asunto.equals("")) {
                smtp.setAsunto(asunto);
            } else {
                throw new TesException(605, "No se permite enviar un correo sin asunto", this.getClass().getName(), TesExcepSeveridad.INFO);
            }
            
            if (receptores != null) {
                smtp.setReceptoresList(receptores);
            }
            if (copiados != null) {
                smtp.setCopiasList(copiados);
            }
            if (ocultos != null) {
                smtp.setCopiasocultasList(ocultos);
            }
            if (receptores == null && copiados == null && ocultos == null) {
                throw new TesException(604, "El correo necesita tener algun receptor", this.getClass().getName(), TesExcepSeveridad.INFO);
            }
            
            smtp.setFormatoConteido(formato);
            if (mensaje != null && !mensaje.equals("")) {
                smtp.setMensaje(mensaje);
            } else {
                throw new TesException(606, "No se permite enviar un correo con el mensaje en blanco", this.getClass().getName(), TesExcepSeveridad.INFO);
            }
            if (adjuntoList != null && !adjuntoList.isEmpty()) {
                smtp.setAdjuntosFileList(adjuntoList);
            }
            smtp.enviarCorreo();
        } catch (TesException ex) {
            this.log.info("Error al enviar los correos. Causa: " + ex.getError());
            throw ex;
        }
        
    }
    
}

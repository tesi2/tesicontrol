/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.aula;

import com.core.c3.dao.entity.Aulas;
import com.core.c4.repository.IAulasRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceAulaImpl implements IServiceAula {

    @Autowired
    private IAulasRepository iAulaRepository;

    @Override
    public List<Aulas> findAll() {
        return this.iAulaRepository.findAll();
    }

    @Override
    public Aulas findById(Integer idaula) {
        return this.iAulaRepository.findById(idaula).get();
    }

    @Override
    public Aulas save(Aulas aula) {
        return this.iAulaRepository.save(aula);
    }

    @Override
    public void delete(Aulas aula) {
        this.iAulaRepository.delete(aula);
    }

    @Override
    public List<Aulas> findByEdificio(Integer idedificio) {
       return this.iAulaRepository.findByEdificio(idedificio);
    }

}

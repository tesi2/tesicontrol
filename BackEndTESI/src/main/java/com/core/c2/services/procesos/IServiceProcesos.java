/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.procesos;

import com.core.c3.dao.entity.Procesos;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceProcesos {

    public Procesos save(Procesos proceso);

    public Procesos findById(Integer idprocesos);

    public void delete(Procesos proceso);

    public List<Procesos> findAll();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.users;

import com.core.c3.dao.entity.Users;
import java.util.List;

/**
 *
 * @author user
 */
public interface IServiceUsers {

    public List<Users> getAll();

    public Users findByUserName(String userName);

    public Users addAlumno(String username);
}

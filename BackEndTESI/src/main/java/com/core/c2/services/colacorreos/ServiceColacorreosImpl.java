/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.colacorreos;

import com.core.c3.dao.entity.Colacorreos;
import com.core.c4.repository.IColacorreosRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author balam
 */
@Service
public class ServiceColacorreosImpl implements IServiceColacorreos{
    
    @Autowired
    private IColacorreosRepository iColacorreosRepository;
    
    @Override
    public List<Colacorreos> sinEnviar(){
        return this.iColacorreosRepository.buscarSinEnviar();
    }
    
    @Override
    public void guardar(Colacorreos colacorrreo){
        this.iColacorreosRepository.save(colacorrreo);
    }
    
    @Override
    public void encolarNotificacionExitosa (String correo, String nombre) {
        String asunto = "Resultado de la revision de documentos";
        String mensaje = "<h3>Estimada(o) " + nombre + "</h3>";
        mensaje += "<p>Hemos revisado tu documentación y nos complace notificarte que esta se encuentra "
                + "correcta. Por favor presenta la misma en ventanilla para cotejar y terminar con tu proceso.</p><br/>";
        mensaje += "<h3 align=\"center\">Atentamente el Tecnológico de Estudios Superiores de Ixtapaluca.</h3>";
        mensaje += "<h4 align=\"center\">Este mensaje es generado en automático, favor de no responder.</h4>";
        
        this.asignar(correo, asunto, mensaje);
    }
    
    @Override
    public void encolarNotificacionObservacion(String correo, String nombre, String observaciones) {
        String asunto = "Resultado de la revision de documentos";
        String mensaje = "<h3>Estimada(o) " + nombre + "</h3>";
        mensaje += "<p>Hemos revisado tu documentación y emos encontrado algunos detalles, por favor corrígelos "
                + "cuanto antes para que puedas continuar con tu tramite.</p><br/>";
        mensaje += "<p>" + observaciones + "</p><br/";
        mensaje += "<h3 align=\"center\">Atentamente el Tecnológico de Estudios Superiores de Ixtapaluca.</h3>";
        mensaje += "<h4 align=\"center\">Este mensaje es generado en automático, favor de no responder.</h4>";
        
        this.asignar(correo, asunto, mensaje);
    }
    
    private void asignar(String correo, String asunto, String mensaje) {
        Colacorreos colacorreos = new Colacorreos();
        colacorreos.setReceptores(correo);
        colacorreos.setAsunto(asunto);
        colacorreos.setTipocuerpo("HTML");
        colacorreos.setCuerpo(mensaje);
        colacorreos.setEnviado(Boolean.FALSE);
        colacorreos.setFechageneracion(new Date());
        this.iColacorreosRepository.save(colacorreos);
    }
    
}

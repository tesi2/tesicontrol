/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.administrativo;

import com.core.c3.dao.entity.Administrativos;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceAdministrativo {

    public Administrativos save(Administrativos admin);

    public void delete(Administrativos admin);

    public Administrativos findById(Integer IdAdministrativo);

    public Administrativos findByCorreo(String correo);

    public List<Administrativos> findAll();
}

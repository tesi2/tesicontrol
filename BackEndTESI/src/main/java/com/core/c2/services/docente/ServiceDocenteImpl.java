/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.docente;

import com.core.c3.dao.entity.Docentes;
import com.core.c4.repository.IDocentesRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceDocenteImpl implements IServiceDocente {
    
    @Autowired
    private IDocentesRepository iDocentesRepo;
    
    @Override
    public List<Docentes> findAll() {
        return this.iDocentesRepo.findAll();
    }
    
    @Override
    public Docentes save(Docentes docente) {
        return this.iDocentesRepo.save(docente);
    }
    
    @Override
    public void delete(Docentes docente) {
        this.iDocentesRepo.delete(docente);
    }
    
    @Override
    public Docentes findByCorreo(String correo) {
        return this.iDocentesRepo.findByCorreo(correo);
    }
    
    @Override
    public Docentes findById(Integer iddocente) {
        return this.iDocentesRepo.findById(iddocente).get();
    }
    
}

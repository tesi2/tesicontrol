/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.edificios;

import com.core.c3.dao.entity.Edificios;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceEdificios {

    public Edificios save(Edificios edificio);

    public Edificios findById(Integer idedificio);

    public void delete(Edificios edificio);

    public List<Edificios> findAll();
    
    public Edificios findByNombre(String nombre);
}

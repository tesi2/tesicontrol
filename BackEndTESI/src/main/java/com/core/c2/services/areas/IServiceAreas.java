/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.areas;

import com.core.c3.dao.entity.Areas;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceAreas {

    public Areas findById(Integer idarea);

    public List<Areas> findAll();

}

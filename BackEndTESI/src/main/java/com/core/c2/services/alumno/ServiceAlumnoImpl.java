/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.alumno;

import com.core.c2.services.carrera.IServiceCarrera;
import com.core.c2.services.grupos.IServiceGrupos;
import com.core.c2.services.users.IServiceUsers;
import com.core.c3.dao.entity.Alumnos;
import com.core.c3.dao.entity.Carreras;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.dao.entity.Users;
import com.core.c4.repository.IAlumnoRepository;
import com.core.c5.poa.Date.DateUtil;
import java.util.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author user
 */
@Service
public class ServiceAlumnoImpl implements IServiceAlumno {

    private static final Logger log = LogManager.getLogger(ServiceAlumnoImpl.class);
    @Autowired
    private IAlumnoRepository iAlumnoRepository;
    @Autowired
    private IServiceCarrera iServiceCarrera;
    @Autowired
    private IServiceGrupos iServiceGrupo;
    @Autowired
    private IServiceUsers iServiceUsers;

    @Override
    public List<Alumnos> findAll() {
        return this.iAlumnoRepository.findAll();
    }

    @Override
    public List<Alumnos> findByCarrera(Integer idalumno) {
        return this.iAlumnoRepository.listarAlumnosPorCarrera(idalumno);
    }

    @Override
    public void delete(Alumnos alumno) {
        this.iAlumnoRepository.delete(alumno);
    }

    @Override
    public Alumnos save(Alumnos alumno) {
        return this.iAlumnoRepository.save(alumno);
    }

    @Override
    public Alumnos findByCorreo(String correo) {
        return this.iAlumnoRepository.buscarPorCorreo(correo);
    }

    @Override
    public Alumnos findById(Integer idAlumno) {
        return this.iAlumnoRepository.findById(idAlumno).get();
    }

    @Override
    public Alumnos findByMatricula(String pMatricula) {
        return this.iAlumnoRepository.findByMatricula(pMatricula);
    }

    @Override
    public Alumnos singUp(Alumnos alumno) {
        String nombreStr = alumno.getNombre() + " " + alumno.getPaterno() + " " + alumno.getMaterno();
        Carreras carrera = this.iServiceCarrera.findById(alumno.getIdcarrera().getIdcarrera());
        log.info("@TESI---->Agregando al alumno: " + nombreStr + " como aspirante de la carrera " + carrera.getNombre());
        Grupos grupo = this.iServiceGrupo.getGrupoDefault();
        log.info("@TESI---->ASignando al grupo: " + grupo.getCodigo());
        alumno.setIdcarrera(carrera);
        alumno.setIdgrupo(grupo);
        String matricula;
        do {
            matricula = this.generarMatriculaTemporal();
        } while (!this.validarMatricula(matricula));
        alumno.setMatricula(matricula);
        log.info("@TESI---->Se asigno la matricula: " + matricula + "al alumno: " + nombreStr);
        alumno.setTipo("CANDIDATO");
        alumno = this.ajustarCampos(alumno);
        alumno.setInscrito(false);
        Users user = this.iServiceUsers.addAlumno(alumno.getCorreo());
        log.info("@TESI---->Se registro el user con id: "+user.getIdusers());
        return this.save(alumno);
        //asincrona envie correo de notificacion: 
    }

    public String generarMatriculaTemporal() {
        String prefijo = "0000";
        String mes = DateUtil.obtenerFechaConFormato(new Date(), "MM");
        String dia = DateUtil.obtenerFechaConFormato(new Date(), "dd");
        String numeroAleatorio = String.valueOf((int)(Math.random() * 99 + 1));
        String matricula = prefijo + dia + mes + numeroAleatorio;
        return matricula;
    }

    public boolean validarMatricula(String matricula) {
        log.info("@TESI---->Validando matricula: " + matricula);
        Alumnos alumno = this.findByMatricula(matricula);
        if (alumno != null) {
            return false;
        } else {
            return true;
        }

    }

    private Alumnos ajustarCampos(Alumnos alumno) {
        alumno.setNombre(alumno.getNombre().toUpperCase());
        alumno.setPaterno(alumno.getPaterno().toUpperCase());
        alumno.setMaterno(alumno.getMaterno().toUpperCase());
        return alumno;
    }
}

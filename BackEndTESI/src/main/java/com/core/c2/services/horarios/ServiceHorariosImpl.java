/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.horarios;

import com.core.c3.dao.entity.Aulas;
import com.core.c3.dao.entity.Docentes;
import com.core.c3.dao.entity.Docentesmaterias;
import com.core.c3.dao.entity.Grupos;
import com.core.c3.dao.entity.Horarios;
import com.core.c3.dao.entity.Materias;
import com.core.c3.ddd.dto.HorarioDTO;
import com.core.c4.repository.IAulasRepository;
import com.core.c4.repository.IDocentesRepository;
import com.core.c4.repository.IDocentesmateriasRepository;
import com.core.c4.repository.IGruposRepository;
import com.core.c4.repository.IHorariosRepository;
import com.core.c4.repository.IMateriaRepository;
import com.core.c5.poa.Exeption.TesExcepSeveridad;
import com.core.c5.poa.Exeption.TesException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceHorariosImpl implements IServiceHorarios {

    @Autowired
    private IHorariosRepository iHorariosRepository;
    @Autowired
    private IDocentesmateriasRepository iDocentesmateriasRepository;
    @Autowired
    private IMateriaRepository iMateriaRepository;
    @Autowired
    private IDocentesRepository iDocentesRepository;
    @Autowired
    private IAulasRepository iAulasRepository;
    @Autowired
    private IGruposRepository iGruposRepository;

    @Override
    public List<HorarioDTO> obtenerPorGrupo(Grupos grupo) {

        List<HorarioDTO> horariosDTOList = new ArrayList<>();
        List<Horarios> horariosList = this.iHorariosRepository.buscarPorGrupo(grupo);

        if (horariosList != null && !horariosList.isEmpty()) {
            horariosList.forEach((horario) -> {
                Docentesmaterias docmat = this.iDocentesmateriasRepository.buscarPorId(horario.getIddocentesmateriascol().getIddocentesmateriascol());
                Materias materia = this.iMateriaRepository.buscarPorId(docmat.getIdmateria().getIdmateria());
                Docentes docente = this.iDocentesRepository.buscarPorId(docmat.getIddocente().getIddocente());
                Aulas aula = this.iAulasRepository.buscarPorId(grupo.getIdaula().getIdaula());
                
                HorarioDTO horarioDTO = this.asignarDTO(horario, docmat, materia, docente, aula, grupo);
                horariosDTOList.add(horarioDTO);
            });
        } else {
            throw new TesException(101, "El grupo no tiene un horario asignado", "ServiceHorariosImpl", TesExcepSeveridad.FATAL);
        }

        return horariosDTOList;
    }
    
    @Override
    public List<HorarioDTO> obtenerPorDocente(Docentes docente) {
        List<HorarioDTO> horariosDTOList = new ArrayList<>();
        List<Docentesmaterias> docmatList = this.iDocentesmateriasRepository.buscarPorDocente(docente);
        
        if (docmatList != null && !docmatList.isEmpty()) {
            docmatList.forEach((docmat) -> {
                Horarios horario = this.iHorariosRepository.buscarPorDocentemateria(docmat);
                if (horario != null) {
                    Grupos grupo = this.iGruposRepository.buscarPorId(horario.getIdgrupo().getIdgrupo());
                    Materias materia = this.iMateriaRepository.buscarPorId(docmat.getIdmateria().getIdmateria());
                    Aulas aula = this.iAulasRepository.buscarPorId(grupo.getIdaula().getIdaula());

                    HorarioDTO horarioDTO = this.asignarDTO(horario, docmat, materia, docente, aula, grupo);
                    horariosDTOList.add(horarioDTO);
                }
            });
        } else {
            throw new TesException(101, "El Docente no tiene materias asignadas", "ServiceHorariosImpl", TesExcepSeveridad.FATAL);
        }

        return horariosDTOList;
    }
    
    private HorarioDTO asignarDTO(Horarios horario, Docentesmaterias docmat, Materias materia, Docentes docente, Aulas aula, Grupos grupo) {
        HorarioDTO horarioDTO = new HorarioDTO();

        horarioDTO.setHorario(horario);
        horarioDTO.setDocentesmaterias(docmat);
        horarioDTO.setMateria(materia);
        horarioDTO.setDocente(docente);
        horarioDTO.setAula(aula);
        horarioDTO.setGrupo(grupo);
        
        return horarioDTO;
    }
    
    @Override
    public void guardar(Horarios horario){
        this.iHorariosRepository.save(horario);
    }
    
    @Override
    public void eliminar(Horarios horario){
        this.iHorariosRepository.delete(horario);
    }

}

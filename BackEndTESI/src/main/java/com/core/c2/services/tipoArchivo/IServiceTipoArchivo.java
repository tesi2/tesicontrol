/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.tipoArchivo;

import com.core.c3.dao.entity.Tipoarchivo;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceTipoArchivo {

    public void delete(Tipoarchivo tipoArchivo);

    public Tipoarchivo save(Tipoarchivo tipoArchivo);

    public Tipoarchivo findById(Integer idTipoArchivo);

    public List<Tipoarchivo> findAll();
}

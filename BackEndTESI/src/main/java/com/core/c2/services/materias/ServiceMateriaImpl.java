/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.materias;

import com.core.c3.dao.entity.Materias;
import com.core.c4.repository.IMateriaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceMateriaImpl implements IServiceMateria {

    @Autowired
    private IMateriaRepository iMateriaRepo;

    @Override
    public void delete(Materias materia) {
        this.iMateriaRepo.delete(materia);
    }

    @Override
    public Materias save(Materias materia) {
        return this.iMateriaRepo.save(materia);
    }

    @Override
    public Materias findById(Integer idmateria) {
        return this.iMateriaRepo.findById(idmateria).get();
    }

    @Override
    public List<Materias> findAll() {
        return this.iMateriaRepo.findAll();
    }

    @Override
    public List<Materias> findByIdCarrera(Integer idmateria) {
        return this.iMateriaRepo.findByIdCarrera(idmateria);
    }

    @Override
    public Materias findByNombre(String nombre) {
        return this.iMateriaRepo.findByNombre(nombre);
    }

    @Override
    public List<Materias> findByIdDocente(Integer iddocente) {
        return this.iMateriaRepo.findByDocente(iddocente);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.docente;

import com.core.c3.dao.entity.Docentes;
import java.util.List;

/**
 *
 * @author Alan Isaac Villafan
 */
public interface IServiceDocente {

    public List<Docentes> findAll();

    public Docentes save(Docentes docente);

    public void delete(Docentes docente);

    public Docentes findByCorreo(String correo);
    
    public Docentes findById(Integer iddocente);
}

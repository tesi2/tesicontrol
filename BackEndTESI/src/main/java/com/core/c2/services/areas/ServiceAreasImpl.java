/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.areas;

import com.core.c3.dao.entity.Areas;
import com.core.c4.repository.IAreasRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceAreasImpl implements IServiceAreas {

    @Autowired
    private IAreasRepository iAreaRepository;

    @Override
    public Areas findById(Integer idarea) {
        return this.iAreaRepository.findById(idarea).get();
    }

    @Override
    public List<Areas> findAll() {
        return this.iAreaRepository.findAll();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.colacorreos;

import com.core.c3.dao.entity.Colacorreos;
import java.util.List;

/**
 *
 * @author balam
 */
public interface IServiceColacorreos {
    
    public List<Colacorreos> sinEnviar();
    
    public void guardar(Colacorreos colacorrreo);
    
    public void encolarNotificacionExitosa (String correo, String nombre);
    
    public void encolarNotificacionObservacion(String correo, String nombre, String observaciones);
    
}

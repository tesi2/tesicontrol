/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c2.services.roles;

import com.core.c3.dao.entity.Roles;
import com.core.c4.repository.IRolRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alan Isaac Villafan
 */
@Service
public class ServiceRolesImpl implements IServiceRoles {

    @Autowired
    private IRolRepository iRolRepository;

    @Override
    public void delete(Roles rol) {
        this.iRolRepository.delete(rol);
    }

    @Override
    public Roles findById(Integer idrol) {
        return this.iRolRepository.findById(idrol).get();
    }

    @Override
    public Roles save(Roles rol) {
        return this.iRolRepository.save(rol);
    }

    @Override
    public List<Roles> findAll() {
        return this.iRolRepository.findAll();
    }

}

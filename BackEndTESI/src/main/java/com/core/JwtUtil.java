/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core;

import com.core.c2.services.users.IServiceUsers;
import com.core.c3.dao.entity.Users;
import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.authenticator.SpnegoAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 *
 * @author user
 */
public class JwtUtil {
    @Autowired
    private IServiceUsers iServiceUsers;
    static void addAuthentication(HttpServletResponse res, String username) {
    String token = Jwts.builder().setSubject(username).signWith(SignatureAlgorithm.HS512, "TESI").compact();
    res.addHeader("Autorization",  token);
        try {
            String json = new Gson().toJson(token);
            res.getWriter().write(json);
        } catch (IOException ex) {
            Logger.getLogger(JwtUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    res.addHeader("username", username);
    }
    static Authentication getAuthentication(HttpServletRequest request){
    String token = request.getHeader("Authorization");
    if(token!=null){
    String user = Jwts.parser().setSigningKey("TESI").parseClaimsJws(token.replace("Bearer", "")).getBody().getSubject();
    return user!= null ? new UsernamePasswordAuthenticationToken(user, null,Collections.emptyList()):null;
    }
    return null;
    }
}

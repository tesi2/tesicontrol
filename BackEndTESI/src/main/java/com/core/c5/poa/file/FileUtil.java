/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.file;

import com.core.c5.poa.Exeption.TesExcepSeveridad;
import com.core.c5.poa.Exeption.TesException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;

/**
 * Este clase se encarga de la mayora de accionesque se
 * pueden realizar para un archivo como converciones,
 * obtener nombre, ruta o extencion, etc.
 * 
 * @author Balam
 */
public class FileUtil extends File{
    
    public FileUtil(String pathname) {
        super(pathname);
    }
    
    /**
     * Este método se encarga de obtener el nombre de un archivo,
     * al pasarle un nombre como <code>archivo.txt</code>
     * devolverá solo el texto <code>archivo</code>
     * 
     * @param nombre Tipo String
     * @return String con el nombre del alchivo
     */
    public static String obtenerNobreArchivo(String nombre) {
        Integer index = nombre.lastIndexOf('.');
        String nombreArchivo;
        nombreArchivo = nombre.substring(0, index);
        return nombreArchivo;
    }
    
    /**
     * Este método se encarga de obtener la extención de un archivo,
     * al pasarle un nombre como <code>archivo.txt</code>
     * devolverá solo el texto <code>txt</code>
     * 
     * @param nombre Tipo String
     * @return String con la extención del alchivo
     */
    public static String obtenerExtencion(String nombre) {
        Integer index = nombre.lastIndexOf('.');
        String extencion;
        extencion = nombre.substring(index + 1);
        return extencion;
    }
    
    /**
     * Este método se encarga de obtener el MimeType de un archivo,
     * al pasarle la extención de un archivo como <code>pdf</code> devolverá el
     * texto <code>application/pdf</code>
     * 
     * @param extencion Tipo String
     * @return String con el MimeType de la extencion proporcionada
     */
    public static String obtenerMimeTypeDeExtension (String extencion) {
        switch (extencion) {
            case "pdf": {
                return "application/pdf";
            }
            case "xml": {
                return "application/xml";
            }
            case "csv": {
                return "text/csv";
            }
            default: {
                return "text/plain";
            }
        }
    }
    
    /**
     * Este método se encarga de convertir un archivo a Base64
     * al recibir una variable de tipo File regresará
     * una cadena de string que correspone al archivo codificado
     * 
     * @param file El archivo a codificar
     * @return String con el archivo codificado
     */
    public static String fileToBase64 (File file) {
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            byte[] encoded = Base64.getEncoder().encode(bytes);
            String stringCodificado = new String(encoded, StandardCharsets.UTF_8);

            return stringCodificado;
        } catch (IOException ex) {
            throw new TesException(700, "No se pudo convertir el archivo a Base 64", "FileUtil", TesExcepSeveridad.FATAL, ex);
        }
    }
    
    /**
     * Este método se encarga de convertir una cadena de String a un archivo
     * y colocarlo en la carpeta TEMP del sitio, se debe de proporcionar el
     * nombre y la extencion que tendra al finalizar la conversión 
     * 
     * @param b64 Cadena de String correspondiente al archivo
     * @param nombre String con el nombre que tendra el archivo
     * @param extension String que indica el tipo/extencion del archivo
     * @return Variable tipo File del archivo decodificado en la carpeta TEMP del sitio
     */
    public static File base64ToFileTemp(String b64, String nombre, String extension) {
	FileOutputStream fos = null;
        try {
            File archivo = new File("C:\\sitioTES\\TEMP" + separator + nombre + "." + extension);
            byte[] decoded = Base64.getDecoder().decode(b64);
            fos = new FileOutputStream(archivo);
            fos.write(decoded);
            return archivo;
        } catch (FileNotFoundException ex) {
                throw new TesException(701, "No se encontro el archivo ", "FileUtil", TesExcepSeveridad.FATAL, ex);
        } catch (IOException ex) {
                throw new TesException(702, "No se puede leer el archivo ", "FileUtil", TesExcepSeveridad.FATAL, ex);
        } finally {
            try {
                if (fos != null){
                    fos.close();
                }
            } catch (IOException ex) {
                throw new TesException(702, "No se puede leer el archivo", "FileUtil", TesExcepSeveridad.FATAL, ex);
            }
        }
    }
    
    /**
     * Este método se encarga de convertir una cadena de String a un archivo
     * y colocarlo en la ruta proporcionada (debe incluir el nombre y la extencion) 
     * 
     * @param b64 Cadena de String correspondiente al archivo
     * @param pathFile String correspondiente a la ruta del archivo <code>C:\Users\User\Desktop\archivo.txt</code>
     * @return Variable tipo File del archivo decodificado en la ruta especificada
     */
    public static File base64ToFile(String b64, String pathFile) {
        FileOutputStream fos = null;
        try {
            File archivo = new File(pathFile);
            byte[] decoded = Base64.getDecoder().decode(b64);
            fos = new FileOutputStream(archivo);
            fos.write(decoded);
            return archivo;
        } catch (FileNotFoundException ex) {
                throw new TesException(701, "No se encontro el archivo ", "FileUtil", TesExcepSeveridad.FATAL, ex);
        } catch (IOException ex) {
                throw new TesException(702, "No se puede leer el archivo ", "FileUtil", TesExcepSeveridad.FATAL, ex);
        } finally {
            try {
                if (fos != null){
                    fos.close();
                }
            } catch (IOException ex) {
                throw new TesException(702, "No se puede leer el archivo ", "FileUtil", TesExcepSeveridad.FATAL, ex);
            }
        }
    }
}

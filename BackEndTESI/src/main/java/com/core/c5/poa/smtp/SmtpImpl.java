/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.smtp;

import com.core.c5.poa.Exeption.TesExcepSeveridad;
import com.core.c5.poa.Exeption.TesException;
import com.core.c5.poa.file.FileUtil;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

/**
 *Clase para enviar correos por SMTP.<p>
 * 
 * Esta clase esta pensada para poder enviar correos tipo SMTP
 * desde una cuenta especificada, la clase primero valida las
 * credenciales de dicha cuenta, despues arma el correo, si todo
 * pasa correctamente este es enviado, cualquier error se imprime
 * en el log.
 * 
 * @author Balam
 */
public class SmtpImpl implements ISmtp{
    
    private String asunto;
    private List<String> receptoresList;
    private Address[] addressResceptores = null;
    private List<String> copiasList;
    private Address[] addressCopias = null;
    private List<String> copiasocultasList;
    private Address[] addressCopiasOcultas = null;
    private String formatoConteido;
    private String mensaje;
    private List<File> adjuntosFileList;
    
    protected String host;
    protected String usuario;
    protected String clave;

    /**
     *Constructor del Smtp.<p>
     * 
     * Para poder usar esta clase se requiere la siguiente inormación
     * 
     * @param host      Tipo string, servidor al que pertence el correo, ejemplo: smtp.gmail.com
     * @param usuario   Tipo string, correo desde el que se desea mandar el e-mail
     * @param clave     Tipo string, contraseña perteneciente al correo que se desea usar
     */
    public SmtpImpl(String host, String usuario, String clave) {
        this.host = host;
        this.usuario = usuario;
        this.clave = clave;
    }
    
    @Override
    public void enviarCorreo() {
        
        final String usser = this.usuario; //La cuenta a usar
        final String pass = this.clave; //La clave de la cuenta
        
        Properties props = System.getProperties();
        props.put("mail.smtp.host", this.host);     //El servidor SMTP
        props.put("mail.smtp.auth", "true");        //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587");         //El puerto SMTP seguro (rara vez cambia, asi que mejor se hard-codea)
        
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usser, pass);
            }
        };
        
        Session session = Session.getInstance(props, auth);
        this.armarCorreo(session);
    }
    
    private void armarCorreo(Session session) {
        try {
            Logger.getLogger(SmtpImpl.class.getName()).log(Level.INFO, "Armando correo");
            MimeMultipart multiParte = new MimeMultipart();
            MimeMessage msg = new MimeMessage(session);
            
            //Se coloca el emisor del correo
            msg.setFrom(new InternetAddress(this.usuario));
            //Se coloca el asunto del correo
            msg.setSubject(this.asunto, "UTF-8");
            //Se coloca la fecha de envio del correo
            msg.setSentDate(new Date());
            
            //Se defie el tipo de contenido para el mensaje y se coloca el mismo
            MimeBodyPart mnsgHtml = new MimeBodyPart();
            switch(this.formatoConteido) {
                case "HTML": {
                    mnsgHtml.setContent(this.mensaje, "text/html; charset=utf-8");
                    break;
                }
                case "TXT": {
                    mnsgHtml.setContent(this.mensaje, "text/plain; charset=utf-8");
                    break;
                }
                default: {
                    mnsgHtml.setContent(this.mensaje, "text/plain; charset=utf-8");
                    break;
                }
            }            
            multiParte.addBodyPart(mnsgHtml);
                    
            //Se colocan los receptores
            if (this.addressResceptores != null) {
                msg.setRecipients(Message.RecipientType.TO, this.addressResceptores);
            }
            if (this.addressCopias != null) {
                msg.setRecipients(Message.RecipientType.CC, this.addressCopias);                
            }
            if (this.addressCopiasOcultas != null) {
                msg.setRecipients(Message.RecipientType.BCC, this.addressCopiasOcultas);
            }
            
            //Si existen archivos adjuntos se colocan
            if (this.adjuntosFileList != null && !this.adjuntosFileList.isEmpty()) {
                for (File file : this.adjuntosFileList) {
//                    File f = FilesEdcmm.base64AFile(adjunto, "csv");
                    byte[] adArray = Files.readAllBytes(file.toPath());

                    MimeBodyPart adj = new MimeBodyPart();
                    
                    String extension = FileUtil.obtenerExtencion(file.getName());
                    String mimeType = FileUtil.obtenerMimeTypeDeExtension(extension);
                    
                    ByteArrayDataSource ds = new ByteArrayDataSource(adArray, mimeType);
                    ds.setName("Archivo adjunto");
                    DataHandler dh = new DataHandler(ds);
                    adj.setDataHandler(dh);                   
                    
                    adj.setFileName(file.getName());
                    adj.setDescription(file.getName());

                    multiParte.addBodyPart(adj);
                }
            }
            
            msg.setContent(multiParte);
            
            Logger.getLogger(SmtpImpl.class.getName()).log(Level.INFO, "Enviando correo");
            Transport.send(msg);
            Logger.getLogger(SmtpImpl.class.getName()).log(Level.INFO, "Correo enviado exitosamente");
            
        } catch (MessagingException | IOException exc) {
            throw new TesException(602,"Error al armar o enviar el correo" , this.getClass().getName(), TesExcepSeveridad.FATAL, exc);
        }
        
    }
    
    private InternetAddress[] obtenerArrayCorreos(List<String> list) {
        InternetAddress[] arrayReceptores = new InternetAddress[list.size()];
        int i = 0;
        for (String correo : list) {
            try {
                if (this.validarCorreo(correo)) {
                    InternetAddress ia = new InternetAddress(correo, false);
                    arrayReceptores[i++] = ia;
                } else {
                    throw new Exception("El correo: " + correo + " posee un formato inválido para la expresión regular");
                }
            } catch (Exception ex) {
                Logger.getLogger(SmtpImpl.class.getName()).log(Level.SEVERE, "Error al validar los correos. ", ex);
                throw new TesException(603, "Error al validar los correos.", this.getClass().getName(), TesExcepSeveridad.FATAL, ex);
            }
        }
        return arrayReceptores;
    }
    
    private boolean validarCorreo(String correo) {
        String expRegular = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(expRegular);
        Matcher m = p.matcher(correo);
        return m.matches();
    }
    
    @Override
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    @Override
    public void setReceptoresList(List<String> receptoresList) {
        this.receptoresList = receptoresList;
        this.addressResceptores = this.obtenerArrayCorreos(this.receptoresList);
    }

    @Override
    public void setCopiasList(List<String> copiasList) {
        this.copiasList = copiasList;
        this.addressCopias = this.obtenerArrayCorreos(this.copiasList);
    }

    @Override
    public void setCopiasocultasList(List<String> copiasocultasList) {
        this.copiasocultasList = copiasocultasList;
        this.addressCopiasOcultas = this.obtenerArrayCorreos(this.copiasocultasList);
    }

    @Override
    public void setFormatoConteido(String formatoConteido) {
        this.formatoConteido = formatoConteido;
    }
    
    @Override
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public void setAdjuntosFileList(List<File> adjuntosFileList) {
        this.adjuntosFileList = adjuntosFileList;
    }
    
}

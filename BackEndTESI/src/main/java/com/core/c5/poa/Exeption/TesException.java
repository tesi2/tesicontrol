/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.Exeption;

/**
 * Esta clase es utilizada para cachar las excepciones que se den
 * mientras alguna otra clase realiza alguna accion contenida
 * dentro de un <code>try-catch</code>.
 * 
 * Se usa en conjunto a la clase enum "TesExcepSeveridad" para indicar
 * el nivel del error.
 * 
 * @author balam
 */
public class TesException extends RuntimeException{
    
    private final int codigo;
    private final String descripcion;
    private final String clase;
    private final TesExcepSeveridad serveridad;

    /**
     * Contructor de TesException sin el Throwable del catch.<p>
     * 
     * Se deven de indicar las siguientes propiedades para poder usar esta clase:
     * 
     * @param codigo        Tipo int, codigo de error, revisar el catalogo para saber el indicado.
     * @param descripcion   Tipo string, breve descripción del error ocurrido.
     * @param clase         Tipo string, clase en la que ocurrio el error.
     * @param serveridad    Tipo enum, revisar clase TesExcepSeveridad para saber cual es el adecuado.
     */
    public TesException(int codigo, String descripcion, String clase, TesExcepSeveridad serveridad) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.clase = clase;
        this.serveridad = serveridad;
    }

    /**
     * Contructor de TesException que usa el Throwable del catch.<p>
     * 
     * Se deven de indicar las siguientes propiedades para poder usar esta clase:
     * 
     * @param codigo        Tipo int, codigo de error, revisar el catalogo para saber el indicado.
     * @param descripcion   Tipo string, breve descripción del error ocurrido.
     * @param clase         Tipo string, clase en la que ocurrio el error.
     * @param serveridad    Tipo enum, revisar clase TesExcepSeveridad para saber cual es el adecuado.
     * @param cause         Tipo throwable, propiedad que contiene la causa raiz del error.
     */
    public TesException(int codigo, String descripcion, String clase, TesExcepSeveridad serveridad, Throwable cause) {
        super(cause);
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.clase = clase;
        this.serveridad = serveridad;
    }
    
    /**
     * Regresa la descripcion del error ocurrido<p>
     * 
     * @return Tipo: string
     */
    public String getError() {
        String strError = "Código:" + codigo + ". Descripción: " + this.descripcion;
        //String strMensaje = "Se detectó el siguiente error FATAL: "; //se usa para enviar correo
        switch (this.serveridad) {
            case INFO: {
                if (super.getCause() != null) {
                    strError += "Error en la clase: " + this.clase + ". Información adicional: " + super.getMessage();
                }
                break;
            }
            case DEBUG: {
                if (super.getCause() != null) {
                    strError += "Error en la clase: " + this.clase + ". Información adicional: " + super.getMessage();
                }
                break;
            }
            case FATAL: {
                if (super.getCause() != null) {
                    strError += "Error en la clase: " + this.clase + ". Información adicional: " + super.getMessage();
                }
                break;
            }
        }
        return strError;
    }
    
    /**
     * Regresa el codigo del error<p>
     * 
     * @return Tipo: int
     */
    public int getCodigo() {
        return codigo;
    }
    
}

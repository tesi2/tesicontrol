/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.smtp.sha;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author ASUS
 */
public class SHAUtils {
      public static String sha512(String texto, Boolean shift) throws NoSuchAlgorithmException{
        String shaTemp;
        try {            
            shaTemp = getStringMessageDigest(texto, "SHA-512");
            if(shift){
                return shaTemp.toUpperCase();
            }
            return shaTemp.toLowerCase();
        } catch (NoSuchAlgorithmException exc) {
            throw (exc);
        }catch(Exception exc){
            throw (exc);
        }
    }
    
    /**
     * Obtiene el Sha512 con Shift = true
     * @param texto
     * @return Retorna el Sha en Upper case
     */
    public static String sha512(String texto) throws NoSuchAlgorithmException{
        String shaTemp;
        Boolean shift = Boolean.TRUE;
        try {            
            shaTemp = getStringMessageDigest(texto, "SHA-512");
            if(shift){
                return shaTemp.toUpperCase();
            }
            return shaTemp.toLowerCase();
        } catch (NoSuchAlgorithmException exc) {
            throw (exc);
        }catch(Exception exc){
            throw (exc);
        }
    }
    
    public static String sha256(String texto) throws NoSuchAlgorithmException{
        String shaTemp;
        Boolean shift = Boolean.TRUE;
        try {            
            shaTemp = getStringMessageDigest(texto, "SHA-256");
            if(shift){
                return shaTemp.toUpperCase();
            }
            return shaTemp.toLowerCase();
        } catch (NoSuchAlgorithmException exc) {
            throw (exc);
        }catch(Exception exc){
            throw (exc);
        }
    }
    
    public static String sha1(String texto, Boolean shift) throws NoSuchAlgorithmException{
        String shaTemp;
        try {            
            shaTemp = getStringMessageDigest(texto, "SHA-1");
            if(shift){
                return shaTemp.toUpperCase();
            }
            return shaTemp.toLowerCase();
        } catch (NoSuchAlgorithmException exc) {
          
            throw (exc);
        }catch(Exception exc){
            throw (exc);
        }
    }    
    
    private static String getStringMessageDigest(String message, String algorithm) throws NoSuchAlgorithmException{
        byte[] digest = null;
        byte[] buffer = message.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm); 
            messageDigest.reset(); 
            messageDigest.update(buffer); 
            digest = messageDigest.digest(); 
        } catch (NoSuchAlgorithmException exc) { 
            throw exc;
        } 
        return toHexadecimal(digest); 
    }
    
    private static String toHexadecimal(byte[] digest){ 
        String hash = ""; 
        for(byte aux : digest) { 
            int b = aux & 0xff; 
            if (Integer.toHexString(b).length() == 1) hash += "0"; 
            hash += Integer.toHexString(b); 
        } 
        return hash;
    }
    
    
}

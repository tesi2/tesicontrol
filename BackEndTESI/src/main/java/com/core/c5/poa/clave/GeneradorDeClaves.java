/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.clave;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Alan Isaac Villafan
 */
public class GeneradorDeClaves {

    private static int porcentajeMayusculas;
    private static int porcentajeSimbolos;
    private static int porcentajeNumeros;
    private static int longitudPassword;
    private static Random semilla;
    private static StringBuilder password;
    // Caracteres que pueden emplearse en la contraseña
    private static String caracteres = "abcdefghijkmnpqrstuvwxyz";
    private static String numeros = "0123456789";
    private static String simbolos = "!+-=&()";

    // Cadena que contiene el password generado
    private static enum TipoCaracterEnum {
        Minuscula, Mayuscula, Simbolo, Numero
    }

    public GeneradorDeClaves() {
        Date d = new Date();
        semilla = new Random(d.getTime());
    }

    public static String generar(int _longitudCaracteres, int _porcentajeMayusculas, int _porcentajeSimbolos,
            int _porcentajeNumeros) {
        longitudPassword = _longitudCaracteres;
        porcentajeMayusculas = _porcentajeMayusculas;
        porcentajeSimbolos = _porcentajeSimbolos;
        porcentajeNumeros = _porcentajeNumeros;

        if (porcentajeMayusculas + porcentajeSimbolos + porcentajeNumeros > 100) {
        }

        Date d = new Date();

        semilla = new Random(d.getTime());

        GeneraPassword();
        return password.toString();
    }

    private static void GeneraPassword() {
        // Se genera una cadena de caracteres en minúscula con la longitud del
        // password seleccionado
        password = new StringBuilder(longitudPassword);
        for (int i = 0; i < longitudPassword; i++) {
            password.append(GetCaracterAleatorio(TipoCaracterEnum.Minuscula));
        }

        // Se obtiene el número de caracteres especiales (Mayúsculas y caracteres)
        int numMayusculas = (int) (longitudPassword * (porcentajeMayusculas / 100d));
        int numSimbolos = (int) (longitudPassword * (porcentajeSimbolos / 100d));
        int numNumeros = (int) (longitudPassword * (porcentajeNumeros / 100d));

        // Se obtienen las posiciones en las que irán los caracteres especiales
        Integer[] caracteresEspeciales = GetPosicionesCaracteresEspeciales(numMayusculas + numSimbolos + numNumeros);
        int posicionInicial = 0;
        int posicionFinal = 0;

        // Se reemplazan las mayúsculas
        posicionFinal += numMayusculas;
        ReemplazaCaracteresEspeciales(caracteresEspeciales, posicionInicial, posicionFinal, TipoCaracterEnum.Mayuscula);

        // Se reemplazan los símbolos
        posicionInicial = posicionFinal;
        posicionFinal += numSimbolos;
        ReemplazaCaracteresEspeciales(caracteresEspeciales, posicionInicial, posicionFinal, TipoCaracterEnum.Simbolo);

        // Se reemplazan los Números
        posicionInicial = posicionFinal;
        posicionFinal += numNumeros;
        ReemplazaCaracteresEspeciales(caracteresEspeciales, posicionInicial, posicionFinal, TipoCaracterEnum.Numero);

    }

    private static void ReemplazaCaracteresEspeciales(Integer[] posiciones, int posicionInicial, int posicionFinal,
            TipoCaracterEnum tipoCaracter) {
        for (Integer i = posicionInicial; i < posicionFinal; i++) {
            int j = posiciones[i];
            password.setCharAt(j, GetCaracterAleatorio(tipoCaracter));
        }
    }

    private static Integer[] GetPosicionesCaracteresEspeciales(int numeroPosiciones) {

        List<Integer> lista = new ArrayList<>();
        while (lista.size() < numeroPosiciones) {
            Integer posicion = semilla.nextInt(longitudPassword);
            if (!lista.contains(posicion)) {
                lista.add(posicion);
            }
        }
        Integer[] arr = new Integer[lista.size()];
        arr = lista.toArray(arr);
        return arr;
    }
static String generarMatricula(){
String matricula ="";
    SimpleDateFormat sdf =  new SimpleDateFormat("yyyy");
    Date d = new Date();
    
return matricula;
}
static String generarMatriculaTemporal(){
String matricula ="";
return matricula;                                            
}

    static char GetCaracterAleatorio(TipoCaracterEnum tipoCaracter) {
        String juegoCaracteres;
        switch (tipoCaracter) {
            case Mayuscula:
                juegoCaracteres = caracteres.toUpperCase();
                break;
            case Minuscula:
                juegoCaracteres = caracteres.toLowerCase();
                break;
            case Numero:
                juegoCaracteres = numeros;
                break;
            default:
                juegoCaracteres = simbolos;
                break;
        }

        // índice máximo de la matriz char de caracteres
        int longitudJuegoCaracteres = juegoCaracteres.length();

        // Obtención de un número aletorio para obtener la posición del carácter
        int numeroAleatorio = semilla.nextInt(longitudJuegoCaracteres);

        // Se devuelve una posición obtenida aleatoriamente
        char c = juegoCaracteres.charAt(numeroAleatorio);
        return c;
    }
}

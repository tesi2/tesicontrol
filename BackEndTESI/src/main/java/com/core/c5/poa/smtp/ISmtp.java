/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.smtp;

import java.io.File;
import java.util.List;

/**
 *
 * @author Balam
 */
public interface ISmtp {
    
    /**
     * Metodo encargado de enviar el correo electrónico.<p>
     */
    public void enviarCorreo();
    
    /**
     * Asunto del correo.<p>
     * 
     * @param asunto    Tipo: string
     */
    public void setAsunto(String asunto);
    
    /**
     * Cuenta(s) que se utilizara como "destinatario" del correo.<p>
     * 
     * @param receptoresList    Tipo: lista de string
     */
    public void setReceptoresList(List<String> receptoresList);

    /**
     * Cuenta(s) que se colocoran como "con copia" del correo<p>
     * 
     * @param copiasList    Tipo: lista de string
     */
    public void setCopiasList(List<String> copiasList);

    /**
     * Cuenta(s) que se colocoran como "con copia oulta" del correo<p>
     * 
     * @param copiasocultasList    Tipo: lista de string
     */
    public void setCopiasocultasList(List<String> copiasocultasList);
    
    /**
     * Formato en el que se enviara el contenido del mensaje, solo se permite "TXT" o "HTML"<p>
     * 
     * @param formatoConteido    Tipo: enum
     */
    public void setFormatoConteido(String formatoConteido);
    
    /**
     * Contenido del mensaje a enviar por correo<p>
     * 
     * @param mensaje   Tipo: string
     */
    public void setMensaje(String mensaje);
    
    /**
     * Documentos que serán enviados como archivos adjuntos.<p>
     * 
     * @param adjuntosFileList    Tipo: lista de file
     */
    public void setAdjuntosFileList(List<File> adjuntosFileList);
}

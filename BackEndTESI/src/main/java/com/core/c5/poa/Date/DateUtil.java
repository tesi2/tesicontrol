/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.Date;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author Alan Isaac Villafan
 */
public class DateUtil {
    public static String obtenerFechaConFormato(Date fecha , String formato){
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        String fechaFormateada = sdf.format(fecha);
        return fechaFormateada;
    }
}

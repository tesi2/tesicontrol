/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.Exeption;

/**
 * Tipos de severidad para la clase TesException.<p>
 * 
 * Los tipos que estan disponibles son:
 * <ul>
 * <li> INFO: Se usa para informar al usuario de alguna falla que está en sus manos corregir,
 * una vez que se corrija, podrá continuar trabajando en la aplicación.
 * <li> DEBUG: Se usa cuando el equipo de desarrollo esta trabajando y las excepciones
 * se deben revisar desde el código o base de datos
 * <li> FATAL: Se usa para reportar excepciones que lanza JAVA y sus recursos
 * y que por ningún motivo se debe entrerar el usuario
 * </ul>
 * 
 * @author balam
 */
public enum TesExcepSeveridad {
    INFO, DEBUG, FATAL
}

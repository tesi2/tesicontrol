/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.core.c5.poa.file;

import com.core.c5.poa.Exeption.TesException;
import java.io.File;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author balam
 */
public class FileUtilTest {
    
    public FileUtilTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of fileToBase64 method, of class FileUtil.
     */
//    @Test
//    public void testFileToBase64() {
//        System.out.println("fileToBase64");
//        try {
//            File file = new File("C:\\Users\\balam\\Desktop\\temporal.txt");
//            String expResult = "ZW4gdGllbXBvIGRlIDAgLSAyNGhyDQplbiByaWVzZ28gZGUgMjQgLSA0OGhyDQp2ZW5jaWRvcyBtw6FzIGRlIDQ4aHINCg0KRW4gcHJvY2Vzbzogbm8gZXN0YW4gZW1iYXJjYWRvIG8gY2FuY2VsYWNpb24NCkVtYmFyY2FkbzogZW1iYXJjYWRvDQpFbnRyZWdhZG86IGVudHJlZ2Fkbw==";
//            System.out.println(expResult);
//            String result = FileUtil.fileToBase64(file);
//            System.out.println(result);
//            assertEquals(expResult, result);
//            System.out.println("!!Exito¡¡");
//        } catch (TesException ex) {
//            System.out.println("Esto fallo, sera que: " + ex.getError());
//            fail("Esto fallo, sera que: " + ex.getError());
//        }
//    }

    /**
     * Test of obtenerNobreArchivo method, of class FileUtil.
     */
//    @Test
//    public void testObtenerNobreArchivo() {
//        System.out.println("Probando metodo obtenerNobreArchivo");
//        String nombre = new File("C:\\Users\\balam\\Desktop\\prueba.txt").getName();
//        String expResult = "prueba";
//        System.out.println("Se espera: " + expResult);
//        String result = FileUtil.obtenerNobreArchivo(nombre);
//        System.out.println("La clase devolvio: " + result);
//        if (expResult.equals(result)) {
//            System.out.println("!!Exito¡¡");
//        } else {
//           fail("Esto fallo"); 
//        }
//    }

    /**
     * Test of obtenerExtencion method, of class FileUtil.
     */
//    @Test
//    public void testObtenerExtencion() {
//        System.out.println("Probando metodo obtenerExtencion");
//        String nombre = new File("C:\\Users\\balam\\Desktop\\prueba.txt").getName();
//        String expResult = "txt";
//        System.out.println("Se espera: " + expResult);
//        String result = FileUtil.obtenerExtencion(nombre);
//        System.out.println("La clase devolvio: " + result);
//        assertEquals(expResult, result);
//        if (expResult.equals(result)) {
//            System.out.println("!!Exito¡¡");
//        } else {
//            fail("Esto fallo");
//        }
//    }

    /**
     * Test of obtenerMimeTypeDeExtension method, of class FileUtil.
     */
//    @Test
//    public void testObtenerMimeTypeDeExtension() {
//        System.out.println("Probando metodo obtenerMimeTypeDeExtension");
//        String extencion = "pdf";
//        String expResult = "application/pdf";
//        System.out.println("Se espera: " + expResult);
//        String result = FileUtil.obtenerMimeTypeDeExtension(extencion);
//        System.out.println("La clase devolvio: " + result);
//        if (expResult.equals(result)) {
//            System.out.println("!!Exito¡¡");
//        } else {
//            fail("Esto fallo");
//        }
//    }

    /**
     * Test of base64ToFile method, of class FileUtil.
     */
//    @Test
//    public void testBase64ToFileTemp() {
//        System.out.println("base64ToFile");
//        try {
//            String b64 = "ZW4gdGllbXBvIGRlIDAgLSAyNGhyDQplbiByaWVzZ28gZGUgMjQgLSA0OGhyDQp2ZW5jaWRvcyBtw6FzIGRlIDQ4aHINCg0KRW4gcHJvY2Vzbzogbm8gZXN0YW4gZW1iYXJjYWRvIG8gY2FuY2VsYWNpb24NCkVtYmFyY2FkbzogZW1iYXJjYWRvDQpFbnRyZWdhZG86IGVudHJlZ2Fkbw==";
//            String nombre = "prueba";
//            String extension = "txt";
//            File expResult = new File("C:\\sitioTES\\TEMP\\prueba.txt");
//            File result = FileUtil.base64ToFileTemp(b64, nombre, extension);
//            assertEquals(expResult, result);
//            System.out.println("!!Exito¡¡");
//        } catch (TesException ex) {
//            System.out.println("Esto fallo, sera que: " + ex.getError());
//            fail("Esto fallo, sera que: " + ex.getError());
//        }
//    }

    /**
     * Test of base64ToFile method, of class FileUtil.
     */
//    @Test
//    public void testBase64ToFile() {
//        System.out.println("base64ToFile");
//        try {
//            String b64 = "ZW4gdGllbXBvIGRlIDAgLSAyNGhyDQplbiByaWVzZ28gZGUgMjQgLSA0OGhyDQp2ZW5jaWRvcyBtw6FzIGRlIDQ4aHINCg0KRW4gcHJvY2Vzbzogbm8gZXN0YW4gZW1iYXJjYWRvIG8gY2FuY2VsYWNpb24NCkVtYmFyY2FkbzogZW1iYXJjYWRvDQpFbnRyZWdhZG86IGVudHJlZ2Fkbw==";
//            String pathFile = "C:\\Users\\balam\\Desktop\\prueba.txt";
//            File expResult = new File("C:\\Users\\balam\\Desktop\\prueba.txt");
//            File result = FileUtil.base64ToFile(b64, pathFile);
//            assertEquals(expResult, result);
//            System.out.println("!!Exito¡¡");
//        } catch (TesException ex) {
//            System.out.println("Esto fallo, sera que: " + ex.getError());
//            fail("Esto fallo, sera que: " + ex.getError());
//        }
//    }
    
}
